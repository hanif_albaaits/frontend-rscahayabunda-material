import {
  GET_USER,
  GET_USER_ONE,
  GET_USER_PEG,
  ADD_USER,
  DEL_USER,
  PUT_USER
} from "../actions/types";

const initialState = {
  user: [],
  user1: [],
  pegawai: [],
  statusGET: "",
  statusDEL: "",
  statusADD: "",
  statusPUT: ""
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_USER:
      return {
        ...state,
        user: action.payload,
        statusGET: action.status
      };

    case GET_USER_ONE:
      return {
        ...state,
        user1: action.payload,
        statusGET: action.status
      };

    case GET_USER_PEG:
      return {
        ...state,
        pegawai: action.payload,
        statusGET: action.status
      };

    case DEL_USER:
      return {
        ...state,
        user: state.user.filter(user => user._id !== action.payload),
        statusDEL: action.status
      };

    case ADD_USER:
      return {
        ...state,
        user: state.user.concat(action.payload),
        statusADD: action.status
      };

    case PUT_USER:
      return {
        ...state,
        statusPUT: action.status
      };

    default:
      return state;
  }
}
