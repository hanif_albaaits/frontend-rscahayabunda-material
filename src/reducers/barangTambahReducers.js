import {
  GET_TAMBAH,
  GET_TAMBAH_ONE,
  GET_TAMBAH_BRG,
  ADD_TAMBAH,
  DEL_TAMBAH,
  PUT_TAMBAH
} from "../actions/types";

const initialState = {
  tambah: [],
  tambah1: [],
  barang: [],
  statusGET: "",
  statusDEL: "",
  statusADD: "",
  statusPUT: ""
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_TAMBAH:
      return {
        ...state,
        tambah: action.payload,
        statusGET: action.status
      };

    case GET_TAMBAH_ONE:
      return {
        ...state,
        tambah1: action.payload,
        statusGET: action.status
      };

    case GET_TAMBAH_BRG:
      return {
        ...state,
        barang: action.payload,
        statusGET: action.status
      };

    case DEL_TAMBAH:
      return {
        ...state,
        tambah: state.tambah.filter(
          tambah => tambah.kd_tambah !== action.payload
        ),
        statusDEL: action.status
      };

    case ADD_TAMBAH:
      return {
        ...state,
        tambah: state.tambah.concat(action.payload),
        statusADD: action.status
      };

    case PUT_TAMBAH:
      return {
        ...state,
        statusPUT: action.status
      };

    default:
      return state;
  }
}
