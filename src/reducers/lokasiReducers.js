import {
  GET_LOKASI,
  GET_LOKASI_ONE,
  ADD_LOKASI,
  DEL_LOKASI,
  PUT_LOKASI
} from "../actions/types";

const initialState = {
  lokasi: [],
  lokasi1: [],
  statusGET: "",
  statusDEL: "",
  statusADD: "",
  statusPUT: ""
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_LOKASI:
      return {
        ...state,
        lokasi: action.payload,
        statusGET: action.status
      };

    case GET_LOKASI_ONE:
      return {
        ...state,
        lokasi1: action.payload,
        statusGET: action.status
      };

    case DEL_LOKASI:
      return {
        ...state,
        lokasi: state.lokasi.filter(lokasi => lokasi.code !== action.payload),
        statusDEL: action.status
      };

    case ADD_LOKASI:
      return {
        ...state,
        lokasi: state.lokasi.concat(action.payload),
        statusADD: action.status
      };

    case PUT_LOKASI:
      return {
        ...state,
        statusPUT: action.status
      };

    default:
      return state;
  }
}
