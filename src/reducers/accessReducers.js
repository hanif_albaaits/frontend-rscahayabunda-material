import {
  GET_ACCESS,
  GET_ACCESS_ONE,
  ADD_ACCESS,
  DEL_ACCESS,
  PUT_ACCESS
} from "../actions/types";

const initialState = {
  access: [],
  access1: [],
  statusGET: "",
  statusDEL: "",
  statusADD: "",
  statusPUT: ""
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_ACCESS:
      return {
        ...state,
        access: action.payload,
        statusGET: action.status
      };

    case GET_ACCESS_ONE:
      return {
        ...state,
        access1: action.payload,
        statusGET: action.status
      };

    case DEL_ACCESS:
      return {
        ...state,
        access: state.access.filter(access => access._id !== action.payload),
        statusDEL: action.status
      };

    case ADD_ACCESS:
      return {
        ...state,
        access: state.access.concat(action.payload),
        statusADD: action.status
      };

    case PUT_ACCESS:
      return {
        ...state,
        statusPUT: action.status
      };

    default:
      return state;
  }
}
