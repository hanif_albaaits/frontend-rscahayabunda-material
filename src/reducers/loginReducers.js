import { CURRENT_USER, REPASS_USER } from "../actions/types";

const initialState = {
  user: {},
  status: "",
  message: ""
};

export default function(state = initialState, action) {
  switch (action.type) {
    case CURRENT_USER:
      return {
        ...state,
        user: action.payload,
        status: action.status,
        message: action.message
      };
    case REPASS_USER:
      return {
        ...state,
        status: action.status,
        message: action.message
      };

    default:
      return state;
  }
}
