import {
  GET_SERVIS,
  GET_SERVIS_ONE,
  GET_SERVIS_BRG,
  ADD_SERVIS,
  DEL_SERVIS,
  PUT_SERVIS
} from "../actions/types";

const initialState = {
  servis: [],
  servis1: [],
  barang: [],
  statusGET: "",
  statusDEL: "",
  statusADD: "",
  statusPUT: ""
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_SERVIS:
      return {
        ...state,
        servis: action.payload,
        statusGET: action.status
      };

    case GET_SERVIS_ONE:
      return {
        ...state,
        servis1: action.payload,
        statusGET: action.status
      };

    case GET_SERVIS_BRG:
      return {
        ...state,
        barang: action.payload,
        statusGET: action.status
      };

    case DEL_SERVIS:
      return {
        ...state,
        servis: state.servis.filter(
          servis => servis.kd_servis !== action.payload
        ),
        statusDEL: action.status
      };

    case ADD_SERVIS:
      return {
        ...state,
        servis: state.servis.concat(action.payload),
        statusADD: action.status
      };

    case PUT_SERVIS:
      return {
        ...state,
        statusPUT: action.status
      };

    default:
      return state;
  }
}
