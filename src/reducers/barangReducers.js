import {
  GET_BARANG,
  GET_BARANG_ONE,
  GET_CARI_BARANG,
  GET_BARANG_SUM,
  GET_KELUAR_SUM,
  ADD_BARANG,
  DEL_BARANG,
  PUT_BARANG
} from "../actions/types";

const initialState = {
  barang: [],
  barang1: {},
  cari: [],
  sum: null,
  keluar: null,
  statusGET: "",
  statusDEL: "",
  statusADD: "",
  statusPUT: ""
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_BARANG:
      return {
        ...state,
        barang: action.payload,
        statusGET: action.status
      };

    case GET_BARANG_ONE:
      return {
        ...state,
        barang1: action.payload,
        statusGET: action.status
      };

    case GET_CARI_BARANG:
      return {
        ...state,
        cari: action.payload,
        statusGET: action.status
      };

    case GET_BARANG_SUM:
      return {
        ...state,
        sum: action.payload,
        statusGET: action.status
      };

    case GET_KELUAR_SUM:
      return {
        ...state,
        keluar: action.payload,
        statusGET: action.status
      };
    case DEL_BARANG:
      return {
        ...state,
        barang: state.barang.filter(
          barang => barang.kd_barang !== action.payload
        ),
        statusDEL: action.status
      };

    case ADD_BARANG:
      return {
        ...state,
        barang: state.barang.concat(action.payload),
        statusADD: action.status
      };

    case PUT_BARANG:
      return {
        ...state,
        statusPUT: action.status
      };

    default:
      return state;
  }
}
