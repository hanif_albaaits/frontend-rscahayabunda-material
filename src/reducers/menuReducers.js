import {
  GET_MENU,
  GET_MENU_ONE,
  GET_MENU_SIDE,
  ADD_MENU,
  DEL_MENU,
  PUT_MENU
} from "../actions/types";

const initialState = {
  menu: [],
  menu1: [],
  side: [],
  statusGET: "",
  statusDEL: "",
  statusADD: "",
  statusPUT: ""
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_MENU:
      return {
        ...state,
        menu: action.payload,
        statusGET: action.status
      };

    case GET_MENU_ONE:
      return {
        ...state,
        menu1: action.payload,
        statusGET: action.status
      };

    case GET_MENU_SIDE:
      return {
        ...state,
        side: action.payload,
        statusGET: action.status
      };

    case DEL_MENU:
      return {
        ...state,
        menu: state.menu.filter(menu => menu.code !== action.payload),
        statusDEL: action.status
      };

    case ADD_MENU:
      return {
        ...state,
        menu: state.menu.concat(action.payload),
        statusADD: action.status
      };

    case PUT_MENU:
      return {
        ...state,
        statusPUT: action.status
      };

    default:
      return state;
  }
}
