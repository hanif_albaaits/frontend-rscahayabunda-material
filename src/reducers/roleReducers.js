import {
  GET_ROLE,
  GET_ROLE_ONE,
  ADD_ROLE,
  DEL_ROLE,
  PUT_ROLE
} from "../actions/types";

const initialState = {
  role: [],
  role1: [],
  statusGET: "",
  statusDEL: "",
  statusADD: "",
  statusPUT: ""
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_ROLE:
      return {
        ...state,
        role: action.payload,
        statusGET: action.status
      };

    case GET_ROLE_ONE:
      return {
        ...state,
        role1: action.payload,
        statusGET: action.status
      };

    case DEL_ROLE:
      return {
        ...state,
        role: state.role.filter(role => role.code !== action.payload),
        statusDEL: action.status
      };

    case ADD_ROLE:
      return {
        ...state,
        role: state.role.concat(action.payload),
        statusADD: action.status
      };

    case PUT_ROLE:
      return {
        ...state,
        statusPUT: action.status
      };

    default:
      return state;
  }
}
