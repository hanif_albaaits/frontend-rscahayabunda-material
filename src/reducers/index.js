import { combineReducers } from "redux";
import login from "./loginReducers";
import user from "./userReducers";
import role from "./roleReducers";
import menu from "./menuReducers";
import divisi from "./divisiReducers";
import lokasi from "./lokasiReducers";
import pegawai from "./pegawaiReducers";
import barang from "./barangReducers";
import ambilBarang from "./barangAmbilReducers";
import pinjamBarang from "./barangPinjamReducers";
import servisBarang from "./barangServisReducers";
import tambahBarang from "./barangTambahReducers";
import access from "./accessReducers";

export default combineReducers({
  loginReducers: login,
  userReducers: user,
  roleReducers: role,
  accessReducers: access,
  menuReducers: menu,
  divisiReducers: divisi,
  lokasiReducers: lokasi,
  pegawaiReducers: pegawai,
  barangReducers: barang,
  ambilBReducers: ambilBarang,
  pinjamBReducers: pinjamBarang,
  servisBReducers: servisBarang,
  tambahBReducers: tambahBarang
});
