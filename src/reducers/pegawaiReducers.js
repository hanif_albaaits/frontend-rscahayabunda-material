import {
  GET_PEGAWAI,
  GET_PEGAWAI_ONE,
  ADD_PEGAWAI,
  DEL_PEGAWAI,
  PUT_PEGAWAI
} from "../actions/types";

const initialState = {
  pegawai: [],
  pegawai1: [],
  statusGET: "",
  statusDEL: "",
  statusADD: "",
  statusPUT: ""
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_PEGAWAI:
      return {
        ...state,
        pegawai: action.payload,
        statusGET: action.status
      };

    case GET_PEGAWAI_ONE:
      return {
        ...state,
        pegawai1: action.payload,
        statusGET: action.status
      };

    case DEL_PEGAWAI:
      return {
        ...state,
        pegawai: state.pegawai.filter(
          pegawai => pegawai.kd_pegawai !== action.payload
        ),
        statusDEL: action.status
      };

    case ADD_PEGAWAI:
      return {
        ...state,
        pegawai: state.pegawai.concat(action.payload),
        statusADD: action.status
      };

    case PUT_PEGAWAI:
      return {
        ...state,
        statusPUT: action.status
      };

    default:
      return state;
  }
}
