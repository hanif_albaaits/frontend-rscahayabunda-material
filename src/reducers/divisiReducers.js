import {
  GET_DIVISI,
  GET_DIVISI_ONE,
  ADD_DIVISI,
  DEL_DIVISI,
  PUT_DIVISI
} from "../actions/types";

const initialState = {
  divisi: [],
  divisi1: [],
  statusGET: "",
  statusDEL: "",
  statusADD: "",
  statusPUT: ""
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_DIVISI:
      return {
        ...state,
        divisi: action.payload,
        statusGET: action.status
      };

    case GET_DIVISI_ONE:
      return {
        ...state,
        divisi1: action.payload,
        statusGET: action.status
      };

    case DEL_DIVISI:
      return {
        ...state,
        divisi: state.divisi.filter(divisi => divisi.code !== action.payload),
        statusDEL: action.status
      };

    case ADD_DIVISI:
      return {
        ...state,
        divisi: state.divisi.concat(action.payload),
        statusADD: action.status
      };

    case PUT_DIVISI:
      return {
        ...state,
        statusPUT: action.status
      };

    default:
      return state;
  }
}
