import {
  GET_AMBIL,
  GET_AMBIL_ONE,
  GET_AMBIL_BRG,
  ADD_AMBIL,
  DEL_AMBIL,
  PUT_AMBIL
} from "../actions/types";

const initialState = {
  ambil: [],
  ambil1: [],
  barang: [],
  statusGET: "",
  statusDEL: "",
  statusADD: "",
  statusPUT: ""
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_AMBIL:
      return {
        ...state,
        ambil: action.payload,
        statusGET: action.status
      };

    case GET_AMBIL_ONE:
      return {
        ...state,
        ambil1: action.payload,
        statusGET: action.status
      };

    case GET_AMBIL_BRG:
      return {
        ...state,
        barang: action.payload,
        statusGET: action.status
      };

    case DEL_AMBIL:
      return {
        ...state,
        ambil: state.ambil.filter(ambil => ambil.kd_ambil !== action.payload),
        statusDEL: action.status
      };

    case ADD_AMBIL:
      return {
        ...state,
        ambil: state.ambil.concat(action.payload),
        statusADD: action.status
      };

    case PUT_AMBIL:
      return {
        ...state,
        statusPUT: action.status
      };

    default:
      return state;
  }
}
