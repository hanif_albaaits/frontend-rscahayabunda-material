import {
  GET_PINJAM,
  GET_PINJAM_ONE,
  GET_PINJAM_BRG,
  GET_CARI_PINJAM,
  ADD_PINJAM,
  DEL_PINJAM,
  PUT_PINJAM
} from "../actions/types";

const initialState = {
  pinjam: [],
  pinjam1: [],
  barang: [],
  cari: [],
  statusGET: "",
  statusDEL: "",
  statusADD: "",
  statusPUT: ""
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_PINJAM:
      return {
        ...state,
        pinjam: action.payload,
        statusGET: action.status
      };

    case GET_PINJAM_ONE:
      return {
        ...state,
        pinjam1: action.payload,
        statusGET: action.status
      };

    case GET_PINJAM_BRG:
      return {
        ...state,
        barang: action.payload,
        statusGET: action.status
      };

    case GET_CARI_PINJAM:
      return {
        ...state,
        cari: action.payload,
        statusGET: action.status
      };

    case DEL_PINJAM:
      return {
        ...state,
        pinjam: state.pinjam.filter(
          pinjam => pinjam.kd_pinjam !== action.payload
        ),
        statusDEL: action.status
      };

    case ADD_PINJAM:
      return {
        ...state,
        pinjam: state.pinjam.concat(action.payload),
        statusADD: action.status
      };

    case PUT_PINJAM:
      return {
        ...state,
        statusPUT: action.status
      };

    default:
      return state;
  }
}
