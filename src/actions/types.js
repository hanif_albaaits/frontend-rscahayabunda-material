export const CURRENT_USER = "CURRENT_USER";
export const REPASS_USER = "REPASS_USER";

export const GET_USER = "GET_USER"; //fetch
export const GET_USER_ONE = "GET_USER_ONE"; //fetch
export const GET_USER_PEG = "GET_USER_PEG"; //fetch
export const ADD_USER = "ADD_USER"; //add
export const DEL_USER = "DEL_USER"; //delete
export const PUT_USER = "PUT_USER"; //update

export const GET_MENU = "GET_MENU"; //fetch
export const GET_MENU_ONE = "GET_MENU_ONE"; //fetch
export const GET_MENU_SIDE = "GET_MENU_SIDE"; //fetch
export const ADD_MENU = "ADD_MENU"; //add
export const DEL_MENU = "DEL_MENU"; //delete
export const PUT_MENU = "PUT_MENU"; //update

export const GET_ROLE = "GET_ROLE"; //fetch
export const GET_ROLE_ONE = "GET_ROLE_ONE"; //fetch
export const ADD_ROLE = "ADD_ROLE"; //add
export const DEL_ROLE = "DEL_ROLE"; //delete
export const PUT_ROLE = "PUT_ROLE"; //update

export const GET_ACCESS = "GET_ACCESS"; //fetch
export const GET_ACCESS_ONE = "GET_ACCESS_ONE"; //fetch
export const ADD_ACCESS = "ADD_ACCESS"; //add
export const DEL_ACCESS = "DEL_ACCESS"; //delete
export const PUT_ACCESS = "PUT_ACCESS"; //update

export const GET_DIVISI = "GET_DIVISI"; //fetch
export const GET_DIVISI_ONE = "GET_DIVISI_ONE"; //fetch
export const ADD_DIVISI = "ADD_DIVISI"; //add
export const DEL_DIVISI = "DEL_DIVISI"; //delete
export const PUT_DIVISI = "PUT_DIVISI"; //update

export const GET_LOKASI = "GET_LOKASI"; //fetch
export const GET_LOKASI_ONE = "GET_LOKASI_ONE"; //fetch
export const ADD_LOKASI = "ADD_LOKASI"; //add
export const DEL_LOKASI = "DEL_LOKASI"; //delete
export const PUT_LOKASI = "PUT_LOKASI"; //update

export const GET_PEGAWAI = "GET_PEGAWAI"; //fetch
export const GET_PEGAWAI_ONE = "GET_PEGAWAI_ONE";
export const ADD_PEGAWAI = "ADD_PEGAWAI"; //add
export const DEL_PEGAWAI = "DEL_PEGAWAI"; //delete
export const PUT_PEGAWAI = "PUT_PEGAWAI"; //update

export const GET_BARANG = "GET_BARANG";
export const GET_BARANG_ONE = "GET_BARANG_ONE";
export const GET_CARI_BARANG = "GET_CARI_BARANG";
export const GET_BARANG_SUM = "GET_BARANG_SUM";
export const GET_KELUAR_SUM = "GET_KELUAR_SUM";

export const ADD_BARANG = "ADD_BARANG";
export const DEL_BARANG = "DEL_BARANG";
export const PUT_BARANG = "PUT_BARANG";

export const GET_AMBIL = "GET_AMBIL";
export const GET_AMBIL_ONE = "GET_AMBIL_ONE";
export const GET_AMBIL_BRG = "GET_AMBIL_BRG";
export const ADD_AMBIL = "ADD_AMBIL";
export const DEL_AMBIL = "DEL_AMBIL";
export const PUT_AMBIL = "PUT_AMBIL";

export const GET_PINJAM = "GET_PINJAM";
export const GET_PINJAM_ONE = "GET_PINJAM_ONE";
export const GET_PINJAM_BRG = "GET_PINJAM_BRG";
export const GET_CARI_PINJAM = "GET_CARI_PINJAM";
export const ADD_PINJAM = "ADD_PINJAM";
export const DEL_PINJAM = "DEL_PINJAM";
export const PUT_PINJAM = "PUT_PINJAM";

export const GET_SERVIS = "GET_SERVIS";
export const GET_SERVIS_ONE = "GET_SERVIS_ONE";
export const GET_SERVIS_BRG = "GET_SERVIS_BRG";
export const ADD_SERVIS = "ADD_SERVIS";
export const DEL_SERVIS = "DEL_SERVIS";
export const PUT_SERVIS = "PUT_SERVIS";

export const GET_TAMBAH = "GET_TAMBAH";
export const GET_TAMBAH_ONE = "GET_TAMBAH_ONE";
export const GET_TAMBAH_BRG = "GET_TAMBAH_BRG";
export const ADD_TAMBAH = "ADD_TAMBAH";
export const DEL_TAMBAH = "DEL_TAMBAH";
export const PUT_TAMBAH = "PUT_TAMBAH";
