import axios from "axios";
import {
  GET_DIVISI,
  GET_DIVISI_ONE,
  ADD_DIVISI,
  DEL_DIVISI,
  PUT_DIVISI
} from "./types";
import ApiConfig from "../config/api.config.json";

let token = localStorage.getItem(ApiConfig.LS.TOKEN);

export const getDivisi = () => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.DIVISI,
    method: "get",
    headers: {
      Authorization: token
    }
  };
  axios(options)
    .then(res => {
      dispatch({
        type: GET_DIVISI,
        payload: res.data.message,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: GET_DIVISI,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const getDivisi1 = code => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.DIVISI + "/" + code,
    method: "get",
    headers: {
      Authorization: token
    }
  };
  axios(options)
    .then(res => {
      dispatch({
        type: GET_DIVISI_ONE,
        payload: res.data.message,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: GET_DIVISI_ONE,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const divisiCreate = body => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.DIVISI,
    method: "post",
    headers: {
      Authorization: token
    },
    data: body
  };
  axios(options)
    .then(res => {
      dispatch({
        type: ADD_DIVISI,
        payload: body,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: ADD_DIVISI,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const divisiUpdate = body => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.DIVISI + "/" + body.code,
    method: "put",
    headers: {
      Authorization: token
    },
    data: body
  };
  axios(options)
    .then(res => {
      dispatch({
        type: PUT_DIVISI,
        payload: body,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: PUT_DIVISI,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const divisiDelete = body => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.DIVISI + "/" + body.code,
    method: "delete",
    headers: {
      Authorization: token
    },
    data: body
  };
  axios(options)
    .then(res => {
      dispatch({
        type: DEL_DIVISI,
        payload: body.code,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: DEL_DIVISI,
        payload: error,
        status: error.response.data.code
      });
    });
};
