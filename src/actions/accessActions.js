import axios from "axios";
import {
  GET_ACCESS,
  GET_ACCESS_ONE,
  ADD_ACCESS,
  DEL_ACCESS,
  PUT_ACCESS
} from "./types";
import ApiConfig from "../config/api.config.json";

let token = localStorage.getItem(ApiConfig.LS.TOKEN);

export const getAccess = () => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.ACCESS,
    method: "get",
    headers: {
      Authorization: token
    }
  };
  axios(options)
    .then(res => {
      dispatch({
        type: GET_ACCESS,
        payload: res.data.message,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: GET_ACCESS,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const getAccess1 = (role, menu) => dispatch => {
  let options = {
    url:
      ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.ACCESS + "/" + role + "/" + menu,
    method: "get",
    headers: {
      Authorization: token
    }
  };
  axios(options)
    .then(res => {
      dispatch({
        type: GET_ACCESS_ONE,
        payload: res.data.message,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: GET_ACCESS_ONE,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const accessCreate = body => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.ACCESS,
    method: "post",
    headers: {
      Authorization: token
    },
    data: body
  };
  axios(options)
    .then(res => {
      dispatch({
        type: ADD_ACCESS,
        payload: body,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: ADD_ACCESS,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const accessUpdate = body => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.ACCESS + "/" + body._id,
    method: "put",
    headers: {
      Authorization: token
    },
    data: body
  };
  axios(options)
    .then(res => {
      dispatch({
        type: PUT_ACCESS,
        payload: body,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: PUT_ACCESS,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const accessDelete = body => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.ACCESS + "/" + body._id,
    method: "delete",
    headers: {
      Authorization: token
    },
    data: body
  };
  axios(options)
    .then(res => {
      dispatch({
        type: DEL_ACCESS,
        payload: body.code,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: DEL_ACCESS,
        payload: error,
        status: error.response.data.code
      });
    });
};
