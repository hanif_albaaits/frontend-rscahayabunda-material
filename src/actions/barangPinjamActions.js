import axios from "axios";
import {
  GET_PINJAM,
  GET_PINJAM_ONE,
  GET_PINJAM_BRG,
  GET_CARI_PINJAM,
  ADD_PINJAM,
  DEL_PINJAM,
  PUT_PINJAM
} from "./types"; //, CREATE_PINJAM, DELETE_PINJAM
import ApiConfig from "../config/api.config.json";

let token = localStorage.getItem(ApiConfig.LS.TOKEN);

export const getPinjam = () => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.PINJAMBRG.PINJAMBRG,
    method: "get",
    headers: {
      Authorization: token
    }
  };
  axios(options)
    .then(res => {
      dispatch({
        type: GET_PINJAM,
        payload: res.data.message,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: GET_PINJAM,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const getPinjam1 = kd_pinjam => dispatch => {
  let options = {
    url:
      ApiConfig.BASE_URL +
      ApiConfig.ENDPOINTS.PINJAMBRG.PINJAMBRG +
      "/" +
      kd_pinjam,
    method: "get",
    headers: {
      Authorization: token
    }
  };
  axios(options)
    .then(res => {
      dispatch({
        type: GET_PINJAM_ONE,
        payload: res.data.message,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: GET_PINJAM_ONE,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const getCariP = body => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.PINJAMBRG.PINJAMBRG + "-cari",
    method: "post",
    headers: {
      Authorization: token
    },
    data: body
  };
  axios(options)
    .then(res => {
      dispatch({
        type: GET_CARI_PINJAM,
        payload: res.data.message,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: GET_CARI_PINJAM,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const getPinjamBrg = kd_barang => dispatch => {
  let options = {
    url:
      ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.PINJAMBRG.BRG + "/" + kd_barang,
    method: "get",
    headers: {
      Authorization: token
    }
  };
  axios(options)
    .then(res => {
      dispatch({
        type: GET_PINJAM_BRG,
        payload: res.data.message,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: GET_PINJAM_BRG,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const pinjamCreate = body => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.PINJAMBRG.PINJAMBRG,
    method: "post",
    headers: {
      Authorization: token
    },
    data: body
  };
  axios(options)
    .then(res => {
      dispatch({
        type: ADD_PINJAM,
        payload: body,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: ADD_PINJAM,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const pinjamUpdate = body => dispatch => {
  let options = {
    url:
      ApiConfig.BASE_URL +
      ApiConfig.ENDPOINTS.PINJAMBRG.PINJAMBRG +
      "/" +
      body.kd_pinjam,
    method: "put",
    headers: {
      Authorization: token
    },
    data: body
  };
  axios(options)
    .then(res => {
      dispatch({
        type: PUT_PINJAM,
        payload: body,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: PUT_PINJAM,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const pinjamDelete = body => dispatch => {
  let options = {
    url:
      ApiConfig.BASE_URL +
      ApiConfig.ENDPOINTS.PINJAMBRG.PINJAMBRG +
      "/" +
      body.kd_pinjam,
    method: "delete",
    headers: {
      Authorization: token
    },
    data: body
  };
  axios(options)
    .then(res => {
      dispatch({
        type: DEL_PINJAM,
        payload: body.kd_pinjam,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: DEL_PINJAM,
        payload: error,
        status: error.response.data.code
      });
    });
};
