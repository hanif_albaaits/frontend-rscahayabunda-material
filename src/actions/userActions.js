import axios from "axios";
import {
  GET_USER,
  GET_USER_ONE,
  GET_USER_PEG,
  ADD_USER,
  DEL_USER,
  PUT_USER
} from "./types"; //, CREATE_USER, DELETE_USER

import ApiConfig from "../config/api.config.json";

let token = localStorage.getItem(ApiConfig.LS.TOKEN);

export const getUser = () => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.USER.USER,
    method: "get",
    headers: {
      Authorization: token
    }
  };
  axios(options)
    .then(res => {
      dispatch({
        type: GET_USER,
        payload: res.data.message,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: GET_USER,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const getUser1 = username => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.USER.USER + "/" + username,
    method: "get",
    headers: {
      Authorization: token
    }
  };
  axios(options)
    .then(res => {
      dispatch({
        type: GET_USER_ONE,
        payload: res.data.message,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: GET_USER_ONE,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const userCreate = body => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.USER.USER,
    method: "post",
    headers: {
      Authorization: token
    },
    data: body
  };
  axios(options)
    .then(res => {
      dispatch({
        type: ADD_USER,
        payload: body,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: ADD_USER,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const userUpdate = body => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.USER.USER + "/" + body._id,
    method: "put",
    headers: {
      Authorization: token
    },
    data: body
  };
  axios(options)
    .then(res => {
      dispatch({
        type: PUT_USER,
        payload: null,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: PUT_USER,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const userDelete = body => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.USER.USER + "/" + body._id,
    method: "delete",
    headers: {
      Authorization: token
    },
    data: body
  };
  axios(options)
    .then(res => {
      dispatch({
        type: DEL_USER,
        payload: body._id,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: DEL_USER,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const getPegawai = () => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.USER.PEGAWAI,
    method: "get",
    headers: {
      Authorization: token
    }
  };
  axios(options)
    .then(res => {
      dispatch({
        type: GET_USER_PEG,
        payload: res.data.message,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: GET_USER_PEG,
        payload: error,
        status: error.response.data.code
      });
    });
};
