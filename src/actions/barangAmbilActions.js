import axios from "axios";
import {
  GET_AMBIL,
  GET_AMBIL_ONE,
  GET_AMBIL_BRG,
  ADD_AMBIL,
  DEL_AMBIL,
  PUT_AMBIL
} from "./types"; //, CREATE_AMBIL, DELETE_AMBIL
import ApiConfig from "../config/api.config.json";

let token = localStorage.getItem(ApiConfig.LS.TOKEN);

export const getAmbil = () => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.AMBILBRG.AMBILBRG,
    method: "get",
    headers: {
      Authorization: token
    }
  };
  axios(options)
    .then(res => {
      dispatch({
        type: GET_AMBIL,
        payload: res.data.message,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: GET_AMBIL,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const getAmbil1 = kd_ambil => dispatch => {
  let options = {
    url:
      ApiConfig.BASE_URL +
      ApiConfig.ENDPOINTS.AMBILBRG.AMBILBRG +
      "/" +
      kd_ambil,
    method: "get",
    headers: {
      Authorization: token
    }
  };
  axios(options)
    .then(res => {
      dispatch({
        type: GET_AMBIL_ONE,
        payload: res.data.message,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: GET_AMBIL_ONE,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const getAmbilBrg = kd_barang => dispatch => {
  let options = {
    url:
      ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.AMBILBRG.BRG + "/" + kd_barang,
    method: "get",
    headers: {
      Authorization: token
    }
  };
  axios(options)
    .then(res => {
      dispatch({
        type: GET_AMBIL_BRG,
        payload: res.data.message,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: GET_AMBIL_BRG,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const ambilCreate = body => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.AMBILBRG.AMBILBRG,
    method: "post",
    headers: {
      Authorization: token
    },
    data: body
  };
  axios(options)
    .then(res => {
      dispatch({
        type: ADD_AMBIL,
        payload: body,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: ADD_AMBIL,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const ambilUpdate = body => dispatch => {
  let options = {
    url:
      ApiConfig.BASE_URL +
      ApiConfig.ENDPOINTS.AMBILBRG.AMBILBRG +
      "/" +
      body.kd_ambil,
    method: "put",
    headers: {
      Authorization: token
    },
    data: body
  };
  axios(options)
    .then(res => {
      dispatch({
        type: PUT_AMBIL,
        payload: body,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: PUT_AMBIL,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const ambilDelete = body => dispatch => {
  let options = {
    url:
      ApiConfig.BASE_URL +
      ApiConfig.ENDPOINTS.AMBILBRG.AMBILBRG +
      "/" +
      body.kd_ambil,
    method: "delete",
    headers: {
      Authorization: token
    },
    data: body
  };
  axios(options)
    .then(res => {
      dispatch({
        type: DEL_AMBIL,
        payload: body.kd_ambil,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: DEL_AMBIL,
        payload: error,
        status: error.response.data.code
      });
    });
};
