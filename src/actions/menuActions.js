import axios from "axios";
import {
  GET_MENU,
  GET_MENU_ONE,
  GET_MENU_SIDE,
  ADD_MENU,
  DEL_MENU,
  PUT_MENU
} from "./types";
import ApiConfig from "../config/api.config.json";

let token = localStorage.getItem(ApiConfig.LS.TOKEN);

export const getMenu = () => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.MENU.MENU,
    method: "get",
    headers: {
      Authorization: token
    }
  };
  axios(options)
    .then(res => {
      dispatch({
        type: GET_MENU,
        payload: res.data.message,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: GET_MENU,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const getMenu1 = code => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.MENU.MENU + "/" + code,
    method: "get",
    headers: {
      Authorization: token
    }
  };
  axios(options)
    .then(res => {
      dispatch({
        type: GET_MENU_ONE,
        payload: res.data.message,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: GET_MENU_ONE,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const getMenuSide = role => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.MENU.SIDE + "/" + role,
    method: "get",
    headers: {
      Authorization: token
    }
  };
  axios(options)
    .then(res => {
      dispatch({
        type: GET_MENU_SIDE,
        payload: res.data.message,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: GET_MENU_SIDE,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const menuCreate = body => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.MENU.MENU,
    method: "post",
    headers: {
      Authorization: token
    },
    data: body
  };
  axios(options)
    .then(res => {
      dispatch({
        type: ADD_MENU,
        payload: body,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: ADD_MENU,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const menuUpdate = body => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.MENU.MENU + "/" + body.code,
    method: "put",
    headers: {
      Authorization: token
    },
    data: body
  };
  axios(options)
    .then(res => {
      dispatch({
        type: PUT_MENU,
        payload: body,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: PUT_MENU,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const menuDelete = body => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.MENU.MENU + "/" + body.code,
    method: "delete",
    headers: {
      Authorization: token
    },
    data: body
  };
  axios(options)
    .then(res => {
      dispatch({
        type: DEL_MENU,
        payload: body.code,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: DEL_MENU,
        payload: error,
        status: error.response.data.code
      });
    });
};
