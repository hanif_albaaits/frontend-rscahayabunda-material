import axios from "axios";
import {
  GET_TAMBAH,
  GET_TAMBAH_ONE,
  GET_TAMBAH_BRG,
  ADD_TAMBAH,
  DEL_TAMBAH,
  PUT_TAMBAH
} from "./types"; //, CREATE_TAMBAH, DELETE_TAMBAH
import ApiConfig from "../config/api.config.json";

let token = localStorage.getItem(ApiConfig.LS.TOKEN);

export const getTambah = () => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.TAMBAHBRG.TAMBAHBRG,
    method: "get",
    headers: {
      Authorization: token
    }
  };
  axios(options)
    .then(res => {
      dispatch({
        type: GET_TAMBAH,
        payload: res.data.message,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: GET_TAMBAH,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const getTambah1 = kd_tambah => dispatch => {
  let options = {
    url:
      ApiConfig.BASE_URL +
      ApiConfig.ENDPOINTS.TAMBAHBRG.TAMBAHBRG +
      "/" +
      kd_tambah,
    method: "get",
    headers: {
      Authorization: token
    }
  };
  axios(options)
    .then(res => {
      dispatch({
        type: GET_TAMBAH_ONE,
        payload: res.data.message,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: GET_TAMBAH_ONE,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const getTambahBrg = kd_barang => dispatch => {
  let options = {
    url:
      ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.TAMBAHBRG.BRG + "/" + kd_barang,
    method: "get",
    headers: {
      Authorization: token
    }
  };
  axios(options)
    .then(res => {
      dispatch({
        type: GET_TAMBAH_BRG,
        payload: res.data.message,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: GET_TAMBAH_BRG,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const tambahCreate = body => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.TAMBAHBRG.TAMBAHBRG,
    method: "post",
    headers: {
      Authorization: token
    },
    data: body
  };
  axios(options)
    .then(res => {
      dispatch({
        type: ADD_TAMBAH,
        payload: body,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: ADD_TAMBAH,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const tambahUpdate = body => dispatch => {
  let options = {
    url:
      ApiConfig.BASE_URL +
      ApiConfig.ENDPOINTS.TAMBAHBRG.TAMBAHBRG +
      "/" +
      body.kd_tambah,
    method: "put",
    headers: {
      Authorization: token
    },
    data: body
  };
  axios(options)
    .then(res => {
      dispatch({
        type: PUT_TAMBAH,
        payload: body,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: PUT_TAMBAH,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const tambahDelete = body => dispatch => {
  let options = {
    url:
      ApiConfig.BASE_URL +
      ApiConfig.ENDPOINTS.TAMBAHBRG.TAMBAHBRG +
      "/" +
      body.kd_tambah,
    method: "delete",
    headers: {
      Authorization: token
    },
    data: body
  };
  axios(options)
    .then(res => {
      dispatch({
        type: DEL_TAMBAH,
        payload: body.kd_tambah,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: DEL_TAMBAH,
        payload: error,
        status: error.response.data.code
      });
    });
};
