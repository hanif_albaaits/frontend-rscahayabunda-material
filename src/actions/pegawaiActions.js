import axios from "axios";
import {
  GET_PEGAWAI,
  GET_PEGAWAI_ONE,
  ADD_PEGAWAI,
  DEL_PEGAWAI,
  PUT_PEGAWAI
} from "./types";
import ApiConfig from "../config/api.config.json";

let token = localStorage.getItem(ApiConfig.LS.TOKEN);

export const getPegawai = () => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.PEGAWAI,
    method: "get",
    headers: {
      Authorization: token
    }
  };
  axios(options)
    .then(res => {
      dispatch({
        type: GET_PEGAWAI,
        payload: res.data.message,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: GET_PEGAWAI,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const getPegawai1 = kd_pegawai => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.PEGAWAI + "/" + kd_pegawai,
    method: "get",
    headers: {
      Authorization: token
    }
  };
  axios(options)
    .then(res => {
      dispatch({
        type: GET_PEGAWAI_ONE,
        payload: res.data.message,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: GET_PEGAWAI_ONE,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const pegawaiCreate = body => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.PEGAWAI,
    method: "post",
    headers: {
      Authorization: token
    },
    data: body
  };
  axios(options)
    .then(res => {
      dispatch({
        type: ADD_PEGAWAI,
        payload: body,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: ADD_PEGAWAI,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const pegawaiUpdate = body => dispatch => {
  let options = {
    url:
      ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.PEGAWAI + "/" + body.kd_pegawai,
    method: "put",
    headers: {
      Authorization: token,
      "Content-Type": "application/json"
    },
    data: body
  };
  axios(options)
    .then(res => {
      dispatch({
        type: PUT_PEGAWAI,
        payload: body,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: PUT_PEGAWAI,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const pegawaiDelete = body => dispatch => {
  let options = {
    url:
      ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.PEGAWAI + "/" + body.kd_pegawai,
    method: "delete",
    headers: {
      Authorization: token
    },
    data: body
  };
  axios(options)
    .then(res => {
      dispatch({
        type: DEL_PEGAWAI,
        payload: body.kd_pegawai,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: DEL_PEGAWAI,
        payload: error,
        status: error.response.data.code
      });
    });
};
