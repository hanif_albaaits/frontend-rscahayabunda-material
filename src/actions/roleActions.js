import axios from "axios";
import { GET_ROLE, GET_ROLE_ONE, ADD_ROLE, DEL_ROLE, PUT_ROLE } from "./types";
import ApiConfig from "../config/api.config.json";

let token = localStorage.getItem(ApiConfig.LS.TOKEN);

export const getRole = () => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.ROLE,
    method: "get",
    headers: {
      Authorization: token
    }
  };
  axios(options)
    .then(res => {
      dispatch({
        type: GET_ROLE,
        payload: res.data.message,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: GET_ROLE,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const getRole1 = code => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.ROLE + "/" + code,
    method: "get",
    headers: {
      Authorization: token
    }
  };
  axios(options)
    .then(res => {
      dispatch({
        type: GET_ROLE_ONE,
        payload: res.data.message,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: GET_ROLE_ONE,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const roleCreate = body => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.ROLE,
    method: "post",
    headers: {
      Authorization: token
    },
    data: body
  };
  axios(options)
    .then(res => {
      dispatch({
        type: ADD_ROLE,
        payload: body,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: ADD_ROLE,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const roleUpdate = body => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.ROLE + "/" + body.code,
    method: "put",
    headers: {
      Authorization: token
    },
    data: body
  };
  alert(JSON.stringify(body));
  axios(options)
    .then(res => {
      dispatch({
        type: PUT_ROLE,
        payload: body,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: PUT_ROLE,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const roleDelete = body => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.ROLE + "/" + body.code,
    method: "delete",
    headers: {
      Authorization: token
    },
    data: body
  };
  axios(options)
    .then(res => {
      dispatch({
        type: DEL_ROLE,
        payload: body.code,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: DEL_ROLE,
        payload: error,
        status: error.response.data.code
      });
    });
};
