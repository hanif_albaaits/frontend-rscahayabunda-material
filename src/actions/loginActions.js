import axios from "axios";
import { CURRENT_USER, REPASS_USER } from "./types";
import ApiConfig from "../config/api.config.json";

// let token = localStorage.getItem(ApiConfig.LS.TOKEN);

// Login User Action
export const UserLogin = userData => dispatch => {
  (async () => {
    let option = {
      url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.LOGIN.LOGIN,
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      data: userData
    };

    try {
      let res = await axios(option);
      // filter what to send with payload
      const userdata = {
        username: res.data.message.userdata.username,
        role_id: res.data.message.userdata.role_id,
        kd_pegawai: res.data.message.userdata.kd_pegawai
      };

      // Set userData and Token to localStorage
      localStorage.setItem(ApiConfig.LS.USERDATA, JSON.stringify(userdata));
      localStorage.setItem(ApiConfig.LS.TOKEN, res.data.message.token);

      dispatch({
        type: CURRENT_USER,
        payload: userdata,
        status: res.data.code,
        message: res.data.message
      });
      // redirect to dashboard
      window.location.href = "/dashboard";
    } catch (err) {
      dispatch({
        type: CURRENT_USER,
        status: err.response.data.code,
        message: err.response.data.message
      });
    }
  })();
};

export const RePassword = body => dispatch => {
  let token = localStorage.getItem(ApiConfig.LS.TOKEN);
  let option = {
    url:
      ApiConfig.BASE_URL +
      ApiConfig.ENDPOINTS.LOGIN.REPASS +
      "/" +
      body.username,
    method: "put",
    headers: {
      Authorization: token,
      "Content-Type": "application/json"
    },
    data: body
  };
  axios(option)
    .then(res => {
      dispatch({
        type: REPASS_USER,
        payload: body,
        status: res.data.code
      });
      localStorage.clear();
      window.location.href = "/dashboard";
    })
    .catch(err => {
      dispatch({
        type: REPASS_USER,
        status: err.response.data.code,
        message: err.response.data.message
      });
    });
};
