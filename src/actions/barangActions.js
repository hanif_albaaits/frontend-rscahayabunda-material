import axios from "axios";
import {
  GET_BARANG,
  GET_BARANG_ONE,
  GET_CARI_BARANG,
  GET_BARANG_SUM,
  GET_KELUAR_SUM,
  ADD_BARANG,
  DEL_BARANG,
  PUT_BARANG
} from "./types"; //, CREATE_BARANG, DELETE_BARANG
import ApiConfig from "../config/api.config.json";

let token = localStorage.getItem(ApiConfig.LS.TOKEN);

export const getBarang = () => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.BARANG,
    method: "get",
    headers: {
      Authorization: token
    }
  };
  axios(options)
    .then(res => {
      dispatch({
        type: GET_BARANG,
        payload: res.data.message,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: GET_BARANG,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const getBarang1 = kd_barang => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.BARANG + "/" + kd_barang,
    method: "get",
    headers: {
      Authorization: token
    }
  };
  axios(options)
    .then(res => {
      dispatch({
        type: GET_BARANG_ONE,
        payload: res.data.message,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: GET_BARANG_ONE,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const getCariB = body => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.BARANG + "-cari",
    method: "post",
    headers: {
      Authorization: token
    },
    data: body
  };
  axios(options)
    .then(res => {
      dispatch({
        type: GET_CARI_BARANG,
        payload: res.data.message,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: GET_CARI_BARANG,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const getBarangSum = () => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.BARANG + "-sum",
    method: "get",
    headers: {
      Authorization: token
    }
  };
  axios(options)
    .then(res => {
      dispatch({
        type: GET_BARANG_SUM,
        payload: res.data.message,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: GET_BARANG_SUM,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const getBarangKeluarSum = () => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.BARANG + "-keluar-sum",
    method: "get",
    headers: {
      Authorization: token
    }
  };
  axios(options)
    .then(res => {
      dispatch({
        type: GET_KELUAR_SUM,
        payload: res.data.message,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: GET_KELUAR_SUM,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const barangCreate = body => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.BARANG,
    method: "post",
    headers: {
      Authorization: token
    },
    data: body
  };
  axios(options)
    .then(res => {
      dispatch({
        type: ADD_BARANG,
        payload: body,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: ADD_BARANG,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const barangUpdate = body => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.BARANG + "/" + body.kd_barang,
    method: "put",
    headers: {
      Authorization: token
    },
    data: body
  };
  axios(options)
    .then(res => {
      dispatch({
        type: PUT_BARANG,
        payload: body,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: PUT_BARANG,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const barangDelete = body => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.BARANG + "/" + body.kd_barang,
    method: "delete",
    headers: {
      Authorization: token
    },
    data: body
  };
  axios(options)
    .then(res => {
      dispatch({
        type: DEL_BARANG,
        payload: body.kd_barang,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: DEL_BARANG,
        payload: error,
        status: error.response.data.code
      });
    });
};
