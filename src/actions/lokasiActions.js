import axios from "axios";
import {
  GET_LOKASI,
  GET_LOKASI_ONE,
  ADD_LOKASI,
  DEL_LOKASI,
  PUT_LOKASI
} from "./types";
import ApiConfig from "../config/api.config.json";

let token = localStorage.getItem(ApiConfig.LS.TOKEN);

export const getLokasi = () => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.LOKASI,
    method: "get",
    headers: {
      Authorization: token
    }
  };
  axios(options)
    .then(res => {
      dispatch({
        type: GET_LOKASI,
        payload: res.data.message,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: GET_LOKASI,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const getLokasi1 = code => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.LOKASI + "/" + code,
    method: "get",
    headers: {
      Authorization: token
    }
  };
  axios(options)
    .then(res => {
      dispatch({
        type: GET_LOKASI_ONE,
        payload: res.data.message,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: GET_LOKASI_ONE,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const lokasiCreate = body => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.LOKASI,
    method: "post",
    headers: {
      Authorization: token
    },
    data: body
  };
  axios(options)
    .then(res => {
      dispatch({
        type: ADD_LOKASI,
        payload: body,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: ADD_LOKASI,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const lokasiUpdate = body => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.LOKASI + "/" + body.code,
    method: "put",
    headers: {
      Authorization: token
    },
    data: body
  };
  axios(options)
    .then(res => {
      dispatch({
        type: PUT_LOKASI,
        payload: body,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: PUT_LOKASI,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const lokasiDelete = body => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.LOKASI + "/" + body.code,
    method: "delete",
    headers: {
      Authorization: token
    },
    data: body
  };
  axios(options)
    .then(res => {
      dispatch({
        type: DEL_LOKASI,
        payload: body.code,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: DEL_LOKASI,
        payload: error,
        status: error.response.data.code
      });
    });
};
