import axios from "axios";
import {
  GET_SERVIS,
  GET_SERVIS_ONE,
  GET_SERVIS_BRG,
  ADD_SERVIS,
  DEL_SERVIS,
  PUT_SERVIS
} from "./types"; //, CREATE_SERVIS, DELETE_SERVIS
import ApiConfig from "../config/api.config.json";

let token = localStorage.getItem(ApiConfig.LS.TOKEN);

export const getServis = () => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.SERVISBRG.SERVISBRG,
    method: "get",
    headers: {
      Authorization: token
    }
  };
  axios(options)
    .then(res => {
      dispatch({
        type: GET_SERVIS,
        payload: res.data.message,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: GET_SERVIS,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const getServis1 = kd_servis => dispatch => {
  let options = {
    url:
      ApiConfig.BASE_URL +
      ApiConfig.ENDPOINTS.SERVISBRG.SERVISBRG +
      "/" +
      kd_servis,
    method: "get",
    headers: {
      Authorization: token
    }
  };
  axios(options)
    .then(res => {
      dispatch({
        type: GET_SERVIS_ONE,
        payload: res.data.message,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: GET_SERVIS_ONE,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const getServisBrg = kd_barang => dispatch => {
  let options = {
    url:
      ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.SERVISBRG.BRG + "/" + kd_barang,
    method: "get",
    headers: {
      Authorization: token
    }
  };
  axios(options)
    .then(res => {
      dispatch({
        type: GET_SERVIS_BRG,
        payload: res.data.message,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: GET_SERVIS_BRG,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const servisCreate = body => dispatch => {
  let options = {
    url: ApiConfig.BASE_URL + ApiConfig.ENDPOINTS.SERVISBRG.SERVISBRG,
    method: "post",
    headers: {
      Authorization: token
    },
    data: body
  };
  axios(options)
    .then(res => {
      dispatch({
        type: ADD_SERVIS,
        payload: body,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: ADD_SERVIS,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const servisUpdate = body => dispatch => {
  let options = {
    url:
      ApiConfig.BASE_URL +
      ApiConfig.ENDPOINTS.SERVISBRG.SERVISBRG +
      "/" +
      body.kd_servis,
    method: "put",
    headers: {
      Authorization: token
    },
    data: body
  };
  axios(options)
    .then(res => {
      dispatch({
        type: PUT_SERVIS,
        payload: body,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: PUT_SERVIS,
        payload: error,
        status: error.response.data.code
      });
    });
};

export const servisDelete = body => dispatch => {
  let options = {
    url:
      ApiConfig.BASE_URL +
      ApiConfig.ENDPOINTS.SERVISBRG.SERVISBRG +
      "/" +
      body.kd_servis,
    method: "delete",
    headers: {
      Authorization: token
    },
    data: body
  };
  axios(options)
    .then(res => {
      dispatch({
        type: DEL_SERVIS,
        payload: body.kd_servis,
        status: res.data.code
      });
    })
    .catch(error => {
      dispatch({
        type: DEL_SERVIS,
        payload: error,
        status: error.response.data.code
      });
    });
};
