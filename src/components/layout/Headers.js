import React from "react";
import PropTypes from "prop-types";
import apiconfig from "../../config/api.config.json";
import { connect } from "react-redux";

import { getCariB } from "../../actions/barangActions";
import { getCariP } from "../../actions/barangPinjamActions";
import moment from "moment";

class Header extends React.Component {
  constructor(props) {
    super(props);
    let userdata = JSON.parse(localStorage.getItem(apiconfig.LS.USERDATA));
    this.state = {
      userdata: userdata,
      notifExpired: [],
      notifPinjaman: [],
      ROLE: null
    };
  }

  onSignOut = () => {
    localStorage.clear();
    window.location.href = "/login";
    //  this.props.history.push("/login");
  };

  componentDidMount() {
    if (this.state.userdata !== null) {
      let role = this.state.userdata.role_id;
      if (role === "RL0001" || role === "RL0002") {
        // console.log("admin");
        this.getCariB();
        this.getCariPadmin();
        this.setState({
          ROLE: true
        });
      } else {
        // console.log("user");
        //this.getCariB();
        this.getCariPuser();
        this.setState({
          ROLE: false
        });
      }
    }
  }

  getCariB = () => {
    let cari = {};
    cari["is_delete"] = false;
    cari["tanggal_expired"] = moment().format("YYYY-MM-DD");
    this.props.getCariB(cari);
  };

  getCariPadmin = () => {
    let cari = {};
    cari["lama_peminjaman"] = moment().format("YYYY-MM-DD");
    cari["is_delete"] = false;
    cari["status"] = 1;
    this.props.getCariP(cari);
  };

  getCariPuser = () => {
    let cari = {};
    cari["kd_peminjam"] = this.state.userdata.kd_pegawai;
    cari["lama_peminjaman"] = moment().format("YYYY-MM-DD");
    cari["is_delete"] = false;
    cari["status"] = 1;
    this.props.getCariP(cari);
  };

  componentWillReceiveProps(newProps) {
    let cariExpired = newProps.ambilBarang.cari;
    let cariPinjaman = newProps.ambilPinjam.cari;
    if (cariExpired !== null && cariPinjaman !== null) {
      this.setState({
        notifExpired: cariExpired,
        notifPinjaman: cariPinjaman
      });
    }
  }

  render() {
    const { notifExpired, notifPinjaman, userdata } = this.state;
    // const akses = this.state.ROLE;
    var nama;
    var role;
    // var kd_pegawai;
    const number = notifExpired.length + notifPinjaman.length;
    if (userdata !== null) {
      nama = userdata.username;
      role = userdata.role_id;
      // kd_pegawai = userdata.kd_pegawai;
    }

    return (
      <nav
        className="site-navbar navbar navbar-default navbar-fixed-top navbar-mega"
        role="navigation"
      >
        <div className="navbar-header">
          {/* <button
            type="button"
            className="navbar-toggler hamburger hamburger-close navbar-toggler-left hided"
            data-toggle="menubar"
          >
            <span className="sr-only">Toggle navigation</span>
            <span className="hamburger-bar" />
          </button> */}
          {/* <button
            type="button"
            className="navbar-toggler collapsed"
            data-target="#site-navbar-collapse"
            data-toggle="collapse"
          >
            <i className="icon md-more" aria-hidden="true" />
          </button> */}
          <div
            // className="navbar-brand navbar-brand-center site-gridmenu-toggle"
            className="navbar-brand navbar-brand-center"
            // data-toggle="gridmenu"
          >
            <img
              className="navbar-brand-logo"
              src="../images/Logo Icon.png"
              title="Remark"
              alt="profilpic"
            />
            <span className="navbar-brand-text hidden-xs-down">
              RS Cahaya Bunda
            </span>
          </div>
        </div>

        <div className="navbar-container container-fluid">
          <div
            className="collapse navbar-collapse navbar-collapse-toolbar"
            // id="site-navbar-collapse"
          >
            <ul className="nav navbar-toolbar">
              <li className="nav-item hidden-float" id="toggleMenubar">
                <a
                  className="nav-link"
                  data-toggle="menubar"
                  href="/dashboard"
                  role="button"
                >
                  <i className="icon hamburger hamburger-arrow-left">
                    <span className="sr-only">Toggle menubar</span>
                    <span className="hamburger-bar" />
                  </i>
                </a>
              </li>
            </ul>

            <ul className="nav navbar-toolbar navbar-right navbar-toolbar-right">
              <li className="nav-item dropdown">
                <a
                  className="nav-link"
                  data-toggle="dropdown"
                  href="/dashboard"
                  title="Notifications"
                  aria-expanded="false"
                  data-animation="scale-up"
                  role="button"
                >
                  <i className="icon md-notifications" aria-hidden="false" />
                  <span className="badge badge-pill badge-danger">
                    {number}
                  </span>
                </a>
                <div
                  className="dropdown-menu dropdown-menu-right dropdown-menu-media"
                  role="menu"
                >
                  <div className="dropdown-menu-header">
                    <h5>NOTIFICATIONS</h5>
                  </div>

                  <div className="list-group">
                    <div data-role="container">
                      <div data-role="content">
                        {notifExpired.map((content, index) => {
                          return (
                            <a
                              className="list-group-item dropdown-item"
                              href="/barang"
                              role="menuitem"
                              key={index}
                            >
                              <div className="media">
                                <div className="pr-10">
                                  <i
                                    className="icon md-receipt bg-red-600 white icon-circle"
                                    aria-hidden="true"
                                  />
                                </div>
                                <div className="media-body">
                                  <h6 className="media-heading">
                                    {content.kd_barang} {content.nama}
                                  </h6>
                                  <time
                                    className="media-meta"
                                    dateTime="2017-06-12T20:50:48+08:00"
                                  >
                                    Kadaluarsa {content.tanggal_expired}
                                  </time>
                                </div>
                              </div>
                            </a>
                          );
                        })}
                        {notifPinjaman.map((content, index) => {
                          return (
                            <a
                              className="list-group-item dropdown-item"
                              href="/barang"
                              role="menuitem"
                              key={index}
                            >
                              <div className="media">
                                <div className="pr-10">
                                  <i
                                    className="icon md-receipt bg-red-600 white icon-circle"
                                    aria-hidden="true"
                                  />
                                </div>
                                <div className="media-body">
                                  <h6 className="media-heading">
                                    {content.kd_pinjam} {content.nama_barang}
                                  </h6>
                                  <div>Peminjam : {content.nama_pegawai}</div>
                                  <time
                                    className="media-meta"
                                    dateTime="2017-06-12T20:50:48+08:00"
                                  >
                                    Batas Tanggal Pengembalian{" "}
                                    {content.lama_peminjaman}
                                  </time>
                                </div>
                              </div>
                            </a>
                          );
                        })}
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              <li className="nav-item dropdown">
                <a
                  className="nav-link navbar-avatar"
                  data-toggle="dropdown"
                  href="/dashboard"
                  aria-expanded="false"
                  data-animation="scale-up"
                  role="button"
                >
                  <span className="avatar avatar-online">
                    <img src="../../images/6.jpg" alt="..." />
                    <i />
                  </span>
                </a>
                <div className="dropdown-menu" role="menu">
                  <a
                    className="dropdown-item"
                    href="/dashboard"
                    role="menuitem"
                  >
                    <i className="icon md-account" aria-hidden="true" /> {nama}{" "}
                    {role}
                  </a>
                  <a className="dropdown-item" href="/settings" role="menuitem">
                    <i className="icon md-settings" aria-hidden="true" />
                    Settings
                  </a>
                  <div className="dropdown-divider" role="presentation" />
                  <a
                    className="dropdown-item"
                    href="/login"
                    role="menuitem"
                    onClick={this.onSignOut}
                  >
                    <i className="icon md-power" aria-hidden="true" /> Logout
                  </a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}

Header.propTypes = {
  getCariB: PropTypes.func.isRequired,
  getCariP: PropTypes.func.isRequired,
  ambilBarang: PropTypes.object.isRequired,
  ambilPinjam: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  ambilBarang: state.barangReducers,
  ambilPinjam: state.pinjamBReducers
});

export default connect(
  mapStateToProps,
  { getCariB, getCariP }
)(Header);
