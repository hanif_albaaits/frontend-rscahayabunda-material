import React from "react";

class NotFound extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="text-center">
        <h1>404 NOT FOUND</h1>
      </div>
    );
  }
}

export default NotFound;
