import React from "react";
import PropTypes from "prop-types";
import apiconfig from "../../config/api.config.json";

// import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { getMenuSide } from "../../actions/menuActions";

class Sidebar extends React.Component {
  constructor(props) {
    super(props);
    let userdata = JSON.parse(localStorage.getItem(apiconfig.LS.USERDATA));
    this.state = {
      userdata: userdata,
      allMenu: []
    };
  }

  componentDidMount() {
    if (this.state.userdata !== null) {
      let role = this.state.userdata.role_id;
      this.props.getMenuSide(role);
    }
  }

  componentWillReceiveProps(newProps) {
    this.setState({
      allMenu: newProps.ambilMenu.side
    });
  }

  render() {
    const menu = this.state.allMenu;

    return (
      <div className="site-menubar">
        <div className="site-menubar-body">
          <div>
            <div>
              <ul className="site-menu" data-plugin="menu">
                <li className="site-menu-category">General</li>
                <li className="site-menu-item ">
                  <a className="animsition-link" href="/dashboard">
                    <i
                      className="site-menu-icon md-view-dashboard"
                      aria-hidden="true"
                    />
                    <span className="site-menu-title">Dashboard</span>
                  </a>
                </li>
                <li className="site-menu-category">Menu</li>

                {menu.map((ele, index) => {
                  let parent = [];
                  if (ele.parent === null) {
                    parent.push(
                      <li className="site-menu-item has-sub" key={index}>
                        <a>
                          <i
                            className="site-menu-icon md-google-pages"
                            aria-hidden="true"
                          />
                          <span className="site-menu-title">
                            {ele.menu_name}
                          </span>
                          <span className="site-menu-arrow" />
                        </a>
                        <ul className="site-menu-sub" id={ele.menu_id}>
                          {menu.map((row, index) => {
                            let child = [];
                            if (
                              row.parent === ele.menu_id &&
                              row.status === 1
                            ) {
                              child.push(
                                <li className="site-menu-item" key={index}>
                                  <a
                                    className="animsition-link"
                                    href={row.controller}
                                  >
                                    <span className="site-menu-title">
                                      {row.menu_name}
                                    </span>
                                  </a>
                                </li>
                              );
                            }
                            return child;
                          })}
                        </ul>
                      </li>
                    );
                  }
                  return parent;
                })}
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Sidebar.propTypes = {
  getMenuSide: PropTypes.func.isRequired,
  ambilMenu: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  ambilMenu: state.menuReducers
});

export default connect(
  mapStateToProps,
  { getMenuSide }
)(Sidebar);
