import React from "react";

class Footer extends React.Component {
  render() {
    return (
      <footer className="site-footer">
        <div className="site-footer-right">
          @2019 Copyright{" "}
          <a href="http://www.gratika.co.id/website/">
            Graha Informatika Nusantara
          </a>
        </div>
      </footer>
    );
  }
}

export default Footer;
