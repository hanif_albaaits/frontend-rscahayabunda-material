import React from "react";
import { Alert } from "reactstrap";
import { UserLogin } from "../../actions/loginActions";
import { connect } from "react-redux";
import PropTypes from "prop-types";

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      formdata: {
        username: "",
        password: ""
      },
      status: "",
      message: "",
      userdata: {},
      isRequest: false,
      validasi: {
        status: false,
        message: ""
      }
    };
    this.onSignIn = this.onSignIn.bind(this);
    this.textChanged = this.textChanged.bind(this);
  }
  textChanged(e) {
    let tmp = this.state.formdata;
    tmp[e.target.name] = e.target.value;
    this.setState({
      formdata: tmp,
      status: "",
      message: "",
      validasi: {
        status: false,
        message: ""
      }
    });
  }

  handlePress = target => {
    if (target.charCode === 13) {
      this.onSignIn();
    }
  };

  onSignIn() {
    const { username, password } = this.state.formdata;
    if (username === "" || password === "") {
      this.setState({
        validasi: {
          status: true,
          message: "Please fill form.."
        }
      });
    } else {
      this.setState({
        isRequest: true
      });
      this.props.UserLogin(this.state.formdata);
      this.setState({
        isRequest: false
      });
    }
  }

  componentWillReceiveProps(newProps) {
    this.setState({
      status: newProps.auth.status,
      message: newProps.auth.message,
      userdata: newProps.auth.user
    });
  }

  render() {
    const { status, message, validasi, formdata } = this.state;

    return (
      <div className="animsition page-login-v3 layout-full">
        <div
          className="page vertical-align text-center"
          data-animsition-in="fade-in"
          data-animsition-out="fade-out"
        >
          >
          <div className="page-content vertical-align-middle">
            <div className="panel">
              <div className="panel-body">
                <div className="brand">
                  <div>
                    <img
                      className="brand-img"
                      src="../../images/logo.png"
                      alt="logo-cahayabunda"
                      style={{ width: "300px" }}
                    />
                  </div>
                </div>
                <form onSubmit={this.onSignIn} autoComplete="off">
                  <div className="form-group form-material floating">
                    <input
                      type="text"
                      className="form-control"
                      id="username"
                      name="username"
                      value={formdata.username}
                      onChange={this.textChanged}
                      onKeyPress={this.handlePress}
                    />
                    <label className="floating-label">Username</label>
                  </div>
                  <div className="form-group form-material floating">
                    <input
                      id="password"
                      type="password"
                      name="password"
                      className="form-control"
                      value={formdata.password}
                      onChange={this.textChanged}
                      onKeyPress={this.handlePress}
                    />
                    <label className="floating-label">Password</label>
                  </div>
                  {status === 404 ? (
                    <Alert color="danger">{message} </Alert>
                  ) : (
                    ""
                  )}
                  {validasi.status === true ? (
                    <Alert color="danger">{validasi.message} </Alert>
                  ) : (
                    ""
                  )}
                  {/* <div className="form-group clearfix">
                    {/* <div className="checkbox-custom checkbox-inline checkbox-primary checkbox-lg float-left">
                      <input
                        type="checkbox"
                        id="inputCheckbox"
                        name="remember"
                      />
                      <label htmlFor="inputCheckbox">Remember me</label>
                    </div>
                    <a className="float-right" href="forgot-password.html">
                      Forgot password?
                    </a>                    
                  </div> */}
                  <button
                    type="button"
                    disabled={this.state.isRequest}
                    className="btn btn-primary btn-block btn-lg mt-40"
                    onClick={this.onSignIn}
                  >
                    Sign In
                  </button>
                </form>
                {/* <p>
                  Still no account? Please go to{" "}
                  <a href="register-v3.html">Sign up</a>
                </p> */}
              </div>
            </div>

            <footer className="page-copyright page-copyright-inverse">
              <p>WEBSITE BY Graha Infromatika Nusantara</p>
              <p>© 2019. All RIGHT RESERVED.</p>
              <div className="social">
                {/* <a className="btn btn-icon btn-pure" href="javascript:void(0)">
                  <i className="icon bd-twitter" aria-hidden="true" />
                </a>
                <a className="btn btn-icon btn-pure" href="javascript:void(0)">
                  <i className="icon bd-facebook" aria-hidden="true" />
                </a>
                <a className="btn btn-icon btn-pure" href="javascript:void(0)">
                  <i className="icon bd-google-plus" aria-hidden="true" />
                </a> */}
              </div>
            </footer>
          </div>
        </div>
      </div>
    );
  }
}

Login.propTypes = {
  UserLogin: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.loginReducers
});

export default connect(
  mapStateToProps,
  { UserLogin }
)(Login);
