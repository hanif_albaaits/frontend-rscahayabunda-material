import React, { Component, Fragment } from "react";
import { Switch } from "react-router-dom";
import { connect } from "react-redux";
import { getMenuSide } from "../actions/menuActions";

import { Redirect } from "react-router";
import PropTypes from "prop-types";
import apiconfig from "../config/api.config.json";
//page component
import Home from "./content/home/home";
import Setting from "./content/home/pengaturan";
import Maintenance from "./content/maintenace";
import listUser from "./content/user/listUser";
import listRole from "./content/role/listRole";
import listDivisi from "./content/divisi/listDivisi";
import listLokasi from "./content/lokasi/listLokasi";
import listMenu from "./content/menu/listMenu";
import listPegawai from "./content/pegawai/listPegawai";
import listBarang from "./content/barang/listBarang";
import listBarangA from "./content/barangAmbil/listBarangA";
import listBarangP from "./content/barangPinjam/listBarangP";
import listBarangS from "./content/barangServis/listBarangS";
import listBarangT from "./content/barangTambah/listBarangT";
import BarangView from "./content/barangView/barangView";

import PrivateRoute from "./PrivateRoute";

class DashboardSwitcher extends Component {
  constructor(props) {
    super(props);
    let userdata = JSON.parse(localStorage.getItem(apiconfig.LS.USERDATA));
    let token = localStorage.getItem(apiconfig.LS.TOKEN);
    this.state = {
      userdata: userdata,
      token: token,
      dataAccess: []
    };
  }

  // componentDidMount() {
  //   if (this.state.userdata !== null) {
  //     let role = this.state.userdata.role_id;
  //     this.props.getMenuSide(role);
  //   }
  // }

  ambilnoparent = data => {
    let temp = [];
    data.map(ele => {
      if (ele.parent !== null && ele.status === 1) {
        temp.push(ele);
      }
      return temp;
    });
    this.setState({
      dataAccess: temp
    });
  };

  componentWillReceiveProps(propsData) {
    this.ambilnoparent(propsData.ambilAccess.side);
  }

  func = url => {
    switch (url) {
      case "/user":
        return listUser;
      case "/pegawai":
        return listPegawai;
      case "/role":
        return listRole;
      case "/divisi":
        return listDivisi;
      case "/lokasi":
        return listLokasi;
      case "/menu":
        return listMenu;
      case "/barang":
        return listBarang;
      case "/brg-ambil":
        return listBarangA;
      case "/brg-pinjam":
        return listBarangP;
      case "/brg-servis":
        return listBarangS;
      case "/brg-tambah":
        return listBarangT;
      default:
        return Home;
    }
  };

  tidakadaakses = () => {
    return <Redirect to="/dashboard" />;
  };

  render() {
    const dataAccess = this.state.dataAccess;

    return (
      <Fragment>
        <Switch>
          <PrivateRoute path="/dashboard" component={Home} />
          <PrivateRoute path="/settings" component={Setting} />
          <PrivateRoute path="/contact" component={Maintenance} />
          <PrivateRoute path="/about" component={Maintenance} />
          <PrivateRoute path="/barangview" component={BarangView} />

          {dataAccess.map((content, index) => {
            return (
              <PrivateRoute
                key={index}
                path={content.controller}
                component={this.func(content.controller)}
              />
            );
          })}
          {this.state.token == null ? <Redirect to="/" /> : ""}
          {this.state.token !== null && dataAccess.length > 0
            ? this.tidakadaakses()
            : ""}
        </Switch>
      </Fragment>
    );
  }
}

DashboardSwitcher.propTypes = {
  getMenuSide: PropTypes.func.isRequired,
  ambilAccess: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  ambilAccess: state.menuReducers
});

export default connect(
  mapStateToProps,
  { getMenuSide }
)(DashboardSwitcher);

// export default DashboardSwitcher;
