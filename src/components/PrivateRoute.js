import React from "react";
import { Route } from "react-router-dom";
import { Redirect } from "react-router";
import apiconfig from "../config/api.config.json";

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      localStorage.getItem(apiconfig.LS.TOKEN) != null ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: "/dashboard",
            state: { from: props.location }
          }}
        />
      )
    }
  />
);

export default PrivateRoute;
