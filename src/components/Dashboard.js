import React from "react";
import Sidebar from "./layout/Sidebar";
import Header from "./layout/Headers";
import Footer from "./layout/Footer";
import DashboardSwitcher from "./DashboardSwitcher";

class Dashboard extends React.Component {
  render() {
    return (
      <div>
        <Header />
        <Sidebar />
        <div className="page">
          <DashboardSwitcher />
        </div>
        <Footer />
      </div>
    );
  }
}

export default Dashboard;
