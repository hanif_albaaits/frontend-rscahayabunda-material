const bacaAkses = (data, url) => {
  let allAccess = data;
  let location = url;
  let hasil = {};
  allAccess.map(content => {
    if (content.controller === location && content.status === 1) {
      return (hasil = content);
    }
    return hasil;
  });
  return hasil;
};

export default bacaAkses;
