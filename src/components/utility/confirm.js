import React from "react";
import { Modal, ModalBody, ModalFooter, ModalHeader, Button } from "reactstrap";

class Confirm extends React.Component {
  render() {
    return (
      <Modal
        isOpen={this.props.showKonfirmasi}
        className="modal-dialog modal-sm"
      >
        <ModalHeader className="">Alert</ModalHeader>
        <ModalBody>
          <p>Anda Yakin ? </p>
        </ModalBody>
        <ModalFooter>
          <Button
            color="primary"
            type="submit"
            onClick={this.props.submitHandler}
          >
            Ya
          </Button>
          <Button color="danger" onClick={this.props.ubahKonfirmasi}>
            Tidak
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

export default Confirm;
