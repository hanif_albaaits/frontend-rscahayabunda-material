import React, { Component } from "react";
import { Pie } from "react-chartjs-2";

class Chart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      chartData: props.chartData
    };
  }

  static defaultProps = {
    displayTitle: true,
    displayLegend: true,
    legendPosition: "right"
  };

  render() {
    return (
      <div className="chart">
        <Pie
          data={this.props.chartData}
          options={{
            maintainAspectRatio: false,
            title: {
              display: this.props.displayTitle,
              text: this.props.judul,
              fontSize: 20
            },
            legend: {
              display: this.props.displayLegend,
              position: this.props.legendPosition
            }
          }}
          height={300}
          width={300}
        />
      </div>
    );
  }
}

export default Chart;
