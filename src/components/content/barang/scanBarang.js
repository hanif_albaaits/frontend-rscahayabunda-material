import React from "react";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button,
  Alert
} from "reactstrap";

class ScanBarang extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      barcode: "",
      alertData: {
        status: false,
        message: ""
      }
    };
  }

  changeHandler = e => {
    let temp = e.target.value;
    this.setState({
      barcode: temp
    });
    if (temp.length >= 13) {
      this.scanHandler(temp);
    }
  };

  validasiBarcode(barcode) {
    // console.log("ini validasi " + barcode);
    let regex = new RegExp(/^BRG[0-9]{10}/);
    return regex.test(String(barcode));
    /*String barcode validasi/*/
  }

  reloadhalaman = () => {
    this.props.closeModalHandler();
    window.location.href = "/barang";
  };

  scanHandler = code => {
    if (this.validasiBarcode(code) === true) {
      window.location.href = "/barangview?code=" + code;
      this.setState({
        barcode: code
      });
    } else {
      this.setState({
        barcode: code,
        alertData: {
          status: true,
          message: "Barcode Anda tidak sesuai dengan ketentuan !"
        }
      });
    }
  };

  scanLagi = () => {
    this.setState({
      barcode: "",
      alertData: {
        status: false
      }
    });
  };

  render() {
    console.log(this.state.barcode);
    return (
      <Modal isOpen={this.props.scan}>
        <ModalHeader> Scan Barang </ModalHeader>
        <ModalBody>
          <p>Silahkan Scan barcode Barang</p>
          <div className="text-center">
            <h2>{this.state.barcode}</h2>
          </div>
          {/* <Spinner /> */}
          {/* <br /> */}
          {this.state.alertData.status === true ? (
            <Alert color="danger">{this.state.alertData.message} </Alert>
          ) : (
            ""
          )}
          <input
            type="text"
            className="input-scan"
            // className="form-control"
            autofocus="true"
            name="barcode"
            onChange={this.changeHandler}
            value={this.state.barcode}
            autoComplete="off"
          />
        </ModalBody>
        <ModalFooter>
          <Button color="danger" onClick={this.reloadhalaman}>
            Tutup
          </Button>
          {/* <Button color="primay" onClick={this.scanLagi}>
            Coba lagi
          </Button> */}
        </ModalFooter>
      </Modal>
    );
  }
}

export default ScanBarang;
