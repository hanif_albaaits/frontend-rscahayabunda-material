import React, { Fragment } from "react";
import { Alert } from "reactstrap";
import { getBarang } from "../../../actions/barangActions";
import { getLokasi } from "../../../actions/lokasiActions";

import { connect } from "react-redux";
import EditBarang from "./editBarang";
import CreateBarang from "./createBarang";
import DeleteBarang from "./deleteBarang";
import ScanBarang from "./scanBarang";

import printTable from "../../utility/printTable";
import exportCsv from "../../utility/exportCsv";
import bacaAkses from "../../utility/bacaAkses";
import Spinner from "../../utility/spinnerTable";

import PropTypes from "prop-types";
import moment from "moment";

import IconButton from "@material-ui/core/IconButton";
import FirstPageIcon from "@material-ui/icons/FirstPage";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import LastPageIcon from "@material-ui/icons/LastPage";
import TablePagination from "@material-ui/core/TablePagination";
import { withStyles } from "@material-ui/core/styles";

class ListBarang extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      formCari: {
        kd_barang: /(?:)/,
        kd_barang_rs: /(?:)/,
        nama: /(?:)/
      },
      AKSES: {},
      createBarang: false,
      deleteBarang: false,
      scanBarang: false,
      editBarang: false,
      allBarang: [],
      currentBarang: {},
      alertData: {
        status: 0,
        message: "",
        code: ""
      },
      hasil: [],
      page: 0,
      rowsPerPage: 5
    };
    this.showCreate = this.showCreate.bind(this);
  }
  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  deleteModalHandler = barangid => {
    let tmp = {};
    this.state.allBarang.map(ele => {
      if (barangid === ele._id) {
        tmp = ele;
      }
      return tmp;
    });
    this.setState({
      currentBarang: tmp,
      deleteBarang: true
    });
  };

  editModalHandler = barangid => {
    let tmp = {};
    this.state.allBarang.map(ele => {
      if (barangid === ele._id) {
        tmp = {
          _id: ele._id,
          kd_barang: ele.kd_barang,
          kd_barang_rs: ele.kd_barang_rs,
          kd_lokasi: ele.kd_lokasi,
          nama: ele.nama,
          merk: ele.merk,
          ukuran: ele.ukuran,
          bahan: ele.bahan,
          tahun: ele.tahun,
          no_seri: ele.no_seri,
          harga_beli: ele.harga_beli,
          total: ele.total,
          jumlah: ele.jumlah,
          kondisi: ele.kondisi,
          catatan: ele.catatan,
          tanggal_masuk: ele.tanggal_masuk,
          tanggal_expired: ele.tanggal_expired,
          update_date: ele.update_date,
          update_by: ele.update_by
        };
        this.setState({
          currentBarang: tmp,
          editBarang: true
        });
      }
      return tmp;
    });
  };

  changeHandler = event => {
    let data = event.target.value;
    let char = data.replace(/\\/g, "");
    let tmp = this.state.formCari;
    if (event.target.name) {
      tmp[event.target.name] = new RegExp(char.toLowerCase());
    } else {
      tmp[event.target.name] = char;
    }
    this.cariData();
  };

  cariData = () => {
    const { kd_barang, kd_barang_rs, nama } = this.state.formCari;
    let temp = [];
    this.state.allBarang.map(ele => {
      if (
        kd_barang.test(ele.kd_barang.toLowerCase()) &&
        kd_barang_rs.test(ele.kd_barang_rs.toLowerCase()) &&
        nama.test(ele.nama.toLowerCase())
      ) {
        temp.push(ele);
      }
      return temp;
    });
    this.setState({
      hasil: temp
    });
  };

  closeModalHandler = () => {
    this.setState({
      createBarang: false,
      editBarang: false,
      deleteBarang: false,
      scanBarang: false
    });
  };

  showCreate = () => {
    this.setState({ createBarang: true });
  };

  showScan = () => {
    this.setState({ scanBarang: true });
  };

  componentDidMount() {
    this.props.getBarang();
    this.props.getLokasi();
  }

  componentWillReceiveProps(newProps) {
    let a = bacaAkses(newProps.ambilAccess.side, window.location.pathname);
    let allBarang = newProps.ambil.barang;

    if (a !== null) {
      this.setState({
        AKSES: a
      });
    }
    if (allBarang !== null) {
      this.setState({
        allBarang: allBarang,
        hasil: allBarang,
        loading: false
      });
    }
  }

  modalStatus = (status, message, code) => {
    this.setState({
      alertData: {
        status: status,
        message: message,
        code: code
      },
      editBarang: false,
      deleteBarang: false,
      createBarang: false
    });
    setTimeout(() => {
      window.location.href = "/barang";
    }, 3000);
  };

  changeDateFormat = tanggal => {
    return moment(tanggal).format("DD/MM/YYYY");
  };

  link = (url, param) => {
    return url + param;
  };

  render() {
    const AKSES = this.state.AKSES;
    const loading = this.state.loading;
    let tablelist;

    if (loading === true) {
      tablelist = <Spinner />;
    } else {
      if (this.state.allBarang.length > 0) {
        tablelist = this.state.hasil
          .slice(
            this.state.page * this.state.rowsPerPage,
            this.state.page * this.state.rowsPerPage + this.state.rowsPerPage
          )
          .map((row, index) => {
            return (
              <tr key={index}>
                <td className="align-middle">{row.kd_barang}</td>
                <td className="align-middle">{row.kd_barang_rs}</td>
                <td className="align-middle">{row.lokasi}</td>
                <td className="align-middle">{row.nama}</td>
                <td className="align-middle">{row.tahun}</td>
                <td className="align-middle">{row.jumlah}</td>
                <td className="align-middle text-center">
                  {AKSES.atribut === 1 || AKSES.atribut === 2 ? (
                    <a href={this.link("/barangview?code=", row.kd_barang)}>
                      <i className="icon md-search" aria-hidden="false" />
                    </a>
                  ) : (
                    ""
                  )}
                  {AKSES.atribut === 2 ? (
                    <Fragment>
                      <button
                        type="button"
                        className="btn btn-icon btn-flat btn-default"
                        title="Edit"
                        onClick={() => {
                          this.editModalHandler(row._id);
                        }}
                      >
                        <i className="icon md-edit" aria-hidden="false" />
                      </button>
                      <button
                        type="button"
                        className="btn btn-sm btn-icon btn-flat btn-default"
                        title="Delete"
                        onClick={() => {
                          this.deleteModalHandler(row._id);
                        }}
                      >
                        <i className="icon md-delete" aria-hidden="false" />
                      </button>
                    </Fragment>
                  ) : (
                    ""
                  )}
                </td>
              </tr>
            );
          });
      } else {
        tablelist = (
          <tr className="text-center">
            <td colSpan="10">Tidak Ada data yang tersedia !</td>
          </tr>
        );
      }
    }

    return (
      <div>
        <div className="page-header">
          <h1 className="page-title">Master Barang</h1>
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <a href="/dashboard">Home</a>
            </li>
            <li className="breadcrumb-item">
              <a href="/dashboard">Manajemen Barang</a>
            </li>
            <li className="breadcrumb-item active">Master Barang</li>
          </ol>
          <div className="page-header-actions">
            {AKSES.atribut === 2 ? (
              <div>
                <button
                  type="button"
                  className="btn btn-primary"
                  onClick={this.showCreate}
                >
                  Tambah
                </button>
                <button
                  type="button"
                  className="btn btn-primary mx-sm-1"
                  onClick={this.showScan}
                >
                  Scan
                </button>
              </div>
            ) : (
              <div>
                <button
                  type="button"
                  className="btn btn-primary mx-sm-1"
                  onClick={this.showScan}
                >
                  Scan
                </button>
              </div>
            )}
          </div>
        </div>
        <div className="page-content">
          {this.state.alertData.status === 1 ? (
            <Alert color="success">
              <b>Data {this.state.alertData.message}</b> Data Barang dengan
              referensi <strong>{this.state.alertData.code} </strong>
              telah {this.state.alertData.message}
            </Alert>
          ) : (
            ""
          )}
          {this.state.alertData.status === 2 ? (
            <Alert color="danger">{this.state.alertData.message} </Alert>
          ) : (
            ""
          )}
          <div className="panel">
            <header className="panel-heading">
              <h3 className="panel-title">List Barang</h3>
            </header>
            <div className="panel-body">
              <form className="form-inline">
                <div className="form-group">
                  <input
                    type="text"
                    placeholder="cari kode barang"
                    className="form-control mx-sm-1"
                    name="kd_barang"
                    onChange={this.changeHandler}
                    autoComplete="off"
                  />
                  <input
                    type="text"
                    placeholder="cari berdasarkan kode RS"
                    className="form-control mx-sm-1"
                    name="kd_barang_rs"
                    onChange={this.changeHandler}
                    autoComplete="off"
                  />
                  <input
                    type="text"
                    placeholder="cari nama barang"
                    className="form-control mx-sm-1"
                    name="nama"
                    onChange={this.changeHandler}
                    autoComplete="off"
                  />
                </div>
              </form>
              <div className="example table-responsive">
                <table id="table" className="table table-bordered">
                  <thead>
                    <tr>
                      <th>Kode Barang</th>
                      <th>No Barang RS</th>
                      <th>Lokasi</th>
                      <th>Nama</th>
                      <th>Tahun Perolehan</th>
                      <th>Jumlah di Gudang</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>{tablelist}</tbody>
                  <TablePagination
                    colSpan={7}
                    count={this.state.hasil.length}
                    rowsPerPage={this.state.rowsPerPage}
                    page={this.state.page}
                    onChangePage={this.handleChangePage}
                    onChangeRowsPerPage={this.handleChangeRowsPerPage}
                    ActionsComponent={TablePaginationActionsWrapped}
                  />
                </table>
                <button
                  type="button"
                  className="btn btn-primary mx-sm-1"
                  onClick={() => {
                    printTable("table");
                  }}
                  hidden={true}
                >
                  Print
                </button>
                <button
                  type="button"
                  className="btn btn-primary mx-sm-1"
                  onClick={() => {
                    exportCsv("m_barang");
                  }}
                >
                  Export to CSV
                </button>
                <CreateBarang
                  create={this.state.createBarang}
                  closeModalHandler={this.closeModalHandler}
                  modalStatus={this.modalStatus}
                  allBarang={this.state.allBarang}
                />
                <DeleteBarang
                  delete={this.state.deleteBarang}
                  barang={this.state.currentBarang}
                  closeModalHandler={this.closeModalHandler}
                  modalStatus={this.modalStatus}
                />
                <ScanBarang
                  scan={this.state.scanBarang}
                  closeModalHandler={this.closeModalHandler}
                />
                <EditBarang
                  edit={this.state.editBarang}
                  closeModalHandler={this.closeModalHandler}
                  currentBarang={this.state.currentBarang}
                  kd_barang_rs={this.state.currentBarang.kd_barang_rs}
                  modalStatus={this.modalStatus}
                  allBarang={this.state.allBarang}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ListBarang.propTypes = {
  getBarang: PropTypes.func.isRequired,
  getLokasi: PropTypes.func.isRequired,
  ambil: PropTypes.object.isRequired,
  ambilAccess: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  ambil: state.barangReducers,
  ambilAccess: state.menuReducers
});

export default connect(
  mapStateToProps,
  { getBarang, getLokasi }
)(ListBarang);

const actionsStyles = theme => ({
  root: {
    flexShrink: 0,
    color: theme.palette.text.secondary,
    marginLeft: theme.spacing.unit * 2.5
  }
});

class TablePaginationActions extends React.Component {
  handleFirstPageButtonClick = event => {
    this.props.onChangePage(event, 0);
  };

  handleBackButtonClick = event => {
    this.props.onChangePage(event, this.props.page - 1);
  };

  handleNextButtonClick = event => {
    this.props.onChangePage(event, this.props.page + 1);
  };

  handleLastPageButtonClick = event => {
    this.props.onChangePage(
      event,
      Math.max(0, Math.ceil(this.props.count / this.props.rowsPerPage) - 1)
    );
  };

  render() {
    const { classes, count, page, rowsPerPage, theme } = this.props;

    return (
      <div className={classes.root}>
        <IconButton
          onClick={this.handleFirstPageButtonClick}
          disabled={page === 0}
          aria-label="First Page"
        >
          {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
        </IconButton>
        <IconButton
          onClick={this.handleBackButtonClick}
          disabled={page === 0}
          aria-label="Previous Page"
        >
          {theme.direction === "rtl" ? (
            <KeyboardArrowRight />
          ) : (
            <KeyboardArrowLeft />
          )}
        </IconButton>
        <IconButton
          onClick={this.handleNextButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="Next Page"
        >
          {theme.direction === "rtl" ? (
            <KeyboardArrowLeft />
          ) : (
            <KeyboardArrowRight />
          )}
        </IconButton>
        <IconButton
          onClick={this.handleLastPageButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="Last Page"
        >
          {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
        </IconButton>
      </div>
    );
  }
}

TablePaginationActions.propTypes = {
  classes: PropTypes.object.isRequired,
  count: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
  theme: PropTypes.object.isRequired
};

const TablePaginationActionsWrapped = withStyles(actionsStyles, {
  withTheme: true
})(TablePaginationActions);
