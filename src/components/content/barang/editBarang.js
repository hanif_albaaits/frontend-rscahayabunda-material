import React from "react";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button,
  Form,
  Alert
} from "reactstrap";
import PropTypes from "prop-types";
import apiconfig from "../../../config/api.config.json";
import moment from "moment";

import Konfirm from "../../utility/confirm";
import { connect } from "react-redux";
import { barangUpdate } from "../../../actions/barangActions";

class EditBarang extends React.Component {
  constructor(props) {
    super(props);
    let userdata = JSON.parse(localStorage.getItem(apiconfig.LS.USERDATA));
    this.state = {
      formdata: {
        kd_barang_rs: "",
        kd_lokasi: "",
        barcode: "",
        nama: "",
        merk: "",
        ukuran: "",
        bahan: "",
        tahun: "",
        no_seri: "",
        harga_beli: "",
        total: "",
        catatan: "",
        kondisi: "",
        tanggal_masuk: "",
        tanggal_expired: ""
      },
      konfirmasi: false,
      updated_by: userdata.username,
      status: "",
      alertData: {
        status: false,
        message: ""
      },
      labelWidth: 0
    };
    this.changeHandler = this.changeHandler.bind(this);
  }

  changeHandler(e) {
    let tmp = this.state.formdata;
    tmp[e.target.name] = e.target.value;
    this.setState({
      alertData: {
        status: false,
        message: ""
      },
      formdata: tmp
    });
  }

  select1 = selectedOption => {
    let tmp = this.state.formdata;
    tmp[selectedOption.target.name] = selectedOption.target.value;
    this.setState({
      formdata: tmp
    });
  };

  validateFilter1(kode) {
    let lama = this.props.kd_barang_rs;
    let allBarang = this.props.allBarang.map(ele => ele.kd_barang_rs);
    let a = allBarang.filter(e => e === kode);
    if (a.length === 0 || kode === lama) {
      return false;
    } else {
      return true;
    }
  }

  validateFilter2(barcode) {
    let lama = this.props.barcode;
    let allBarang = this.props.allBarang.map(ele => ele.barcode);
    let a = allBarang.filter(e => e === barcode);
    if (a.length === 0 || barcode === lama) {
      return false;
    } else {
      return true;
    }
  }

  submitValidasi = () => {
    const {
      kd_barang_rs,
      kd_lokasi,
      barcode,
      nama,
      merk,
      ukuran,
      bahan,
      tahun,
      no_seri,
      harga_beli,
      total,
      catatan,
      kondisi,
      tanggal_masuk
    } = this.state.formdata;
    if (
      kd_barang_rs === "" ||
      kd_lokasi === "" ||
      barcode === "" ||
      nama === "" ||
      merk === "" ||
      ukuran === "" ||
      bahan === "" ||
      tahun === "" ||
      no_seri === "" ||
      harga_beli === "" ||
      total === "" ||
      catatan === "" ||
      kondisi === "" ||
      tanggal_masuk === ""
    ) {
      this.setState({
        alertData: {
          status: true,
          message: "Semua field harus di isi!"
        }
      });
    } else if (this.validateFilter1(kd_barang_rs) === true) {
      this.setState({
        alertData: {
          status: true,
          message: "Ouh, kode barang telah digunakan !"
        }
      });
    } else if (this.validateFilter2(barcode) === true) {
      this.setState({
        alertData: {
          status: true,
          message: "Ouh, barcode telah digunakan !"
        }
      });
    } else {
      this.setState({ konfirmasi: true });
    }
  };

  submitHandler = () => {
    let form = this.state.formdata;
    form["updated_by"] = this.state.updated_by;
    this.props.barangUpdate(this.state.formdata);
    this.konfirmasiTidak();
  };

  componentWillReceiveProps(newProps) {
    this.setState({
      formdata: newProps.currentBarang,
      status: newProps.ambil.statusPUT
    });
  }

  konfirmasiTidak = () => {
    this.setState({ konfirmasi: false });
  };

  ubahKondisi = data => {
    if (data === "1") {
      return "Baik";
    }
    if (data === "2") {
      return "Baik Tidak terpakai";
    }
    if (data === "3") {
      return "Rusak";
    }
  };

  // ubahLokasi = data => {
  //   this.props.ambil1.lokasi.map(row => {
  //     if (data === row.code) {
  //       return row.name;
  //     }
  //     return row.name;
  //   });
  // };

  render() {
    const lokasi = this.props.ambil1.lokasi.map((row, x) => {
      return {
        value: row.code,
        label: row.name
      };
    });

    const kondisi = [
      { value: "1", label: "Baik" },
      { value: "2", label: "Baik Tidak Terpakai" },
      { value: "3", label: "Rusak" }
    ];

    const t_min = moment()
      .add(1, "days")
      .format("YYYY-MM-DD");
    this.state.status === 200
      ? this.props.modalStatus(1, "Di Ubah!", this.state.formdata.nama)
      : console.log(this.state.status);
    return (
      <Modal isOpen={this.props.edit} className="modal-dialog modal-lg">
        <ModalHeader>
          Ubah Data Barang{" " + this.state.formdata.kd_barang}
        </ModalHeader>
        <ModalBody>
          <Konfirm
            showKonfirmasi={this.state.konfirmasi}
            ubahKonfirmasi={this.KonfirmasiTidak}
            submitHandler={this.submitHandler}
          />
          <Form>
            <div className="form-row">
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="kd_barang_rs"
                  className="col-4 col-form-label text-right"
                >
                  Kode Barang RS
                </label>
                <div className="col-8">
                  <input
                    type="text"
                    className="form-control"
                    name="kd_barang_rs"
                    placeholder="kode barang RS"
                    value={this.state.formdata.kd_barang_rs}
                    onChange={this.changeHandler}
                  />
                </div>
              </div>
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="selectlocation"
                  className="col-4 col-form-label text-right"
                >
                  Lokasi
                </label>
                <div className="col-8">
                  <select
                    name="kd_lokasi"
                    className="form-control"
                    onChange={this.select1}
                  >
                    <option hidden>Pilih Lokasi Barang</option>
                    {lokasi.map((row, index) => {
                      return (
                        <option key={index} value={row.value} name={row.label}>
                          {row.label}
                        </option>
                      );
                    })}
                  </select>
                </div>
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="nama"
                  className="col-4 col-form-label text-right"
                >
                  Nama Barang
                </label>
                <div className="col-8">
                  <input
                    type="text"
                    className="form-control"
                    name="nama"
                    placeholder="Nama barang"
                    value={this.state.formdata.nama}
                    onChange={this.changeHandler}
                    disabled
                  />
                </div>
              </div>
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="kondisi"
                  className="col-4 col-form-label text-right"
                >
                  Kondisi
                </label>
                <div className="col-8">
                  <select
                    name="kondisi"
                    className="form-control"
                    onChange={this.select1}
                    disabled
                  >
                    <option hidden>
                      {this.ubahKondisi(this.state.formdata.kondisi)}
                    </option>
                    {kondisi.map((row, index) => {
                      return (
                        <option key={index} value={row.value} name={row.label}>
                          {row.label}
                        </option>
                      );
                    })}
                  </select>
                </div>
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="merk"
                  className="col-4 col-form-label text-right"
                >
                  Merk
                </label>
                <div className="col-8">
                  <input
                    type="text"
                    className="form-control"
                    name="merk"
                    placeholder="merk barang"
                    value={this.state.formdata.merk}
                    onChange={this.changeHandler}
                  />
                </div>
              </div>
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="ukuran"
                  className="col-4 col-form-label text-right"
                >
                  Ukuran barang
                </label>
                <div className="col-8">
                  <input
                    type="text"
                    className="form-control"
                    name="ukuran"
                    placeholder="ukuran / spesifikasi"
                    value={this.state.formdata.ukuran}
                    onChange={this.changeHandler}
                  />
                </div>
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="bahan"
                  className="col-4 col-form-label text-right"
                >
                  Bahan
                </label>
                <div className="col-8">
                  <input
                    type="text"
                    className="form-control"
                    name="bahan"
                    placeholder="bahan (medis,cair,elektronik,dll)"
                    value={this.state.formdata.bahan}
                    onChange={this.changeHandler}
                  />
                </div>
              </div>
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="tahun"
                  className="col-4 col-form-label text-right"
                >
                  Tahun Perolehan
                </label>
                <div className="col-8">
                  <input
                    type="number"
                    className="form-control"
                    name="tahun"
                    placeholder="Tahun perolehan"
                    value={this.state.formdata.tahun}
                    onChange={this.changeHandler}
                  />
                </div>
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="no_seri"
                  className="col-4 col-form-label text-right"
                >
                  Nomor Seri
                </label>
                <div className="col-8">
                  <input
                    type="text"
                    className="form-control"
                    name="no_seri"
                    placeholder="Nomor seri"
                    value={this.state.formdata.no_seri}
                    onChange={this.changeHandler}
                  />
                </div>
              </div>
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="harga_beli"
                  className="col-4 col-form-label text-right"
                >
                  Harga Beli
                </label>
                <div className="col-8">
                  <input
                    type="number"
                    className="form-control"
                    name="harga_beli"
                    placeholder="Harga beli barang"
                    value={this.state.formdata.harga_beli}
                    onChange={this.changeHandler}
                    disabled
                  />
                </div>
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="total"
                  className="col-4 col-form-label text-right"
                >
                  Jumlah Total Barang:
                </label>
                <div className="col-8">
                  <input
                    type="number"
                    className="form-control"
                    name="total"
                    placeholder="Jumlah barang total"
                    value={this.state.formdata.total}
                    onChange={this.changeHandler}
                    disabled
                  />
                </div>
              </div>
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="tanggal_masuk"
                  className="col-4 col-form-label text-right"
                >
                  Tanggal Masuk
                </label>
                <div className="col-8">
                  <input
                    type="date"
                    className="form-control"
                    name="tanggal_masuk"
                    placeholder="tanggal masuk"
                    value={this.state.formdata.tanggal_masuk}
                    onChange={this.changeHandler}
                    disabled
                  />
                </div>
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="jumlah"
                  className="col-4 col-form-label text-right"
                >
                  Jumlah di gudang
                </label>
                <div className="col-8">
                  <input
                    type="number"
                    className="form-control"
                    name="jumlah"
                    placeholder="Jumlah barang di gudang"
                    value={this.state.formdata.jumlah}
                    onChange={this.changeHandler}
                    disabled
                  />
                </div>
              </div>
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="tanggal_expired"
                  className="col-4 col-form-label text-right"
                >
                  Tanggal Kadaluarsa
                </label>
                <div className="col-8">
                  <input
                    type="date"
                    className="form-control"
                    name="tanggal_expired"
                    placeholder="tanggal kadaluarsa"
                    value={this.state.formdata.tanggal_expired}
                    onChange={this.changeHandler}
                    min={t_min}
                  />
                </div>
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="catatan"
                  className="col-4 col-form-label text-right"
                >
                  Catatan
                </label>
                <div className="col-8">
                  <textarea
                    type="text"
                    className="form-control"
                    name="catatan"
                    placeholder="catatan tambahan (satuan, dll)"
                    value={this.state.formdata.catatan}
                    onChange={this.changeHandler}
                  />
                </div>
              </div>
            </div>
            {this.state.alertData.status === true ? (
              <Alert color="danger">{this.state.alertData.message} </Alert>
            ) : (
              ""
            )}
          </Form>
        </ModalBody>
        <ModalFooter>
          <Button
            variant="contained"
            color="primary"
            onClick={this.submitValidasi}
          >
            Ubah
          </Button>
          <Button variant="contained" onClick={this.props.closeModalHandler}>
            Batal
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

EditBarang.propTypes = {
  barangUpdate: PropTypes.func.isRequired,
  ambil: PropTypes.object.isRequired,
  ambil1: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  ambil: state.barangReducers,
  ambil1: state.lokasiReducers
});

export default connect(
  mapStateToProps,
  { barangUpdate }
)(EditBarang);
