import React from "react";
import { Modal, ModalBody, ModalFooter, ModalHeader, Button } from "reactstrap";
import { barangDelete } from "../../../actions/barangActions";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import apiconfig from "../../../config/api.config.json";

class DeleteBarang extends React.Component {
  constructor(props) {
    super(props);
    let userdata = JSON.parse(localStorage.getItem(apiconfig.LS.USERDATA));
    this.state = {
      status: "",
      updated_by: userdata.username
    };
    this.deleteHandler = this.deleteHandler.bind(this);
  }

  deleteHandler() {
    let data = this.props.barang;
    data["updated_by"] = this.state.updated_by;
    this.props.barangDelete(data);
    this.props.closeModalHandler();
  }

  componentWillReceiveProps(newStatus) {
    this.setState({
      status: newStatus.ambil.statusDEL
    });
  }

  render() {
    this.state.status === 200
      ? this.props.modalStatus(1, "Di Hapus!", this.props.barang.nama)
      : console.log(this.state.status);

    return (
      <Modal isOpen={this.props.delete} className={this.props.className}>
        <ModalHeader> Hapus data Barang </ModalHeader>
        <ModalBody>
          <p>
            Apa anda ingin menghapus barang{" "}
            <strong>{this.props.barang.nama}</strong> ?
          </p>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={this.deleteHandler}>
            Ya
          </Button>
          <Button color="danger" onClick={this.props.closeModalHandler}>
            Tidak
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

DeleteBarang.propTypes = {
  barangDelete: PropTypes.func.isRequired,
  ambil: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  ambil: state.barangReducers
});

export default connect(
  mapStateToProps,
  { barangDelete }
)(DeleteBarang);
