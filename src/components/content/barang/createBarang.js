import React from "react";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button,
  Form,
  Alert
} from "reactstrap";
import PropTypes from "prop-types";
import apiconfig from "../../../config/api.config.json";

import Konfirm from "../../utility/confirm";
import { connect } from "react-redux";
import { barangCreate } from "../../../actions/barangActions";
import moment from "moment";

class CreateBarang extends React.Component {
  constructor(props) {
    super(props);
    let userdata = JSON.parse(localStorage.getItem(apiconfig.LS.USERDATA));
    this.state = {
      formdata: {
        kd_barang_rs: "",
        kd_lokasi: "",
        nama: "",
        merk: "",
        ukuran: "",
        bahan: "",
        tahun: "",
        no_seri: "",
        harga_beli: "",
        total: "",
        catatan: "",
        kondisi: "",
        tanggal_masuk: "",
        tanggal_expired: "",
        created_by: userdata.username
      },
      konfirmasi: false,
      status: "",
      alertData: {
        status: false,
        message: ""
      }
    };
    this.changeHandler = this.changeHandler.bind(this);
  }

  changeHandler(e) {
    let tmp = this.state.formdata;
    tmp[e.target.name] = e.target.value;
    this.setState({
      alertData: {
        status: false,
        message: ""
      },
      formdata: tmp
    });
  }

  select1 = selectedOption => {
    let tmp = this.state.formdata;
    tmp[selectedOption.target.name] = selectedOption.target.value;
    this.setState({
      formdata: tmp
    });
  };

  validateFilter1(kode) {
    let allBarang = this.props.allBarang.map(ele => ele.kd_barang_rs);
    let a = allBarang.filter(e => e === kode);
    if (a.length === 0) {
      return false;
    } else {
      return true;
    }
  }

  submitValidasi = () => {
    const {
      kd_barang_rs,
      kd_lokasi,
      nama,
      merk,
      ukuran,
      bahan,
      tahun,
      no_seri,
      harga_beli,
      total,
      catatan,
      kondisi,
      tanggal_masuk
    } = this.state.formdata;
    if (
      kd_barang_rs === "" ||
      kd_lokasi === "" ||
      nama === "" ||
      merk === "" ||
      ukuran === "" ||
      bahan === "" ||
      tahun === "" ||
      no_seri === "" ||
      harga_beli === "" ||
      total === "" ||
      catatan === "" ||
      kondisi === "" ||
      tanggal_masuk === ""
    ) {
      this.setState({
        alertData: {
          status: true,
          message:
            "Semua field harus di isi! Jika tidak ada berikan tanda strip (-)"
        }
      });
    } else if (this.validateFilter1(kd_barang_rs) === true) {
      this.setState({
        alertData: {
          status: true,
          message: "Ouh, kode barang telah digunakan !"
        }
      });
    } else {
      this.setState({ konfirmasi: true });
    }
  };

  submitHandler = () => {
    this.props.barangCreate(this.state.formdata);
    this.props.closeModalHandler();
    this.KonfirmasiTidak();
  };

  componentWillReceiveProps(newStatus) {
    this.setState({
      status: newStatus.ambil.statusADD
    });
  }

  KonfirmasiTidak = () => {
    this.setState({ konfirmasi: false });
  };

  render() {
    const lokasi = this.props.ambil1.lokasi.map((row, x) => {
      return {
        value: row.code,
        label: row.name
      };
    });
    const kondisi = [
      { value: "1", label: "Baik" },
      { value: "2", label: "Baik Tidak Terpakai" },
      { value: "3", label: "Rusak" }
    ];
    const today = moment().format("YYYY-MM-DD");
    const t_min = moment()
      .add(1, "days")
      .format("YYYY-MM-DD");
    const t_max = moment()
      .add(1, "months")
      .format("YYYY-MM-DD");

    this.state.status === 200
      ? this.props.modalStatus(1, "Di Tambahkan", this.state.formdata.nama)
      : console.log(this.state.status);
    return (
      <Modal isOpen={this.props.create} className="modal-dialog modal-lg">
        <ModalHeader>Tambah Barang Baru</ModalHeader>
        <ModalBody>
          <Konfirm
            showKonfirmasi={this.state.konfirmasi}
            ubahKonfirmasi={this.KonfirmasiTidak}
            submitHandler={this.submitHandler}
          />
          <Form>
            <div className="form-row">
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="kd_barang_rs"
                  className="col-4 col-form-label text-right"
                >
                  Kode Barang RS
                </label>
                <div className="col-8">
                  <input
                    type="text"
                    className="form-control"
                    name="kd_barang_rs"
                    placeholder="kode barang RS"
                    value={this.state.formdata.kd_barang_rs}
                    onChange={this.changeHandler}
                  />
                </div>
              </div>
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="selectlocation"
                  className="col-4 col-form-label text-right"
                >
                  Lokasi
                </label>
                <div className="col-8">
                  <select
                    name="kd_lokasi"
                    className="form-control"
                    onChange={this.select1}
                  >
                    <option hidden>Pilih Lokasi Barang</option>
                    {lokasi.map((row, index) => {
                      return (
                        <option key={index} value={row.value} name={row.label}>
                          {row.label}
                        </option>
                      );
                    })}
                  </select>
                </div>
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="nama"
                  className="col-4 col-form-label text-right"
                >
                  Nama Barang
                </label>
                <div className="col-8">
                  <input
                    type="text"
                    className="form-control"
                    name="nama"
                    placeholder="Nama barang"
                    value={this.state.formdata.nama}
                    onChange={this.changeHandler}
                  />
                </div>
              </div>
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="kondisi"
                  className="col-4 col-form-label text-right"
                >
                  Kondisi
                </label>
                <div className="col-8">
                  <select
                    name="kondisi"
                    className="form-control"
                    onChange={this.select1}
                  >
                    <option hidden>Pilih Kondisi Barang</option>
                    {kondisi.map((row, index) => {
                      return (
                        <option key={index} value={row.value} name={row.label}>
                          {row.label}
                        </option>
                      );
                    })}
                  </select>
                </div>
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="merk"
                  className="col-4 col-form-label text-right"
                >
                  Merk
                </label>
                <div className="col-8">
                  <input
                    type="text"
                    className="form-control"
                    name="merk"
                    placeholder="merk barang"
                    value={this.state.formdata.merk}
                    onChange={this.changeHandler}
                  />
                </div>
              </div>
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="ukuran"
                  className="col-4 col-form-label text-right"
                >
                  Ukuran barang
                </label>
                <div className="col-8">
                  <input
                    type="text"
                    className="form-control"
                    name="ukuran"
                    placeholder="ukuran / spesifikasi"
                    value={this.state.formdata.ukuran}
                    onChange={this.changeHandler}
                  />
                </div>
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="bahan"
                  className="col-4 col-form-label text-right"
                >
                  Bahan
                </label>
                <div className="col-8">
                  <input
                    type="text"
                    className="form-control"
                    name="bahan"
                    placeholder="bahan (medis,cair,elektronik,dll)"
                    value={this.state.formdata.bahan}
                    onChange={this.changeHandler}
                  />
                </div>
              </div>
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="tahun"
                  className="col-4 col-form-label text-right"
                >
                  Tahun Perolehan
                </label>
                <div className="col-8">
                  <input
                    type="number"
                    className="form-control"
                    name="tahun"
                    placeholder="Tahun perolehan"
                    value={this.state.formdata.tahun}
                    onChange={this.changeHandler}
                  />
                </div>
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="no_seri"
                  className="col-4 col-form-label text-right"
                >
                  Nomor Seri
                </label>
                <div className="col-8">
                  <input
                    type="text"
                    className="form-control"
                    name="no_seri"
                    placeholder="Nomor seri"
                    value={this.state.formdata.no_seri}
                    onChange={this.changeHandler}
                  />
                </div>
              </div>
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="harga_beli"
                  className="col-4 col-form-label text-right"
                >
                  Harga Beli
                </label>
                <div className="col-8">
                  <input
                    type="number"
                    className="form-control"
                    name="harga_beli"
                    placeholder="Harga beli barang"
                    value={this.state.formdata.harga_beli}
                    onChange={this.changeHandler}
                  />
                </div>
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="total"
                  className="col-4 col-form-label text-right"
                >
                  Jumlah Barang
                </label>
                <div className="col-8">
                  <input
                    type="number"
                    className="form-control"
                    name="total"
                    placeholder="Jumlah barang total"
                    value={this.state.formdata.total}
                    onChange={this.changeHandler}
                  />
                </div>
              </div>
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="tanggal_masuk"
                  className="col-4 col-form-label text-right"
                >
                  Tanggal Masuk
                </label>
                <div className="col-8">
                  <input
                    type="date"
                    className="form-control"
                    name="tanggal_masuk"
                    placeholder="tanggal masuk"
                    value={this.state.formdata.tanggal_masuk}
                    onChange={this.changeHandler}
                    min={today}
                    max={t_max}
                  />
                </div>
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="catatan"
                  className="col-4 col-form-label text-right"
                >
                  Catatan
                </label>
                <div className="col-8">
                  <textarea
                    type="text"
                    className="form-control"
                    name="catatan"
                    placeholder="catatan tambahan (satuan, dll)"
                    value={this.state.formdata.catatan}
                    onChange={this.changeHandler}
                  />
                </div>
              </div>
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="tanggal_expired"
                  className="col-4 col-form-label text-right"
                >
                  Tanggal Kadaluarsa
                </label>
                <div className="col-8">
                  <input
                    type="date"
                    className="form-control"
                    name="tanggal_expired"
                    placeholder="tanggal kadaluarsa"
                    value={this.state.formdata.tanggal_expired}
                    onChange={this.changeHandler}
                    min={t_min}
                  />
                </div>
              </div>
            </div>
            {this.state.alertData.status === true ? (
              <Alert color="danger">{this.state.alertData.message} </Alert>
            ) : (
              ""
            )}
          </Form>
        </ModalBody>
        <ModalFooter>
          <Button
            variant="contained"
            color="primary"
            onClick={this.submitValidasi}
          >
            Tambahkan
          </Button>
          <Button variant="contained" onClick={this.props.closeModalHandler}>
            Batal
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

CreateBarang.propTypes = {
  barangCreate: PropTypes.func.isRequired,
  ambil: PropTypes.object.isRequired,
  ambil1: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  ambil: state.barangReducers,
  ambil1: state.lokasiReducers
});

export default connect(
  mapStateToProps,
  { barangCreate }
)(CreateBarang);
