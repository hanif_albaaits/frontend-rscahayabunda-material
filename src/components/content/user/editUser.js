import React from "react";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  Alert
} from "reactstrap";
import Select from "react-select";
import PropTypes from "prop-types";
import apiconfig from "../../../config/api.config.json";

import Konfirm from "../../utility/confirm";
import { connect } from "react-redux";
import { userUpdate } from "../../../actions/userActions";

class EditUser extends React.Component {
  constructor(props) {
    super(props);
    let userdata = JSON.parse(localStorage.getItem(apiconfig.LS.USERDATA));

    this.state = {
      formdata: {
        username: "",
        password: "",
        repassword: "",
        role_id: "",
        kd_pegawai: ""
      },
      konfirmasi: false,
      currentUsername: "",
      selectedRole: null,
      updated_by: userdata.username,
      status: "",
      alertData: {
        status: false,
        message: ""
      },
      labelWidth: 0
    };
    this.changeHandler = this.changeHandler.bind(this);
  }

  changeHandler(e) {
    let tmp = this.state.formdata;
    tmp[e.target.name] = e.target.value;
    this.setState({
      alertData: {
        status: false,
        message: ""
      },
      formdata: tmp
    });
  }

  select1 = selectedOption => {
    let tmp = this.state.formdata;
    tmp[selectedOption.name] = selectedOption.value;
    this.setState({
      formdata: tmp
    });
  };

  validateFilter(username) {
    let lama = this.props.username;
    let allUser = this.props.allUser.map(ele => ele.username);
    let a = allUser.filter(e => e === username);
    if (a.length === 0 || username === lama) {
      return false;
    } else {
      return true;
    }
  }

  validatePassword(password) {
    let regex = new RegExp(/((?=.*[a-z])(?=.*[A-Z]))/);
    return regex.test(String(password));
    /*At least one lC,Up,Number,[@#$%],6-20 /((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})/*/
  }
  validateUsername(username) {
    let regex = new RegExp(/((?=.*\d).{6,10})/);
    return regex.test(String(username));
    /*At least one lC,Up,Number,[@#$%],6-20*/
  }

  submitValidasi = () => {
    if (
      this.state.formdata.username === "" ||
      this.state.formdata.password === "" ||
      this.state.formdata.repassword === "" ||
      this.state.formdata.role_id === "" ||
      this.state.formdata.kd_pegawai === ""
    ) {
      this.setState({
        alertData: {
          status: true,
          message: "Semua field harus di isi!"
        }
      });
    } else if (this.validateFilter(this.state.formdata.username) === true) {
      this.setState({
        alertData: {
          status: true,
          message: "Ouh, nama username kamu telah digunakan !"
        }
      });
    } else if (this.validateUsername(this.state.formdata.username) === false) {
      this.setState({
        alertData: {
          status: true,
          message: "Username setidaknya memiliki 1 angka dan 6 karakter"
        }
      });
    } else if (this.validatePassword(this.state.formdata.password) === false) {
      this.setState({
        alertData: {
          status: true,
          message: "Password setidaknya memiliki 1 Huruf Besar dan Kecil"
        }
      });
    } else if (
      this.state.formdata.password !== this.state.formdata.repassword
    ) {
      this.setState({
        alertData: {
          status: true,
          message: "Konfirmasi Password Salah !"
        }
      });
    } else {
      this.setState({ konfirmasi: true });
    }
  };

  submitHandler = () => {
    let form = this.state.formdata;
    form["updated_by"] = this.state.updated_by;
    this.props.userUpdate(this.state.formdata);
    this.konfirmasiTidak();
  };

  componentWillReceiveProps(newProps) {
    this.setState({
      formdata: newProps.currentUser,
      status: newProps.ambil.statusPUT
    });
  }

  konfirmasiTidak = () => {
    this.setState({ konfirmasi: false });
  };

  render() {
    const ambilPegawai = this.props.ambil.pegawai.map((row, x) => {
      return {
        value: row.kd_pegawai,
        label: row.name,
        name: "kd_pegawai"
      };
    });

    const ambilRole = this.props.ambilRole.role.map((row, x) => {
      return {
        value: row.code,
        label: row.name,
        name: "role_id"
      };
    });

    this.state.status === 200
      ? this.props.modalStatus(1, "Di Ubah!", this.state.formdata.username)
      : console.log(this.state.status);
    return (
      <Modal isOpen={this.props.edit} className={this.props.className}>
        <ModalHeader>Ubah Data User</ModalHeader>
        <ModalBody>
          <Konfirm
            showKonfirmasi={this.state.konfirmasi}
            ubahKonfirmasi={this.KonfirmasiTidak}
            submitHandler={this.submitHandler}
          />
          <Form>
            <FormGroup>
              <Label for="username">Username</Label>
              <Input
                type="text"
                name="username"
                id="username"
                placeholder="masukan username"
                value={this.state.formdata.username}
                onChange={this.changeHandler}
                disabled
              />
            </FormGroup>
            <FormGroup>
              <Label for="password">Password Baru</Label>
              <Input
                type="password"
                name="password"
                id="password"
                placeholder="password baru"
                value={this.state.formdata.password}
                onChange={this.changeHandler}
              />
            </FormGroup>
            <FormGroup>
              <Label for="repassword">Konfirmasi Password</Label>
              <Input
                type="password"
                name="repassword"
                id="repassword"
                placeholder="konfirmasi password"
                value={this.state.formdata.repassword}
                onChange={this.changeHandler}
              />
            </FormGroup>
            <FormGroup>
              <Label for="selectrole">Pilih Pegawai</Label>
              <Select
                className="basic-single"
                classNamePrefix="select"
                label="Select Pegawai"
                name="kd_pegawai"
                options={ambilPegawai}
                value={this.state.kd_pegawai}
                onChange={this.select1}
              />
            </FormGroup>
            <FormGroup>
              <Label for="selectrole">Pilih Role</Label>
              <Select
                className="basic-single"
                classNamePrefix="select"
                label="Select Role"
                name="role_id"
                options={ambilRole}
                value={this.state.role_id}
                onChange={this.select1}
              />
            </FormGroup>
          </Form>
        </ModalBody>
        <ModalFooter>
          {this.state.alertData.status === true ? (
            <Alert color="danger">{this.state.alertData.message} </Alert>
          ) : (
            ""
          )}
          <Button
            variant="contained"
            color="primary"
            onClick={this.submitValidasi}
          >
            Ubah
          </Button>
          <Button variant="contained" onClick={this.props.closeModalHandler}>
            Batal
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

EditUser.propTypes = {
  userUpdate: PropTypes.func.isRequired,
  ambil: PropTypes.object.isRequired,
  ambilRole: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  ambil: state.userReducers,
  ambilRole: state.roleReducers
});

export default connect(
  mapStateToProps,
  { userUpdate }
)(EditUser);
