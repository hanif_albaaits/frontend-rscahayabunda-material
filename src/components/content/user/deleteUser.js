import React from "react";
import { Modal, ModalBody, ModalFooter, ModalHeader, Button } from "reactstrap";
import { userDelete } from "../../../actions/userActions";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import apiconfig from "../../../config/api.config.json";

class DeleteUser extends React.Component {
  constructor(props) {
    super(props);
    let userdata = JSON.parse(localStorage.getItem(apiconfig.LS.USERDATA));
    this.state = {
      status: "",
      user: userdata.username
    };
    this.deleteHandler = this.deleteHandler.bind(this);
  }

  deleteHandler() {
    let data = this.props.user;
    data["updated_by"] = this.state.user;
    this.props.userDelete(data);
    this.props.closeModalHandler();
  }

  componentWillReceiveProps(newStatus) {
    this.setState({
      status: newStatus.ambil.statusDEL
    });
  }

  render() {
    this.state.status === 200
      ? this.props.modalStatus(1, "Di Hapus!", this.props.user.username)
      : console.log(this.state.status);

    return (
      <Modal isOpen={this.props.delete} className={this.props.className}>
        <ModalHeader> Hapus Akun User </ModalHeader>
        <ModalBody>
          <p>
            Apa anda ingin menghapus user{" "}
            <strong>{this.props.user.username}</strong> ?
          </p>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={this.deleteHandler}>
            Ya
          </Button>
          <Button color="danger" onClick={this.props.closeModalHandler}>
            Tidak
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

DeleteUser.propTypes = {
  userDelete: PropTypes.func.isRequired,
  ambil: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  ambil: state.userReducers
});

export default connect(
  mapStateToProps,
  { userDelete }
)(DeleteUser);
