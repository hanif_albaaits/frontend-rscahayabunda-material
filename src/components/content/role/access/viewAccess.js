import React from "react";
import { Modal, ModalBody, ModalFooter, ModalHeader, Button } from "reactstrap";
import Grid from "@material-ui/core/Grid";

class ViewAccess extends React.Component {
  render() {
    return (
      <Modal isOpen={this.props.view} className={this.props.className}>
        <ModalHeader> Lihat Akses Menu</ModalHeader>
        <ModalBody>
          <div>
            <h3>{this.props.access.role_id} </h3>
          </div>
          <div>
            <Grid container spacing={24}>
              <Grid item xs={6}>
                Menu ID
                <br />
                Nama Menu
                <br />
                Status
                <br />
                Atribut
                <br />
                Created By
                <br />
                Created Date
                <br />
                Updated By
                <br />
                Updated Date
              </Grid>
              <Grid item xs={6}>
                {this.props.access.menu_id}
                <br />
                {this.props.access.menu_name}
                <br />
                {this.props.access.status}
                <br />
                {this.props.access.atribut}
                <br />
                {this.props.access.created_by}
                <br />
                {this.props.access.created_date}
                <br />
                {this.props.access.updated_by}
                <br />
                {this.props.access.updated_date}
              </Grid>
            </Grid>
          </div>
        </ModalBody>
        <ModalFooter>
          <Button color="danger" onClick={this.props.closeModalHandler}>
            Tutup
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

export default ViewAccess;
