import React from "react";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  Alert
} from "reactstrap";
import Select from "react-select";
import PropTypes from "prop-types";
import apiconfig from "../../../../config/api.config.json";

import Konfirm from "../../../utility/confirm";
import { connect } from "react-redux";
import { accessUpdate, getAccess } from "../../../../actions/accessActions";

class EditAccess extends React.Component {
  constructor(props) {
    super(props);
    let userdata = JSON.parse(localStorage.getItem(apiconfig.LS.USERDATA));
    this.state = {
      formdata: {
        atribut: "",
        status: ""
      },
      konfirmasi: false,
      updated_by: userdata.username,
      status: "",
      alertData: {
        status: false,
        message: ""
      }
    };
  }
  select1 = selectedOption => {
    let tmp = this.state.formdata;
    tmp[selectedOption.name] = selectedOption.value;
    this.setState({
      formdata: tmp
    });
  };

  submitValidasi = () => {
    if (
      this.state.formdata.status === "" ||
      this.state.formdata.atribut === ""
    ) {
      this.setState({
        alertData: {
          status: true,
          message: "Semua field harus di isi!"
        }
      });
    } else {
      this.setState({ konfirmasi: true });
    }
  };

  submitHandler = () => {
    let form = this.state.formdata;
    form["menu_id"] = this.props.currentAccess.menu_id;
    form["updated_by"] = this.state.updated_by;
    this.props.accessUpdate(this.state.formdata);
    this.props.closeModalHandler();
    this.KonfirmasiTidak();
    setTimeout(() => {
      this.props.getAccess();
    }, 1000);
  };

  KonfirmasiTidak = () => {
    this.setState({ konfirmasi: false });
  };

  componentWillReceiveProps(newProps) {
    this.setState({
      formdata: newProps.currentAccess,
      status: newProps.ambil.statusPUT
    });
  }

  render() {
    const ambilAtribut = [
      { value: 1, label: "read-only", name: "atribut" },
      { value: 2, label: "full-acces", name: "atribut" }
    ];
    const ambilStatus = [
      { value: 1, label: "enabled", name: "status" },
      { value: 2, label: "disabled", name: "status" }
    ];

    this.state.status === 200
      ? this.props.modalStatus(1, "Di Ubah!", this.state.formdata.menu_id)
      : console.log(this.state.status);
    return (
      <Modal isOpen={this.props.edit} className={this.props.className}>
        <ModalHeader>Ubah Menu Akses</ModalHeader>
        <ModalBody>
          <Konfirm
            showKonfirmasi={this.state.konfirmasi}
            ubahKonfirmasi={this.KonfirmasiTidak}
            submitHandler={this.submitHandler}
          />
          <Form>
            <FormGroup>
              <Label for="role_id">Role ID</Label>
              <Input
                type="text"
                name="role_id"
                id="role_id"
                value={this.props.currentAccess.role_id}
                disabled
              />
            </FormGroup>
            <FormGroup>
              <Label for="menu_name">Menu Name</Label>
              <Input
                type="text"
                name="menu_name"
                id="menu_name"
                value={this.props.currentAccess.menu_name}
                disabled
              />
            </FormGroup>
            <FormGroup>
              <Label for="status">Status: (enabled / disabled)</Label>
              <Select
                className="basic-single"
                classNamePrefix="select"
                label="Select Status"
                name="status"
                options={ambilStatus}
                onChange={this.select1}
              />
            </FormGroup>
            <FormGroup>
              <Label for="atribut">Pilih Atribut</Label>
              <Select
                className="basic-single"
                classNamePrefix="select"
                label="Select Atribut"
                name="atribut"
                options={ambilAtribut}
                onChange={this.select1}
              />
            </FormGroup>
          </Form>
        </ModalBody>
        <ModalFooter>
          {this.state.alertData.status === true ? (
            <Alert color="danger">{this.state.alertData.message} </Alert>
          ) : (
            ""
          )}
          <Button
            variant="contained"
            color="primary"
            onClick={this.submitValidasi}
          >
            Ubah
          </Button>
          <Button variant="contained" onClick={this.props.closeModalHandler}>
            Batal
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

EditAccess.propTypes = {
  accessUpdate: PropTypes.func.isRequired,
  ambil: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  ambil: state.roleReducers
});

export default connect(
  mapStateToProps,
  { accessUpdate, getAccess }
)(EditAccess);
