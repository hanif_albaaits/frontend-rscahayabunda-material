import React from "react";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  Alert
} from "reactstrap";
import Select from "react-select";
import PropTypes from "prop-types";
import apiconfig from "../../../../config/api.config.json";

import Konfirm from "../../../utility/confirm";
import { connect } from "react-redux";
import { accessCreate, getAccess } from "../../../../actions/accessActions";

class CreateAccess extends React.Component {
  constructor(props) {
    super(props);
    let userdata = JSON.parse(localStorage.getItem(apiconfig.LS.USERDATA));
    this.state = {
      formdata: {
        menu_id: "",
        atribut: "",
        created_by: userdata.username
      },
      konfirmasi: false,
      status: "",
      alertData: {
        status: false,
        message: ""
      }
    };
  }
  select1 = selectedOption => {
    let tmp = this.state.formdata;
    tmp[selectedOption.name] = selectedOption.value;
    this.setState({
      formdata: tmp
    });
  };

  submitValidasi = () => {
    if (
      this.state.formdata.menu_id === "" ||
      this.state.formdata.atribut === ""
    ) {
      this.setState({
        alertData: {
          status: true,
          message: "Semua field harus di isi!"
        }
      });
    } else {
      this.setState({ konfirmasi: true });
    }
  };

  submitHandler = () => {
    let form = this.state.formdata;
    form["role_id"] = this.props.role_id;
    this.props.accessCreate(this.state.formdata);
    this.props.closeModalHandler();
    this.KonfirmasiTidak();
    setTimeout(() => {
      this.props.getAccess();
    }, 1000);
  };

  componentWillReceiveProps(newStatus) {
    this.setState({
      status: newStatus.ambil.statusADD
    });
  }

  KonfirmasiTidak = () => {
    this.setState({ konfirmasi: false });
  };

  render() {
    const ambilMenu = this.props.ambil1.menu.map((row, x) => {
      return {
        value: row.code,
        label: row.name,
        name: "menu_id"
      };
    });
    const ambilAtribut = [
      { value: 1, label: "read-only", name: "atribut" },
      { value: 2, label: "full-acces", name: "atribut" }
    ];

    // this.state.status === 200
    //   ? this.props.modalStatus(1, "Di Tambahkan", this.state.formdata.menu_id)
    //   : console.log(this.state.status);
    return (
      <Modal isOpen={this.props.create} className={this.props.className}>
        <ModalHeader>Tambah Akses Menu</ModalHeader>
        <ModalBody>
          <Konfirm
            showKonfirmasi={this.state.konfirmasi}
            ubahKonfirmasi={this.KonfirmasiTidak}
            submitHandler={this.submitHandler}
          />
          <Form>
            <FormGroup>
              <Label for="role_id">Code Role</Label>
              <Input
                type="text"
                name="role_id"
                id="role_id"
                value={this.props.role_id}
                disabled
              />
            </FormGroup>
            <FormGroup>
              <Label for="menu_id">Pilih Menu</Label>
              <Select
                className="basic-single"
                classNamePrefix="select"
                label="Select Menu"
                name="menu_id"
                options={ambilMenu}
                onChange={this.select1}
              />
            </FormGroup>
            <FormGroup>
              <Label for="atribut">Pilih Atribut</Label>
              <Select
                className="basic-single"
                classNamePrefix="select"
                label="Select Atribut"
                name="atribut"
                options={ambilAtribut}
                onChange={this.select1}
              />
            </FormGroup>
          </Form>
        </ModalBody>
        <ModalFooter>
          {this.state.alertData.status === true ? (
            <Alert color="danger">{this.state.alertData.message} </Alert>
          ) : (
            ""
          )}
          <Button
            variant="contained"
            color="primary"
            onClick={this.submitValidasi}
          >
            Tambahkan
          </Button>
          <Button variant="contained" onClick={this.props.closeModalHandler}>
            Batal
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

CreateAccess.propTypes = {
  accessCreate: PropTypes.func.isRequired,
  ambil: PropTypes.object.isRequired,
  ambil1: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  ambil: state.accessReducers,
  ambil1: state.menuReducers
});

export default connect(
  mapStateToProps,
  { accessCreate, getAccess }
)(CreateAccess);
