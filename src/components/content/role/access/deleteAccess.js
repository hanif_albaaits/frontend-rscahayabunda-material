import React from "react";
import { Modal, ModalBody, ModalFooter, ModalHeader, Button } from "reactstrap";
import { accessDelete, getAccess } from "../../../../actions/accessActions";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import apiconfig from "../../../../config/api.config.json";

class DeleteAccess extends React.Component {
  constructor(props) {
    super(props);
    let userdata = JSON.parse(localStorage.getItem(apiconfig.LS.USERDATA));
    this.state = {
      status: "",
      updated_by: userdata.username
    };
    this.deleteHandler = this.deleteHandler.bind(this);
  }

  deleteHandler() {
    let data = this.props.access;
    data["updated_by"] = this.state.updated_by;
    this.props.accessDelete(data);
    this.props.closeModalHandler();
    setTimeout(() => {
      this.props.getAccess();
    }, 1000);
  }

  componentWillReceiveProps(newStatus) {
    this.setState({
      status: newStatus.ambil.statusDEL
    });
  }

  render() {
    // this.state.status === 200
    //   ? this.props.modalStatus(1, "Di Hapus!", this.props.role.name)
    //   : console.log(this.state.status);

    return (
      <Modal isOpen={this.props.delete} className={this.props.className}>
        <ModalHeader> Hapus Menu Akses</ModalHeader>
        <ModalBody>
          <p>
            Apa anda ingin menghapus menu{" "}
            <strong>{this.props.access.menu_id}</strong> ?
          </p>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={this.deleteHandler}>
            Ya
          </Button>
          <Button color="danger" onClick={this.props.closeModalHandler}>
            Tidak
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

DeleteAccess.propTypes = {
  accessDelete: PropTypes.func.isRequired,
  ambil: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  ambil: state.accessReducers
});

export default connect(
  mapStateToProps,
  { accessDelete, getAccess }
)(DeleteAccess);
