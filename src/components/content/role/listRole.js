import React, { Fragment } from "react";
import { Alert } from "reactstrap";
import { getRole } from "../../../actions/roleActions";
import { getMenu } from "../../../actions/menuActions";
import { getAccess } from "../../../actions/accessActions";
import { connect } from "react-redux";

import EditRole from "./editRole";
import CreateRole from "./createRole";
import DeleteRole from "./deleteRole";
import ViewRole from "./viewRole";
import printTable from "../../utility/printTable";
import exportCsv from "../../utility/exportCsv";
import bacaAkses from "../../utility/bacaAkses";
import Spinner from "../../utility/spinnerTable";

import PropTypes from "prop-types";
import moment from "moment";

import IconButton from "@material-ui/core/IconButton";
import FirstPageIcon from "@material-ui/icons/FirstPage";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import LastPageIcon from "@material-ui/icons/LastPage";
import TablePagination from "@material-ui/core/TablePagination";
import { withStyles } from "@material-ui/core/styles";

class ListRole extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      formCari: {
        code: /(?:)/,
        name: /(?:)/,
        created_date: /(?:)/
      },
      AKSES: {},
      createRole: false,
      deleteRole: false,
      editRole: false,
      viewRole: false,
      allRole: [],
      currentRole: {},
      alertData: {
        status: 0,
        message: "",
        code: ""
      },
      hasil: [],
      page: 0,
      rowsPerPage: 5
    };
    this.showCreate = this.showCreate.bind(this);
  }
  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  deleteModalHandler = roleid => {
    let tmp = {};
    this.state.allRole.map(ele => {
      if (roleid === ele._id) {
        tmp = ele;
      }
      return tmp;
    });
    this.setState({
      currentRole: tmp,
      deleteRole: true
    });
  };

  viewModalHandler = roleid => {
    let tmp = {};
    this.state.allRole.map(ele => {
      if (roleid === ele._id) {
        tmp = ele;
      }
      return tmp;
    });
    this.setState({
      currentRole: tmp,
      viewRole: true
    });
  };

  editModalHandler = roleid => {
    let tmp = {};
    this.state.allRole.map(ele => {
      if (roleid === ele._id) {
        tmp = {
          _id: ele._id,
          code: ele.code,
          name: ele.name,
          description: ele.description,
          update_date: ele.update_date,
          update_by: ele.update_by
        };
        this.setState({
          currentRole: tmp,
          editRole: true
        });
      }
      return tmp;
    });
  };

  changeHandler = event => {
    let data = event.target.value;
    let char = data.replace(/\\/g, "");
    let tmp = this.state.formCari;
    if (event.target.name) {
      tmp[event.target.name] = new RegExp(char.toLowerCase());
    } else {
      tmp[event.target.name] = char;
    }
    this.cariData();
  };

  cariData = () => {
    const { code, name, created_date } = this.state.formCari;
    let temp = [];
    this.state.allRole.map(ele => {
      if (
        code.test(ele.code.toLowerCase()) &&
        name.test(ele.name.toLowerCase()) &&
        created_date.test(ele.created_date)
      ) {
        temp.push(ele);
      }
      return temp;
    });
    this.setState({
      hasil: temp
    });
  };

  closeModalHandler = () => {
    this.setState({
      createRole: false,
      viewRole: false,
      editRole: false,
      deleteRole: false
    });
  };

  showCreate = () => {
    this.setState({ createRole: true });
  };

  componentDidMount() {
    this.props.getRole();
    this.props.getMenu();
    this.props.getAccess();
  }

  componentWillReceiveProps(newProps) {
    let a = bacaAkses(newProps.ambilAccess.side, window.location.pathname);
    let allRole = newProps.ambil.role;
    if (a !== null) {
      this.setState({
        AKSES: a
      });
    }
    if (allRole !== null) {
      this.setState({
        allRole: allRole,
        hasil: allRole,
        loading: false
      });
    }
  }

  modalStatus = (status, message, code) => {
    this.setState({
      alertData: {
        status: status,
        message: message,
        code: code
      },
      viewRole: false,
      editRole: false,
      deleteRole: false,
      createRole: false
    });
    setTimeout(() => {
      window.location.href = "/role";
    }, 3000);
  };

  changeDateFormat = tanggal => {
    return moment(tanggal, "YYYY-MM-DD hh:mm:ss a").format("DD/MM/YYYY");
  };

  render() {
    const AKSES = this.state.AKSES;
    const loading = this.state.loading;
    let tablelist;

    if (loading === true) {
      tablelist = <Spinner />;
    } else {
      if (this.state.allRole.length > 0) {
        tablelist = this.state.hasil
          .slice(
            this.state.page * this.state.rowsPerPage,
            this.state.page * this.state.rowsPerPage + this.state.rowsPerPage
          )
          .map((row, index) => {
            return (
              <tr key={index}>
                <td className="align-middle">{row.code}</td>
                <td className="align-middle">{row.name}</td>
                <td className="align-middle">
                  {this.changeDateFormat(row.created_date)}
                </td>
                <td className="align-middle">{row.created_by}</td>
                <td className="align-middle text-center">
                  {AKSES.atribut === 2 ? (
                    <Fragment>
                      <button
                        type="button"
                        className="btn btn-icon btn-flat btn-default"
                        title="view"
                        onClick={() => {
                          this.viewModalHandler(row._id);
                        }}
                      >
                        <i className="icon md-search" aria-hidden="false" />
                      </button>
                      <button
                        type="button"
                        className="btn btn-icon btn-flat btn-default"
                        title="Edit"
                        onClick={() => {
                          this.editModalHandler(row._id);
                        }}
                      >
                        <i className="icon md-edit" aria-hidden="false" />
                      </button>
                      <button
                        type="button"
                        className="btn btn-sm btn-icon btn-flat btn-default"
                        title="Delete"
                        onClick={() => {
                          this.deleteModalHandler(row._id);
                        }}
                      >
                        <i className="icon md-delete" aria-hidden="false" />
                      </button>
                    </Fragment>
                  ) : (
                    ""
                  )}
                </td>
              </tr>
            );
          });
      } else {
        tablelist = (
          <tr className="text-center">
            <td colSpan="10">Tidak Ada data yang tersedia !</td>
          </tr>
        );
      }
    }

    return (
      <div>
        <div className="page-header">
          <h1 className="page-title">Master Role</h1>
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <a href="/dashboard">Home</a>
            </li>
            <li className="breadcrumb-item">
              <a href="/dashboard">Master</a>
            </li>
            <li className="breadcrumb-item active">Master Role</li>
          </ol>
          <div className="page-header-actions">
            {AKSES.atribut === 2 ? (
              <div>
                <button
                  type="button"
                  className="btn btn-primary"
                  onClick={this.showCreate}
                >
                  Tambah Role
                </button>
              </div>
            ) : (
              ""
            )}
          </div>
        </div>
        <div className="page-content">
          {this.state.alertData.status === 1 ? (
            <Alert color="success">
              <b>Data {this.state.alertData.message}</b> Data Role dengan
              referensi code <strong>{this.state.alertData.code} </strong>
              telah {this.state.alertData.message}
            </Alert>
          ) : (
            ""
          )}
          {this.state.alertData.status === 2 ? (
            <Alert color="danger">{this.state.alertData.message} </Alert>
          ) : (
            ""
          )}
          <div className="panel">
            <header className="panel-heading">
              <h3 className="panel-title">List Role</h3>
            </header>
            <div className="panel-body">
              <form className="form-inline">
                <div className="form-group">
                  <input
                    type="text"
                    placeholder="cari ID role"
                    className="form-control mx-sm-1"
                    name="code"
                    onChange={this.changeHandler}
                    autoComplete="off"
                  />
                  <input
                    type="text"
                    placeholder="cari nama role"
                    className="form-control mx-sm-1"
                    name="name"
                    onChange={this.changeHandler}
                    autoComplete="off"
                  />
                  <input
                    type="date"
                    className="form-control mx-sm-1"
                    name="created_date"
                    onChange={this.changeHandler}
                  />
                </div>
              </form>
              <div className="example table-responsive">
                <table id="table" className="table table-bordered">
                  <thead>
                    <tr>
                      <th>ID Role</th>
                      <th>Nama Role</th>
                      <th>Created Date</th>
                      <th>Created By</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>{tablelist}</tbody>
                  <TablePagination
                    colSpan={5}
                    count={this.state.hasil.length}
                    rowsPerPage={this.state.rowsPerPage}
                    page={this.state.page}
                    onChangePage={this.handleChangePage}
                    onChangeRowsPerPage={this.handleChangeRowsPerPage}
                    ActionsComponent={TablePaginationActionsWrapped}
                  />
                </table>
                <button
                  type="button"
                  className="btn btn-primary mx-sm-1"
                  onClick={() => {
                    printTable("table");
                  }}
                  hidden={true}
                >
                  Print
                </button>
                <button
                  type="button"
                  className="btn btn-primary mx-sm-1"
                  onClick={() => {
                    exportCsv("m_role");
                  }}
                >
                  Export to CSV
                </button>
                <ViewRole
                  view={this.state.viewRole}
                  closeModalHandler={this.closeModalHandler}
                  role={this.state.currentRole}
                />
                <DeleteRole
                  delete={this.state.deleteRole}
                  role={this.state.currentRole}
                  closeModalHandler={this.closeModalHandler}
                  modalStatus={this.modalStatus}
                />
                <CreateRole
                  create={this.state.createRole}
                  closeModalHandler={this.closeModalHandler}
                  modalStatus={this.modalStatus}
                  allRole={this.state.allRole}
                />
                <EditRole
                  edit={this.state.editRole}
                  closeModalHandler={this.closeModalHandler}
                  currentRole={this.state.currentRole}
                  name={this.state.currentRole.name}
                  modalStatus={this.modalStatus}
                  allRole={this.state.allRole}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ListRole.propTypes = {
  getRole: PropTypes.func.isRequired,
  ambil: PropTypes.object.isRequired,
  ambilAccess: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  ambil: state.roleReducers,
  ambilAccess: state.menuReducers
});

export default connect(
  mapStateToProps,
  { getRole, getMenu, getAccess }
)(ListRole);

const actionsStyles = theme => ({
  root: {
    flexShrink: 0,
    color: theme.palette.text.secondary,
    marginLeft: theme.spacing.unit * 2.5
  }
});

class TablePaginationActions extends React.Component {
  handleFirstPageButtonClick = event => {
    this.props.onChangePage(event, 0);
  };

  handleBackButtonClick = event => {
    this.props.onChangePage(event, this.props.page - 1);
  };

  handleNextButtonClick = event => {
    this.props.onChangePage(event, this.props.page + 1);
  };

  handleLastPageButtonClick = event => {
    this.props.onChangePage(
      event,
      Math.max(0, Math.ceil(this.props.count / this.props.rowsPerPage) - 1)
    );
  };

  render() {
    const { classes, count, page, rowsPerPage, theme } = this.props;

    return (
      <div className={classes.root}>
        <IconButton
          onClick={this.handleFirstPageButtonClick}
          disabled={page === 0}
          aria-label="First Page"
        >
          {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
        </IconButton>
        <IconButton
          onClick={this.handleBackButtonClick}
          disabled={page === 0}
          aria-label="Previous Page"
        >
          {theme.direction === "rtl" ? (
            <KeyboardArrowRight />
          ) : (
            <KeyboardArrowLeft />
          )}
        </IconButton>
        <IconButton
          onClick={this.handleNextButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="Next Page"
        >
          {theme.direction === "rtl" ? (
            <KeyboardArrowLeft />
          ) : (
            <KeyboardArrowRight />
          )}
        </IconButton>
        <IconButton
          onClick={this.handleLastPageButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="Last Page"
        >
          {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
        </IconButton>
      </div>
    );
  }
}

TablePaginationActions.propTypes = {
  classes: PropTypes.object.isRequired,
  count: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
  theme: PropTypes.object.isRequired
};

const TablePaginationActionsWrapped = withStyles(actionsStyles, {
  withTheme: true
})(TablePaginationActions);
