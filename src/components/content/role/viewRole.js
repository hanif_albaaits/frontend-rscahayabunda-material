import React from "react";
import { Link } from "react-router-dom";
import { Alert } from "reactstrap";
import { connect } from "react-redux";

import { Modal, ModalBody, ModalFooter, ModalHeader, Button } from "reactstrap";
import PropTypes from "prop-types";

import EditAccess from "./access/editAccess";
import CreateAccess from "./access/createAccess";
import DeleteAccess from "./access/deleteAccess";
import ViewAccess from "./access/viewAccess";

class ViewRole extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      allAccess: [],
      currentAccess: {},
      createAccess: false,
      deleteAccess: false,
      editAccess: false,
      viewAccess: false,
      alertData: {
        status: 0,
        message: "",
        code: ""
      }
    };
  }
  deleteModalHandler = id => {
    let tmp = {};
    this.state.allAccess.map(ele => {
      if (id === ele._id) {
        tmp = ele;
      }
      return tmp;
    });
    this.setState({
      currentAccess: tmp,
      deleteAccess: true
    });
  };
  viewModalHandler = id => {
    let tmp = {};
    this.state.allAccess.map(ele => {
      if (id === ele._id) {
        tmp = ele;
      }
      return tmp;
    });
    this.setState({
      currentAccess: tmp,
      viewAccess: true
    });
  };
  editModalHandler = id => {
    let tmp = {};
    this.state.allAccess.map(ele => {
      if (id === ele._id) {
        tmp = {
          _id: ele._id,
          role_id: ele.role_id,
          menu_id: ele.menu_id,
          menu_name: ele.menu_name,
          status: ele.status,
          atribut: ele.atribut,
          update_date: ele.update_date,
          update_by: ele.update_by
        };
        this.setState({
          currentAccess: tmp,
          editAccess: true
        });
      }
      return tmp;
    });
  };

  closeModalHandler = () => {
    this.setState({
      createAccess: false,
      viewAccess: false,
      editAccess: false,
      deleteAccess: false
    });
  };

  showCreate = () => {
    this.setState({ createAccess: true });
  };

  filterAllAccess(code, data) {
    let role_id = code;
    let allAccess = data;
    let temp = [];
    allAccess.map(ele => {
      if (role_id === ele.role_id) {
        temp.push(ele);
      }
      return temp;
    });
    this.setState({
      allAccess: temp
    });
  }

  changeStatus = status => {
    if (status === 1) {
      return "enabled";
    }
    if (status === 2) {
      return "disabled";
    }
  };

  changeAtribut = atribut => {
    if (atribut === 1) {
      return "read-only";
    }
    if (atribut === 2) {
      return "full-access";
    }
  };

  componentWillReceiveProps(newProps) {
    let role_id = newProps.role.code;
    if (role_id) {
      this.filterAllAccess(newProps.role.code, newProps.ambil.access);
    }
  }

  modalStatus = (status, message, code) => {
    this.setState({
      alertData: {
        status: status,
        message: message,
        code: code
      }
    });
    setTimeout(() => {
      window.location = "/role";
    }, 3000);
  };

  render() {
    return (
      <Modal isOpen={this.props.view} className="modal-dialog modal-lg">
        <ModalHeader>
          <div>
            <h3>{this.props.role.name} </h3>
            <h5>{this.props.role.code} </h5>
            <h6>Description : {this.props.role.description}</h6>
          </div>
        </ModalHeader>
        <ModalBody>
          {this.state.alertData.status === 1 ? (
            <Alert color="success">
              <b>Data {this.state.alertData.message}</b> Data Role dengan
              referensi code <strong>{this.state.alertData.code} </strong>
              telah {this.state.alertData.message}
            </Alert>
          ) : (
            ""
          )}

          <div className="table-responsive">
            <table id="table" className="table">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Menu</th>
                  <th>Status</th>
                  <th>Atribut</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                {this.state.allAccess.map((row, index) => {
                  return (
                    <tr key={index}>
                      <td>{index + 1}</td>
                      <td>{row.menu_name}</td>
                      <td>{this.changeStatus(row.status)}</td>
                      <td>{this.changeAtribut(row.atribut)}</td>
                      <td>
                        <Link
                          to="#"
                          onClick={() => {
                            this.viewModalHandler(row._id);
                          }}
                        >
                          view
                        </Link>{" "}
                        <Link
                          to="#"
                          onClick={() => {
                            this.editModalHandler(row._id);
                          }}
                        >
                          edit
                        </Link>{" "}
                        <Link
                          to="#"
                          onClick={() => {
                            this.deleteModalHandler(row._id);
                          }}
                        >
                          delete
                        </Link>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
            <ViewAccess
              view={this.state.viewAccess}
              closeModalHandler={this.closeModalHandler}
              access={this.state.currentAccess}
            />
            <DeleteAccess
              delete={this.state.deleteAccess}
              access={this.state.currentAccess}
              closeModalHandler={this.closeModalHandler}
              modalStatus={this.modalStatus}
            />
            <CreateAccess
              create={this.state.createAccess}
              closeModalHandler={this.closeModalHandler}
              modalStatus={this.modalStatus}
              allAccess={this.state.allAccess}
              role_id={this.props.role.code}
            />
            <EditAccess
              edit={this.state.editAccess}
              closeModalHandler={this.closeModalHandler}
              currentAccess={this.state.currentAccess}
              modalStatus={this.modalStatus}
              allAccess={this.state.allAccess}
            />
          </div>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={this.showCreate}>
            Tambah Akses
          </Button>
          <Button color="danger" onClick={this.props.closeModalHandler}>
            Tutup
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

ViewRole.propTypes = {
  ambil: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  ambil: state.accessReducers
});

export default connect(mapStateToProps)(ViewRole);
