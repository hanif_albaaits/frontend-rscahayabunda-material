import React from "react";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  Alert
} from "reactstrap";
import Select from "react-select";
import PropTypes from "prop-types";
import apiconfig from "../../../config/api.config.json";

import Konfirm from "../../utility/confirm";
import { connect } from "react-redux";
import { menuUpdate } from "../../../actions/menuActions";

class EditMenu extends React.Component {
  constructor(props) {
    super(props);
    let userdata = JSON.parse(localStorage.getItem(apiconfig.LS.USERDATA));

    this.state = {
      formdata: {
        name: "",
        controller: "",
        parent: ""
      },
      konfirmasi: false,
      currentMenuname: "",
      updated_by: userdata.username,
      status: "",
      menuParent: [],
      alertData: {
        status: false,
        message: ""
      },
      labelWidth: 0
    };
    this.changeHandler = this.changeHandler.bind(this);
  }

  changeHandler(e) {
    let tmp = this.state.formdata;
    tmp[e.target.name] = e.target.value;
    this.setState({
      alertData: {
        status: false,
        message: ""
      },
      formdata: tmp
    });
  }

  select1 = selectedOption => {
    let tmp = this.state.formdata;
    tmp["parent"] = selectedOption.value;
    this.setState({
      formdata: tmp
    });
  };

  validateFilter1(name) {
    let lama = this.props.name;
    let allMenu = this.props.allMenu.map(ele => ele.name);
    let a = allMenu.filter(e => e === name);
    if (a.length === 0 || name === lama) {
      return false;
    } else {
      return true;
    }
  }

  validateFilter2(controller) {
    let lama = this.props.controller;
    let allMenu = this.props.allMenu.map(ele => ele.controller);
    let a = allMenu.filter(e => e === controller);
    if (a.length === 0 || controller === lama) {
      return false;
    } else {
      return true;
    }
  }

  submitValidasi = () => {
    if (
      this.state.formdata.name === "" ||
      this.state.formdata.controller === "" ||
      this.state.formdata.parent === ""
    ) {
      this.setState({
        alertData: {
          status: true,
          message: "Semua field harus di isi!"
        }
      });
    } else if (this.validateFilter1(this.state.formdata.name) === true) {
      this.setState({
        alertData: {
          status: true,
          message: "Ouh, nama menu kamu telah digunakan !"
        }
      });
    } else if (this.validateFilter2(this.state.formdata.controller) === true) {
      this.setState({
        alertData: {
          status: true,
          message: "Ouh, controller kamu telah digunakan !"
        }
      });
    } else {
      this.setState({ konfirmasi: true });
    }
  };

  submitHandler = () => {
    let form = this.state.formdata;
    form["updated_by"] = this.state.updated_by;
    this.props.menuUpdate(this.state.formdata);
    this.props.closeModalHandler();
    this.KonfirmasiTidak();
  };

  KonfirmasiTidak = () => {
    this.setState({ konfirmasi: false });
  };

  componentWillReceiveProps(newProps) {
    this.ambilParent(newProps.allMenu);
    this.setState({
      formdata: newProps.currentMenu,
      status: newProps.ambil.statusPUT
    });
  }

  ambilParent = data => {
    let temp = [
      {
        value: null,
        label: "Membuat Parent"
      }
    ];
    data.map(ele => {
      if (ele.parent === null) {
        temp.push({
          value: ele.code,
          label: ele.name
        });
      }
      return temp;
    });
    this.setState({
      menuParent: temp
    });
  };

  render() {
    const select1 = this.state.menuParent;

    this.state.status === 200
      ? this.props.modalStatus(1, "Di Ubah!", this.state.formdata.name)
      : console.log(this.state.status);
    return (
      <Modal isOpen={this.props.edit} className={this.props.className}>
        <ModalHeader>Ubah Data Menu</ModalHeader>
        <ModalBody>
          <Konfirm
            showKonfirmasi={this.state.konfirmasi}
            ubahKonfirmasi={this.KonfirmasiTidak}
            submitHandler={this.submitHandler}
          />
          <Form>
            <FormGroup>
              <Label for="name">Nama Menu</Label>
              <Input
                type="text"
                name="name"
                id="name"
                placeholder="masukan name"
                value={this.state.formdata.name}
                onChange={this.changeHandler}
              />
            </FormGroup>
            <FormGroup>
              <Label for="controller">Controller Menu</Label>
              <Input
                type="text"
                name="controller"
                id="controller"
                placeholder="Controller Menu"
                value={this.state.formdata.controller}
                onChange={this.changeHandler}
              />
            </FormGroup>
            <FormGroup>
              <Label for="selectparent">
                Pilih Menu Parent / Membuat Parent
              </Label>
              <Select
                className="basic-single"
                classNamePrefix="select"
                label="Select Parent"
                name="parent"
                options={select1}
                value={this.state.parent}
                onChange={this.select1}
              />
            </FormGroup>
          </Form>
        </ModalBody>
        <ModalFooter>
          {this.state.alertData.status === true ? (
            <Alert color="danger">{this.state.alertData.message} </Alert>
          ) : (
            ""
          )}
          <Button
            variant="contained"
            color="primary"
            onClick={this.submitValidasi}
          >
            Ubah
          </Button>
          <Button variant="contained" onClick={this.props.closeModalHandler}>
            Batal
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

EditMenu.propTypes = {
  menuUpdate: PropTypes.func.isRequired,
  ambil: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  ambil: state.menuReducers
});

export default connect(
  mapStateToProps,
  { menuUpdate }
)(EditMenu);
