import React from "react";
import { Modal, ModalBody, ModalFooter, ModalHeader, Button } from "reactstrap";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";

const styles = theme => ({
  root: {
    width: "100%",
    flexGrow: 1
  },
  table: {
    minWidth: 700
  }
});

class ViewMenu extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <Modal isOpen={this.props.view} className={this.props.className}>
        <ModalHeader> Lihat Menu</ModalHeader>
        <ModalBody>
          <div>
            <h3>{this.props.menu.name} </h3>
            <h5>{this.props.menu.code} </h5>
          </div>
          <div className={classes.root}>
            <Grid container spacing={24}>
              <Grid item xs={6}>
                Menu ID
                <br />
                Nama Menu
                <br />
                Controller
                <br />
                Parent
                <br />
                Created By
                <br />
                Created Date
                <br />
                Updated By
                <br />
                Updated Date
              </Grid>
              <Grid item xs={6}>
                {this.props.menu.code}
                <br />
                {this.props.menu.name}
                <br />
                {this.props.menu.controller}
                <br />
                {this.props.menu.parent}
                <br />
                {this.props.menu.created_by}
                <br />
                {this.props.menu.created_date}
                <br />
                {this.props.menu.updated_by}
                <br />
                {this.props.menu.updated_date}
              </Grid>
            </Grid>
          </div>
        </ModalBody>
        <ModalFooter>
          <Button color="danger" onClick={this.props.closeModalHandler}>
            Tutup
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

ViewMenu.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ViewMenu);
