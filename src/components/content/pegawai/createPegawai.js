import React from "react";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  Alert
} from "reactstrap";
import Select from "react-select";
import PropTypes from "prop-types";
import apiconfig from "../../../config/api.config.json";

import Konfirm from "../../utility/confirm";
import { connect } from "react-redux";
import { pegawaiCreate } from "../../../actions/pegawaiActions";

class CreatePegawai extends React.Component {
  constructor(props) {
    super(props);
    let userdata = JSON.parse(localStorage.getItem(apiconfig.LS.USERDATA));
    this.state = {
      formdata: {
        kd_pegawai_rs: "",
        kd_divisi: "",
        first_name: "",
        last_name: "",
        email: "",
        no_hp: "",
        alamat: "",
        created_by: userdata.username
      },
      konfirmasi: false,
      status: "",
      alertData: {
        status: false,
        message: ""
      }
    };
    this.changeHandler = this.changeHandler.bind(this);
  }

  changeHandler(e) {
    let tmp = this.state.formdata;
    tmp[e.target.name] = e.target.value;
    this.setState({
      alertData: {
        status: false,
        message: ""
      },
      formdata: tmp
    });
  }

  select1 = selectedOption => {
    let tmp = this.state.formdata;
    tmp["kd_divisi"] = selectedOption.value;
    this.setState({
      formdata: tmp
    });
  };

  validateFilter(kode) {
    let allPegawai = this.props.allPegawai.map(ele => ele.kd_pegawai_rs);
    let a = allPegawai.filter(e => e === kode);
    if (a.length === 0) {
      return false;
    } else {
      return true;
    }
  }

  validateEmail(email) {
    let regex = new RegExp(
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    );
    return regex.test(String(email).toLowerCase());
  }

  validatePhone(phone) {
    let regex = new RegExp(/^[0-9-+,()]{9,15}$/);
    return regex.test(phone);
  }

  submitValidasi = () => {
    const {
      kd_pegawai_rs,
      kd_divisi,
      first_name,
      last_name,
      email,
      no_hp,
      alamat
    } = this.state.formdata;
    if (
      kd_pegawai_rs === "" ||
      kd_divisi === "" ||
      first_name === "" ||
      last_name === "" ||
      email === "" ||
      no_hp === "" ||
      alamat === ""
    ) {
      this.setState({
        alertData: {
          status: true,
          message: "Semua field harus di isi!"
        }
      });
    } else if (this.validateFilter(kd_pegawai_rs) === true) {
      this.setState({
        alertData: {
          status: true,
          message: "Kode pegawai RS telah digunakan !"
        }
      });
    } else if (this.validateEmail(email) === false) {
      this.setState({
        alertData: {
          status: true,
          message: "Format email salah, ketik di bagian email dengan benar!"
        }
      });
    } else if (this.validatePhone(no_hp) === false) {
      this.setState({
        alertData: {
          status: true,
          message:
            "Format nomor handphone salah, ketik di bagian nomor dengan benar!"
        }
      });
    } else {
      this.setState({ konfirmasi: true });
    }
  };

  submitHandler = () => {
    this.props.pegawaiCreate(this.state.formdata);
    this.props.closeModalHandler();
    this.KonfirmasiTidak();
  };

  componentWillReceiveProps(newStatus) {
    this.setState({
      status: newStatus.ambil.statusADD
    });
  }

  KonfirmasiTidak = () => {
    this.setState({ konfirmasi: false });
  };

  render() {
    const select1 = this.props.ambil1.divisi.map((row, x) => {
      return {
        value: row.code,
        label: row.name
      };
    });
    this.state.status === 200
      ? this.props.modalStatus(
          1,
          "Di Tambahkan",
          this.state.formdata.first_name + " " + this.state.formdata.last_name
        )
      : console.log(this.state.status);
    return (
      <Modal isOpen={this.props.create} className={this.props.className}>
        <ModalHeader>Tambah Pegawai Baru</ModalHeader>
        <ModalBody>
          <Konfirm
            showKonfirmasi={this.state.konfirmasi}
            ubahKonfirmasi={this.KonfirmasiTidak}
            submitHandler={this.submitHandler}
          />
          <Form>
            <FormGroup>
              <Label for="kd_pegawai_rs">Kode Pegawai RS</Label>
              <Input
                type="text"
                name="kd_pegawai_rs"
                id="kd_pegawai_rs"
                placeholder="Masukan kode pegawai RS"
                value={this.state.formdata.kd_pegawai_rs}
                onChange={this.changeHandler}
              />
            </FormGroup>
            <FormGroup>
              <Label for="selectdivisi">Pilih Divisi</Label>
              <Select
                className="basic-single"
                classNamePrefix="select"
                label="Select Divisi"
                name="kd_divisi"
                options={select1}
                value={this.state.kd_divisi}
                onChange={this.select1}
              />
            </FormGroup>
            <FormGroup>
              <Label for="first_name">Nama Pertama</Label>
              <Input
                type="text"
                name="first_name"
                id="first_name"
                placeholder="Masukan nama pertama"
                value={this.state.formdata.first_name}
                onChange={this.changeHandler}
              />
            </FormGroup>
            <FormGroup>
              <Label for="last_name">Nama Terakhir</Label>
              <Input
                type="text"
                name="last_name"
                id="last_name"
                placeholder="Masukan nama terakhir"
                value={this.state.formdata.last_name}
                onChange={this.changeHandler}
              />
            </FormGroup>
            <FormGroup>
              <Label for="email">Email</Label>
              <Input
                type="text"
                name="email"
                id="email"
                placeholder="Masukan email"
                value={this.state.formdata.email}
                onChange={this.changeHandler}
              />
            </FormGroup>
            <FormGroup>
              <Label for="no_hp">No Handphone</Label>
              <Input
                type="number"
                name="no_hp"
                id="no_hp"
                placeholder="Masukan nomor handphone"
                value={this.state.formdata.no_hp}
                onChange={this.changeHandler}
              />
            </FormGroup>
            <FormGroup>
              <Label for="alamat">Alamat</Label>
              <Input
                type="text"
                name="alamat"
                id="alamat"
                placeholder="Masukan alamat"
                value={this.state.formdata.alamat}
                onChange={this.changeHandler}
              />
            </FormGroup>
          </Form>
        </ModalBody>
        <ModalFooter>
          {this.state.alertData.status === true ? (
            <Alert color="danger">{this.state.alertData.message} </Alert>
          ) : (
            ""
          )}
          <Button
            variant="contained"
            color="primary"
            onClick={this.submitValidasi}
          >
            Tambahkan
          </Button>
          <Button variant="contained" onClick={this.props.closeModalHandler}>
            Batal
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

CreatePegawai.propTypes = {
  pegawaiCreate: PropTypes.func.isRequired,
  ambil: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  ambil: state.pegawaiReducers,
  ambil1: state.divisiReducers
});

export default connect(
  mapStateToProps,
  { pegawaiCreate }
)(CreatePegawai);
