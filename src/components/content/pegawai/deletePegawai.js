import React from "react";
import { Modal, ModalBody, ModalFooter, ModalHeader, Button } from "reactstrap";
import { pegawaiDelete } from "../../../actions/pegawaiActions";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import apiconfig from "../../../config/api.config.json";

class DeletePegawai extends React.Component {
  constructor(props) {
    super(props);
    let userdata = JSON.parse(localStorage.getItem(apiconfig.LS.USERDATA));
    this.state = {
      status: "",
      updated_by: userdata.username
    };
    this.deleteHandler = this.deleteHandler.bind(this);
  }

  deleteHandler() {
    let data = this.props.pegawai;
    data["updated_by"] = this.state.updated_by;
    this.props.pegawaiDelete(data);
    this.props.closeModalHandler();
  }

  componentWillReceiveProps(newStatus) {
    this.setState({
      status: newStatus.ambil.statusDEL
    });
  }

  render() {
    this.state.status === 200
      ? this.props.modalStatus(1, "Di Hapus!", this.props.pegawai.name)
      : console.log(this.state.status);

    return (
      <Modal isOpen={this.props.delete} className={this.props.className}>
        <ModalHeader> Hapus Pegawai </ModalHeader>
        <ModalBody>
          <p>
            Apa anda ingin menghapus pegawai{" "}
            <strong>{this.props.pegawai.name}</strong> ?
          </p>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={this.deleteHandler}>
            Ya
          </Button>
          <Button color="danger" onClick={this.props.closeModalHandler}>
            Tidak
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

DeletePegawai.propTypes = {
  pegawaiDelete: PropTypes.func.isRequired,
  ambil: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  ambil: state.pegawaiReducers
});

export default connect(
  mapStateToProps,
  { pegawaiDelete }
)(DeletePegawai);
