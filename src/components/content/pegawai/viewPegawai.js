import React from "react";
import { Modal, ModalBody, ModalFooter, ModalHeader, Button } from "reactstrap";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";

const styles = theme => ({
  root: {
    width: "100%",
    flexGrow: 1
  },
  table: {
    minWidth: 700
  }
});

class ViewPegawai extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <Modal isOpen={this.props.view} className={this.props.className}>
        <ModalHeader> Lihat Pegawai</ModalHeader>
        <ModalBody>
          <div>
            <h3>{this.props.pegawai.name} </h3>
            <h5>{this.props.pegawai.kd_pegawai} </h5>
          </div>
          <div className={classes.root}>
            <Grid container spacing={24}>
              <Grid item xs={6}>
                Kode Pegawai
                <br />
                No Pegawai RS
                <br />
                Nama Pegawai
                <br />
                Email
                <br />
                No Handphone
                <br />
                Alamat
                <br />
                Created By
                <br />
                Created Date
                <br />
                Updated By
                <br />
                Updated Date
              </Grid>
              <Grid item xs={6}>
                {this.props.pegawai.kd_pegawai}
                <br />
                {this.props.pegawai.kd_pegawai_rs}
                <br />
                {this.props.pegawai.name}
                <br />
                {this.props.pegawai.email}
                <br />
                {this.props.pegawai.no_hp}
                <br />
                {this.props.pegawai.alamat}
                <br />
                {this.props.pegawai.created_by}
                <br />
                {this.props.pegawai.created_date}
                <br />
                {this.props.pegawai.updated_by}
                <br />
                {this.props.pegawai.updated_date}
              </Grid>
            </Grid>
          </div>
        </ModalBody>
        <ModalFooter>
          <Button color="danger" onClick={this.props.closeModalHandler}>
            Tutup
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

ViewPegawai.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ViewPegawai);
