import React from "react";
import { Modal, ModalBody, ModalFooter, ModalHeader, Button } from "reactstrap";
import { tambahDelete } from "../../../actions/barangTambahActions";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import apiconfig from "../../../config/api.config.json";

class DeleteBarangT extends React.Component {
  constructor(props) {
    super(props);
    let userdata = JSON.parse(localStorage.getItem(apiconfig.LS.USERDATA));
    this.state = {
      status: "",
      updated_by: userdata.username
    };
    this.deleteHandler = this.deleteHandler.bind(this);
  }

  deleteHandler() {
    let data = this.props.tambah;
    data["updated_by"] = this.state.updated_by;
    this.props.tambahDelete(data);
    this.props.closeModalHandler();
  }

  componentWillReceiveProps(newStatus) {
    this.setState({
      status: newStatus.ambil1.statusDEL
    });
  }

  render() {
    this.state.status === 200
      ? this.props.modalStatus(1, "Di Hapus!", this.props.tambah.kd_tambah)
      : console.log(this.state.status);

    return (
      <Modal isOpen={this.props.delete} className={this.props.className}>
        <ModalHeader> Hapus data pengambilan barang </ModalHeader>
        <ModalBody>
          <p>
            Apa anda ingin menghapus data{" "}
            <strong>{this.props.tambah.kd_tambah}</strong> ?
          </p>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={this.deleteHandler}>
            Ya
          </Button>
          <Button color="danger" onClick={this.props.closeModalHandler}>
            Tidak
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

DeleteBarangT.propTypes = {
  tambahDelete: PropTypes.func.isRequired,
  ambil1: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  ambil1: state.tambahBReducers
});

export default connect(
  mapStateToProps,
  { tambahDelete }
)(DeleteBarangT);
