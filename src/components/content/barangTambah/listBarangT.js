import React, { Fragment } from "react";
import { Alert } from "reactstrap";
import { getTambah } from "../../../actions/barangTambahActions";
import { getPegawai } from "../../../actions/pegawaiActions";
import { getBarang } from "../../../actions/barangActions";
import { connect } from "react-redux";

import CreateBarangT from "./createBarangT";
import DeleteBarangT from "./deleteBarangT";
import ViewBarangT from "./viewBarangT";
import printTable from "../../utility/printTable";
import exportCsv from "../../utility/exportCsv";
import bacaAkses from "../../utility/bacaAkses";
import Spinner from "../../utility/spinnerTable";

import PropTypes from "prop-types";
import moment from "moment";

import IconButton from "@material-ui/core/IconButton";
import FirstPageIcon from "@material-ui/icons/FirstPage";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import LastPageIcon from "@material-ui/icons/LastPage";
import TablePagination from "@material-ui/core/TablePagination";
import { withStyles } from "@material-ui/core/styles";

class LisBarangT extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      formCari: {
        kd_tambah: /(?:)/,
        kd_penambah: /(?:)/,
        kd_barang: /(?:)/,
        tanggal_masuk: /(?:)/
      },
      AKSES: {},
      createBarangT: false,
      deleteBarangT: false,
      viewBarangT: false,
      allBarangT: [],
      currentBarangT: {},
      alertData: {
        status: 0,
        message: "",
        code: ""
      },
      hasil: [],
      page: 0,
      rowsPerPage: 5
    };
    this.showCreate = this.showCreate.bind(this);
  }
  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  deleteModalHandler = tambahid => {
    let tmp = {};
    this.state.allBarangT.map(ele => {
      if (tambahid === ele._id) {
        tmp = ele;
      }
      return tmp;
    });
    this.setState({
      currentBarangT: tmp,
      deleteBarangT: true
    });
  };

  viewModalHandler = tambahid => {
    let tmp = {};
    this.state.allBarangT.map(ele => {
      if (tambahid === ele._id) {
        tmp = ele;
      }
      return tmp;
    });
    this.setState({
      currentBarangT: tmp,
      viewBarangT: true
    });
  };

  changeHandler = event => {
    let data = event.target.value;
    let char = data.replace(/\\/g, "");
    let tmp = this.state.formCari;
    if (event.target.name) {
      tmp[event.target.name] = new RegExp(char.toLowerCase());
    } else {
      tmp[event.target.name] = char;
    }
    this.cariData();
  };

  cariData = () => {
    const {
      kd_tambah,
      kd_penambah,
      kd_barang,
      tanggal_masuk
    } = this.state.formCari;
    let temp = [];
    this.state.allBarangT.map(ele => {
      if (
        kd_tambah.test(ele.kd_tambah.toLowerCase()) &&
        kd_penambah.test(ele.kd_penambah.toLowerCase()) &&
        kd_barang.test(ele.kd_barang.toLowerCase()) &&
        tanggal_masuk.test(ele.tanggal_masuk)
      ) {
        temp.push(ele);
      }
      return temp;
    });
    this.setState({
      hasil: temp
    });
  };

  closeModalHandler = () => {
    this.setState({
      createBarangT: false,
      viewBarangT: false,
      deleteBarangT: false
    });
  };

  showCreate = () => {
    this.setState({ createBarangT: true });
  };

  componentDidMount() {
    this.props.getTambah();
    this.props.getBarang();
    this.props.getPegawai();
  }

  componentWillReceiveProps(newProps) {
    let a = bacaAkses(newProps.ambilAccess.side, window.location.pathname);
    let allBarangT = newProps.ambil.tambah;

    if (a !== null) {
      this.setState({
        AKSES: a
      });
    }
    if (allBarangT !== null) {
      this.setState({
        allBarangT: allBarangT,
        hasil: allBarangT,
        loading: false
      });
    }
  }

  modalStatus = (status, message, code) => {
    this.setState({
      alertData: {
        status: status,
        message: message,
        code: code
      },
      viewBarangT: false,
      deleteBarangT: false,
      createBarangT: false
    });
    setTimeout(() => {
      window.location.href = "/brg-tambah";
    }, 3000);
  };

  changeDateFormat = tanggal => {
    if (tanggal === null) {
      return tanggal;
    } else {
      return moment(tanggal).format("DD/MM/YYYY");
    }
  };

  render() {
    const AKSES = this.state.AKSES;
    const loading = this.state.loading;
    let tablelist;

    if (loading === true) {
      tablelist = <Spinner />;
    } else {
      if (this.state.allBarangT.length > 0) {
        tablelist = this.state.hasil
          .slice(
            this.state.page * this.state.rowsPerPage,
            this.state.page * this.state.rowsPerPage + this.state.rowsPerPage
          )
          .map((row, index) => {
            return (
              <tr key={index}>
                <td className="align-middle">{row.kd_tambah}</td>
                <td className="align-middle">{row.kd_penambah}</td>
                <td className="align-middle">{row.kd_barang}</td>
                <td className="align-middle">{row.nama_pegawai}</td>
                <td className="align-middle">{row.nama_barang}</td>
                <td className="align-middle">{row.jumlah_masuk}</td>
                <td className="align-middle">
                  {this.changeDateFormat(row.tanggal_masuk)}
                </td>
                <td className="align-middle text-center">
                  {AKSES.atribut === 1 || AKSES.atribut === 2 ? (
                    <button
                      type="button"
                      className="btn btn-icon btn-flat btn-default"
                      title="view"
                      onClick={() => {
                        this.viewModalHandler(row._id);
                      }}
                    >
                      <i className="icon md-search" aria-hidden="false" />
                    </button>
                  ) : (
                    ""
                  )}
                  {AKSES.atribut === 2 ? (
                    <Fragment>
                      <button
                        type="button"
                        className="btn btn-sm btn-icon btn-flat btn-default"
                        title="Delete"
                        onClick={() => {
                          this.deleteModalHandler(row._id);
                        }}
                      >
                        <i className="icon md-delete" aria-hidden="false" />
                      </button>
                    </Fragment>
                  ) : (
                    ""
                  )}
                </td>
              </tr>
            );
          });
      } else {
        tablelist = (
          <tr className="text-center">
            <td colSpan="10">Tidak Ada data yang tersedia !</td>
          </tr>
        );
      }
    }

    return (
      <div>
        <div className="page-header">
          <h1 className="page-title">Data Penambahan</h1>
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <a href="/dashboard">Home</a>
            </li>
            <li className="breadcrumb-item">
              <a href="/barang">Manajemen Barang</a>
            </li>
            <li className="breadcrumb-item active">Data Penambahan</li>
          </ol>
          <div className="page-header-actions">
            {AKSES.atribut === 2 ? (
              <div>
                <button
                  type="button"
                  className="btn btn-primary"
                  onClick={this.showCreate}
                >
                  Tambah Stok
                </button>
              </div>
            ) : (
              ""
            )}
          </div>
        </div>

        <div className="page-content">
          {this.state.alertData.status === 1 ? (
            <Alert color="success">
              <b>Data {this.state.alertData.message}</b> Data Tambah Stok dengan
              referensi <strong>{this.state.alertData.code} </strong>
              telah {this.state.alertData.message}
            </Alert>
          ) : (
            ""
          )}
          {this.state.alertData.status === 2 ? (
            <Alert color="danger">{this.state.alertData.message} </Alert>
          ) : (
            ""
          )}
          <div className="panel">
            <header className="panel-heading">
              <h3 className="panel-title">List Penambahan</h3>
            </header>
            <div className="panel-body">
              <form className="form-inline">
                <div className="form-group">
                  <input
                    type="text"
                    placeholder="cari kode penambahan"
                    className="form-control mx-sm-1"
                    name="kd_tambah"
                    onChange={this.changeHandler}
                    autoComplete="off"
                  />
                  <input
                    type="text"
                    placeholder="cari ID pegawai"
                    className="form-control mx-sm-1"
                    name="kd_penambah"
                    onChange={this.changeHandler}
                    autoComplete="off"
                  />
                  <input
                    type="text"
                    placeholder="cari kode barang"
                    className="form-control mx-sm-1"
                    name="kd_barang"
                    onChange={this.changeHandler}
                    autoComplete="off"
                  />
                  <input
                    type="date"
                    className="form-control mx-sm-1"
                    name="tanggal_masuk"
                    onChange={this.changeHandler}
                  />
                </div>
              </form>
              <div className="example table-responsive">
                <table id="table" className="table table-bordered">
                  <thead>
                    <tr>
                      <th>Kode Tambah Stok</th>
                      <th>ID Pegawai</th>
                      <th>Kode Barang</th>
                      <th>Nama Pegawai</th>
                      <th>Nama Barang</th>
                      <th>Jumlah Barang</th>
                      <th>Tanggal Masuk</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>{tablelist}</tbody>
                  <TablePagination
                    colSpan={8}
                    count={this.state.hasil.length}
                    rowsPerPage={this.state.rowsPerPage}
                    page={this.state.page}
                    onChangePage={this.handleChangePage}
                    onChangeRowsPerPage={this.handleChangeRowsPerPage}
                    ActionsComponent={TablePaginationActionsWrapped}
                  />
                </table>
                <button
                  type="button"
                  className="btn btn-primary mx-sm-1"
                  hidden={true}
                  onClick={() => {
                    printTable("table");
                  }}
                >
                  Print
                </button>
                <button
                  type="button"
                  className="btn btn-primary mx-sm-1"
                  onClick={() => {
                    exportCsv("t_tambah_barang");
                  }}
                >
                  Export to CSV
                </button>
                <CreateBarangT
                  create={this.state.createBarangT}
                  closeModalHandler={this.closeModalHandler}
                  modalStatus={this.modalStatus}
                  allBarangT={this.state.allBarangT}
                />
                <ViewBarangT
                  view={this.state.viewBarangT}
                  closeModalHandler={this.closeModalHandler}
                  tambah={this.state.currentBarangT}
                />
                <DeleteBarangT
                  delete={this.state.deleteBarangT}
                  tambah={this.state.currentBarangT}
                  closeModalHandler={this.closeModalHandler}
                  modalStatus={this.modalStatus}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

LisBarangT.propTypes = {
  getTambah: PropTypes.func.isRequired,
  getPegawai: PropTypes.func.isRequired,
  getBarang: PropTypes.func.isRequired,
  ambil: PropTypes.object.isRequired,
  ambilAccess: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  ambil: state.tambahBReducers,
  ambilAccess: state.menuReducers
});

export default connect(
  mapStateToProps,
  { getTambah, getPegawai, getBarang }
)(LisBarangT);

const actionsStyles = theme => ({
  root: {
    flexShrink: 0,
    color: theme.palette.text.secondary,
    marginLeft: theme.spacing.unit * 2.5
  }
});

class TablePaginationActions extends React.Component {
  handleFirstPageButtonClick = event => {
    this.props.onChangePage(event, 0);
  };

  handleBackButtonClick = event => {
    this.props.onChangePage(event, this.props.page - 1);
  };

  handleNextButtonClick = event => {
    this.props.onChangePage(event, this.props.page + 1);
  };

  handleLastPageButtonClick = event => {
    this.props.onChangePage(
      event,
      Math.max(0, Math.ceil(this.props.count / this.props.rowsPerPage) - 1)
    );
  };

  render() {
    const { classes, count, page, rowsPerPage, theme } = this.props;

    return (
      <div className={classes.root}>
        <IconButton
          onClick={this.handleFirstPageButtonClick}
          disabled={page === 0}
          aria-label="First Page"
        >
          {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
        </IconButton>
        <IconButton
          onClick={this.handleBackButtonClick}
          disabled={page === 0}
          aria-label="Previous Page"
        >
          {theme.direction === "rtl" ? (
            <KeyboardArrowRight />
          ) : (
            <KeyboardArrowLeft />
          )}
        </IconButton>
        <IconButton
          onClick={this.handleNextButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="Next Page"
        >
          {theme.direction === "rtl" ? (
            <KeyboardArrowLeft />
          ) : (
            <KeyboardArrowRight />
          )}
        </IconButton>
        <IconButton
          onClick={this.handleLastPageButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="Last Page"
        >
          {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
        </IconButton>
      </div>
    );
  }
}

TablePaginationActions.propTypes = {
  classes: PropTypes.object.isRequired,
  count: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
  theme: PropTypes.object.isRequired
};

const TablePaginationActionsWrapped = withStyles(actionsStyles, {
  withTheme: true
})(TablePaginationActions);
