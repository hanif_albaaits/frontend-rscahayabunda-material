import React from "react";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  Alert
} from "reactstrap";
import Select from "react-select";
import PropTypes from "prop-types";
import apiconfig from "../../../config/api.config.json";
import moment from "moment";

import Konfirm from "../../utility/confirm";
import { connect } from "react-redux";
import { tambahCreate } from "../../../actions/barangTambahActions";

class CreateBarangT extends React.Component {
  constructor(props) {
    super(props);
    let userdata = JSON.parse(localStorage.getItem(apiconfig.LS.USERDATA));
    this.state = {
      formdata: {
        kd_penambah: "",
        kd_barang: "",
        lokasi: "",
        kondisi: "",
        catatan: "",
        cara_perolehan: "",
        jumlah_masuk: "",
        tanggal_masuk: "",
        created_by: userdata.username
      },
      konfirmasi: false,
      status: "",
      alertData: {
        status: false,
        message: ""
      }
    };
    this.changeHandler = this.changeHandler.bind(this);
  }

  changeHandler(e) {
    let tmp = this.state.formdata;
    tmp[e.target.name] = e.target.value;
    this.setState({
      alertData: {
        status: false,
        message: ""
      },
      formdata: tmp
    });
  }

  select1 = selectedOption => {
    let tmp = this.state.formdata;
    tmp[selectedOption.name] = selectedOption.value;
    this.setState({
      formdata: tmp
    });
  };

  submitValidasi = () => {
    const {
      kd_penambah,
      kd_barang,
      lokasi,
      kondisi,
      catatan,
      cara_perolehan,
      jumlah_masuk,
      tanggal_masuk
    } = this.state.formdata;
    if (
      kd_penambah === "" ||
      kd_barang === "" ||
      lokasi === "" ||
      kondisi === "" ||
      catatan === "" ||
      cara_perolehan === "" ||
      jumlah_masuk === "" ||
      tanggal_masuk === ""
    ) {
      this.setState({
        alertData: {
          status: true,
          message: "Semua field harus di isi!"
        }
      });
    } else {
      this.setState({ konfirmasi: true });
    }
  };

  submitHandler = () => {
    let form = this.state.formdata;
    form["status"] = 1;
    this.props.tambahCreate(form);
    this.props.closeModalHandler();
    this.KonfirmasiTidak();
  };

  componentWillReceiveProps(newStatus) {
    this.setState({
      status: newStatus.ambil.statusADD
    });
  }

  KonfirmasiTidak = () => {
    this.setState({ konfirmasi: false });
  };

  render() {
    const sBarang = this.props.ambilBarang.barang.map((row, x) => {
      return {
        value: row.kd_barang,
        label: row.nama + " " + row.merk,
        name: "kd_barang"
      };
    });
    const sPegawai = this.props.ambilPegawai.pegawai.map((row, x) => {
      return {
        value: row.kd_pegawai,
        label: row.name,
        name: "kd_penambah"
      };
    });
    const t_min = moment()
      .subtract(1, "weeks")
      .format("YYYY-MM-DD");
    const t_max = moment()
      .add(1, "weeks")
      .format("YYYY-MM-DD");

    this.state.status === 200
      ? this.props.modalStatus(1, "Di Tambahkan", this.state.formdata.kd_barang)
      : console.log(this.state.status);
    return (
      <Modal isOpen={this.props.create} className="modal-dialog modal-lg">
        <ModalHeader>Tambah Stock Barang </ModalHeader>
        <ModalBody>
          <Konfirm
            showKonfirmasi={this.state.konfirmasi}
            ubahKonfirmasi={this.KonfirmasiTidak}
            submitHandler={this.submitHandler}
          />
          <Form>
            <FormGroup>
              <Label for="sBarang">Pilih Barang yang ditambahkan :</Label>
              <Select
                className="basic-single"
                classNamePrefix="select"
                label="sBarang"
                name="kd_barang"
                options={sBarang}
                value={this.state.kd_barang}
                onChange={this.select1}
              />
            </FormGroup>
            <FormGroup>
              <Label for="sPegawai">Pilih Pegawai yang menambahkan:</Label>
              <Select
                className="basic-single"
                classNamePrefix="select"
                label="sPegawai"
                name="kd_penambah"
                options={sPegawai}
                value={this.state.kd_penambah}
                onChange={this.select1}
              />
            </FormGroup>
            <FormGroup>
              <Label for="lokasi">Asal perolehan barang:</Label>
              <Input
                type="text"
                name="lokasi"
                id="lokasi"
                placeholder="Asal perolehan barang"
                value={this.state.formdata.lokasi}
                onChange={this.changeHandler}
              />
            </FormGroup>
            <FormGroup>
              <Label for="kondisi">Kondisi barang saat ditambahkan :</Label>
              <Input
                type="text"
                name="kondisi"
                id="kondisi"
                placeholder="Masukan kondisi barang"
                value={this.state.formdata.kondisi}
                onChange={this.changeHandler}
              />
            </FormGroup>
            <FormGroup>
              <Label for="catatan">Catatan barang:</Label>
              <Input
                type="text"
                name="catatan"
                id="catatan"
                placeholder="catatan"
                value={this.state.formdata.catatan}
                onChange={this.changeHandler}
              />
            </FormGroup>
            <FormGroup>
              <Label for="cara_perolehan">Cara perolehan barang :</Label>
              <Input
                type="text"
                name="cara_perolehan"
                id="cara_perolehan"
                placeholder="Cara perolehan barang"
                value={this.state.formdata.cara_perolehan}
                onChange={this.changeHandler}
              />
            </FormGroup>
            <FormGroup>
              <Label for="jumlah_masuk">Jumlah pemasukan barang :</Label>
              <Input
                type="number"
                name="jumlah_masuk"
                id="jumlah_masuk"
                placeholder="jumlah barang yang ditambahkan"
                value={this.state.formdata.jumlah_masuk}
                onChange={this.changeHandler}
              />
            </FormGroup>
            <FormGroup>
              <Label for="tanggal_masuk">Tanggal masuk barang :</Label>
              <Input
                type="date"
                name="tanggal_masuk"
                id="tanggal_masuk"
                value={this.state.formdata.tanggal_masuk}
                onChange={this.changeHandler}
                min={t_min}
                max={t_max}
              />
            </FormGroup>
          </Form>
        </ModalBody>
        <ModalFooter>
          {this.state.alertData.status === true ? (
            <Alert color="danger">{this.state.alertData.message} </Alert>
          ) : (
            ""
          )}
          <Button
            variant="contained"
            color="primary"
            onClick={this.submitValidasi}
          >
            Tambahkan
          </Button>
          <Button variant="contained" onClick={this.props.closeModalHandler}>
            Batal
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

CreateBarangT.propTypes = {
  tambahCreate: PropTypes.func.isRequired,
  ambil: PropTypes.object.isRequired,
  ambilPegawai: PropTypes.object.isRequired,
  ambilBarang: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  ambil: state.tambahBReducers,
  ambilBarang: state.barangReducers,
  ambilPegawai: state.pegawaiReducers
});

export default connect(
  mapStateToProps,
  { tambahCreate }
)(CreateBarangT);
