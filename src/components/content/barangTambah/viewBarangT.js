import React from "react";
import { Modal, ModalBody, ModalFooter, ModalHeader, Button } from "reactstrap";

class viewBarangT extends React.Component {
  render() {
    return (
      <Modal isOpen={this.props.view} className="modal-lg">
        <ModalHeader>
          Data penambahan stok {this.props.tambah.kd_tambah}
        </ModalHeader>
        <ModalBody>
          <form>
            <div className="card mb-3">
              <div className="card-header">Barang yang di tambah </div>
              <div className="card-body">
                <div className="form-row">
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="nama_barang"
                      className="col-4 col-form-label text-right"
                    >
                      Nama Barang
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.tambah.nama_barang}
                        disabled
                      />
                    </div>
                  </div>
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="merk"
                      className="col-4 col-form-label text-right"
                    >
                      Merk
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.tambah.merk_barang}
                        disabled
                      />
                    </div>
                  </div>
                </div>
                <div className="form-row">
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="ukuran"
                      className="col-4 col-form-label text-right"
                    >
                      Ukuran
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.tambah.ukuran_barang}
                        disabled
                      />
                    </div>
                  </div>
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="bahan"
                      className="col-4 col-form-label text-right"
                    >
                      Bahan
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.tambah.bahan_barang}
                        disabled
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="card mb-3">
              <div className="card-header">Data Pegawai yang menambahkan</div>
              <div className="card-body">
                <div className="form-row">
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="nama"
                      className="col-4 col-form-label text-right"
                    >
                      Nama Pegawai
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.tambah.nama_pegawai}
                        disabled
                      />
                    </div>
                  </div>
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="email"
                      className="col-4 col-form-label text-right"
                    >
                      Email
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.tambah.email_pegawai}
                        disabled
                      />
                    </div>
                  </div>
                </div>
                <div className="form-row">
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="no_hp"
                      className="col-4 col-form-label text-right"
                    >
                      No HP
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.tambah.nohp_pegawai}
                        disabled
                      />
                    </div>
                  </div>
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="alamat"
                      className="col-4 col-form-label text-right"
                    >
                      Alamat
                    </label>
                    <div className="col-8">
                      <textarea
                        type="text"
                        className="form-control"
                        value={this.props.tambah.alamat_pegawai}
                        disabled
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="card mb-3">
              <div className="card-header">Data Penambahan barang</div>
              <div className="card-body">
                <div className="form-row">
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="kondisi"
                      className="col-4 col-form-label text-right"
                    >
                      Kondisi :
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.tambah.kondisi}
                        disabled
                      />
                    </div>
                  </div>
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="Catatan"
                      className="col-4 col-form-label text-right"
                    >
                      Catatan :
                    </label>
                    <div className="col-8">
                      <textarea
                        type="text"
                        className="form-control"
                        value={this.props.tambah.catatan}
                        disabled
                      />
                    </div>
                  </div>
                </div>
                <div className="form-row">
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="jumlah_masuk"
                      className="col-4 col-form-label text-right"
                    >
                      Jumlah :
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.tambah.jumlah_masuk}
                        disabled
                      />
                    </div>
                  </div>
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="tanggal_tambah"
                      className="col-4 col-form-label text-right"
                    >
                      Tanggal Masuk
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.tambah.tanggal_masuk}
                        disabled
                      />
                    </div>
                  </div>
                </div>
                <div className="form-row">
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="lokasi"
                      className="col-4 col-form-label text-right"
                    >
                      Asal barang :
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.tambah.lokasi}
                        disabled
                      />
                    </div>
                  </div>
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="cara_perolehan"
                      className="col-4 col-form-label text-right"
                    >
                      Cara Perolehan :
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.tambah.cara_perolehan}
                        disabled
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div>
              Dibuat Oleh : {this.props.tambah.created_by + ",   "}
              Tanggal Dibuat :{this.props.tambah.created_date}
            </div>
            <div>
              Diubah Oleh : {this.props.tambah.updated_by + ",   "}
              Tanggal Diubah :{this.props.tambah.updated_date}
            </div>
          </form>
        </ModalBody>
        <ModalFooter>
          <Button color="danger" onClick={this.props.closeModalHandler}>
            Tutup
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

export default viewBarangT;
