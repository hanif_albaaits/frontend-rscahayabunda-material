import React, { Fragment } from "react";
import { Alert } from "reactstrap";
import { getLokasi } from "../../../actions/lokasiActions";
import { connect } from "react-redux";
import EditLokasi from "./editLokasi";
import CreateLokasi from "./createLokasi";
import DeleteLokasi from "./deleteLokasi";
import ViewLokasi from "./viewLokasi";
import printTable from "../../utility/printTable";
import exportCsv from "../../utility/exportCsv";
import bacaAkses from "../../utility/bacaAkses";
import Spinner from "../../utility/spinnerTable";

import PropTypes from "prop-types";
import moment from "moment";

import IconButton from "@material-ui/core/IconButton";
import FirstPageIcon from "@material-ui/icons/FirstPage";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import LastPageIcon from "@material-ui/icons/LastPage";
import TablePagination from "@material-ui/core/TablePagination";
import { withStyles } from "@material-ui/core/styles";

class ListLokasi extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      formCari: {
        code: /(?:)/,
        name: /(?:)/,
        ruang: /(?:)/,
        lantai: /(?:)/
      },
      AKSES: {},
      createLokasi: false,
      deleteLokasi: false,
      editLokasi: false,
      viewLokasi: false,
      allLokasi: [],
      currentLokasi: {},
      alertData: {
        status: 0,
        message: "",
        code: ""
      },
      hasil: [],
      page: 0,
      rowsPerPage: 5
    };
    this.showCreate = this.showCreate.bind(this);
  }
  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  deleteModalHandler = lokasiid => {
    let tmp = {};
    this.state.allLokasi.map(ele => {
      if (lokasiid === ele._id) {
        tmp = ele;
      }
      return tmp;
    });
    this.setState({
      currentLokasi: tmp,
      deleteLokasi: true
    });
  };

  viewModalHandler = lokasiid => {
    let tmp = {};
    this.state.allLokasi.map(ele => {
      if (lokasiid === ele._id) {
        tmp = ele;
      }
      return tmp;
    });
    this.setState({
      currentLokasi: tmp,
      viewLokasi: true
    });
  };

  editModalHandler = lokasiid => {
    let tmp = {};
    this.state.allLokasi.map(ele => {
      if (lokasiid === ele._id) {
        tmp = {
          _id: ele._id,
          code: ele.code,
          name: ele.name,
          ruang: ele.ruang,
          lantai: ele.lantai,
          description: ele.description,
          update_date: ele.update_date,
          update_by: ele.update_by
        };
        this.setState({
          currentLokasi: tmp,
          editLokasi: true
        });
      }
      return tmp;
    });
  };

  changeHandler = event => {
    let data = event.target.value;
    let char = data.replace(/\\/g, "");
    let tmp = this.state.formCari;
    if (event.target.name) {
      tmp[event.target.name] = new RegExp(char.toLowerCase());
    } else {
      tmp[event.target.name] = char;
    }
    this.cariData();
  };

  cariData = () => {
    const { code, name, lantai, ruang } = this.state.formCari;
    let temp = [];
    this.state.allLokasi.map(ele => {
      if (
        code.test(ele.code.toLowerCase()) &&
        name.test(ele.name.toLowerCase()) &&
        ruang.test(ele.ruang.toLowerCase()) &&
        lantai.test(ele.lantai.toLowerCase())
      ) {
        temp.push(ele);
      }
      return temp;
    });
    this.setState({
      hasil: temp
    });
  };

  closeModalHandler = () => {
    this.setState({
      createLokasi: false,
      viewLokasi: false,
      editLokasi: false,
      deleteLokasi: false
    });
  };

  showCreate = () => {
    this.setState({ createLokasi: true });
  };

  componentDidMount() {
    this.props.getLokasi();
  }

  componentWillReceiveProps(newProps) {
    let a = bacaAkses(newProps.ambilAccess.side, window.location.pathname);
    let allLokasi = newProps.ambil.lokasi;
    if (a !== null) {
      this.setState({
        AKSES: a
      });
    }
    if (allLokasi !== null) {
      this.setState({
        allLokasi: allLokasi,
        hasil: allLokasi,
        loading: false
      });
    }
  }

  modalStatus = (status, message, code) => {
    this.setState({
      alertData: {
        status: status,
        message: message,
        code: code
      },
      viewLokasi: false,
      editLokasi: false,
      deleteLokasi: false,
      createLokasi: false
    });
    setTimeout(() => {
      window.location.href = "/lokasi";
    }, 3000);
  };
  changeDateFormat = tanggal => {
    return moment(tanggal, "YYYY-MM-DD hh:mm:ss a").format("DD/MM/YYYY");
  };

  render() {
    const AKSES = this.state.AKSES;
    const loading = this.state.loading;
    let tablelist;

    if (loading === true) {
      tablelist = <Spinner />;
    } else {
      if (this.state.allLokasi.length > 0) {
        tablelist = this.state.hasil
          .slice(
            this.state.page * this.state.rowsPerPage,
            this.state.page * this.state.rowsPerPage + this.state.rowsPerPage
          )
          .map((row, index) => {
            return (
              <tr key={index}>
                <td className="align-middle">{row.code}</td>
                <td className="align-middle">{row.name}</td>
                <td className="align-middle">{row.ruang}</td>
                <td className="align-middle">{row.lantai}</td>
                <td className="align-middle">
                  {this.changeDateFormat(row.created_date)}
                </td>
                <td className="align-middle">{row.created_by}</td>
                <td className="align-middle text-center">
                  {AKSES.atribut === 1 || AKSES.atribut === 2 ? (
                    <button
                      type="button"
                      className="btn btn-icon btn-flat btn-default"
                      title="view"
                      onClick={() => {
                        this.viewModalHandler(row._id);
                      }}
                    >
                      <i className="icon md-search" aria-hidden="false" />
                    </button>
                  ) : (
                    ""
                  )}
                  {AKSES.atribut === 2 ? (
                    <Fragment>
                      <button
                        type="button"
                        className="btn btn-icon btn-flat btn-default"
                        title="Edit"
                        onClick={() => {
                          this.editModalHandler(row._id);
                        }}
                      >
                        <i className="icon md-edit" aria-hidden="false" />
                      </button>
                      <button
                        type="button"
                        className="btn btn-sm btn-icon btn-flat btn-default"
                        title="Delete"
                        onClick={() => {
                          this.deleteModalHandler(row._id);
                        }}
                      >
                        <i className="icon md-delete" aria-hidden="false" />
                      </button>
                    </Fragment>
                  ) : (
                    ""
                  )}
                </td>
              </tr>
            );
          });
      } else {
        tablelist = (
          <tr className="text-center">
            <td colSpan="10">Tidak Ada data yang tersedia !</td>
          </tr>
        );
      }
    }

    return (
      <div>
        <div className="page-header">
          <h1 className="page-title">Master Lokasi</h1>
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <a href="/dashboard">Home</a>
            </li>
            <li className="breadcrumb-item">
              <a href="/dashboard">Master</a>
            </li>
            <li className="breadcrumb-item active">Master Lokasi</li>
          </ol>
          <div className="page-header-actions">
            {AKSES.atribut === 2 ? (
              <div>
                <button
                  type="button"
                  className="btn btn-primary"
                  onClick={this.showCreate}
                >
                  Tambah Lokasi
                </button>
              </div>
            ) : (
              ""
            )}
          </div>
        </div>
        <div className="page-content">
          {this.state.alertData.status === 1 ? (
            <Alert color="success">
              <b>Data {this.state.alertData.message}</b> Data Lokasi dengan
              referensi <strong>{this.state.alertData.code} </strong>
              telah {this.state.alertData.message}
            </Alert>
          ) : (
            ""
          )}
          {this.state.alertData.status === 2 ? (
            <Alert color="danger">{this.state.alertData.message} </Alert>
          ) : (
            ""
          )}
          <div className="panel">
            <header className="panel-heading">
              <h3 className="panel-title">List Lokasi</h3>
            </header>
            <div className="panel-body">
              <form className="form-inline">
                <div className="form-group">
                  <input
                    type="text"
                    placeholder="cari nama lokasi"
                    className="form-control mx-sm-1"
                    name="name"
                    onChange={this.changeHandler}
                    autoComplete="off"
                  />
                  <input
                    type="text"
                    placeholder="cari ruang"
                    className="form-control mx-sm-1"
                    name="ruang"
                    autoComplete="off"
                    onChange={this.changeHandler}
                  />
                  <input
                    type="text"
                    placeholder="cari lantai"
                    className="form-control mx-sm-1"
                    name="lantai"
                    autoComplete="off"
                    onChange={this.changeHandler}
                  />
                </div>
              </form>
              <div className="example table-responsive">
                <table id="table" className="table table-bordered">
                  <thead>
                    <tr>
                      <th>ID Lokasi</th>
                      <th>Nama Lokasi</th>
                      <th>Ruang</th>
                      <th>Lantai</th>
                      <th>Created Date</th>
                      <th>Created By</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>{tablelist}</tbody>
                  <TablePagination
                    colSpan={7}
                    count={this.state.hasil.length}
                    rowsPerPage={this.state.rowsPerPage}
                    page={this.state.page}
                    onChangePage={this.handleChangePage}
                    onChangeRowsPerPage={this.handleChangeRowsPerPage}
                    ActionsComponent={TablePaginationActionsWrapped}
                  />
                </table>
                <button
                  type="button"
                  className="btn btn-primary mx-sm-1"
                  onClick={() => {
                    printTable("table");
                  }}
                  hidden={true}
                >
                  Print
                </button>
                <button
                  type="button"
                  className="btn btn-primary mx-sm-1"
                  onClick={() => {
                    exportCsv("m_lokasi");
                  }}
                >
                  Export to CSV
                </button>
                <ViewLokasi
                  view={this.state.viewLokasi}
                  closeModalHandler={this.closeModalHandler}
                  lokasi={this.state.currentLokasi}
                />
                <DeleteLokasi
                  delete={this.state.deleteLokasi}
                  lokasi={this.state.currentLokasi}
                  closeModalHandler={this.closeModalHandler}
                  modalStatus={this.modalStatus}
                />
                <CreateLokasi
                  create={this.state.createLokasi}
                  closeModalHandler={this.closeModalHandler}
                  modalStatus={this.modalStatus}
                  allLokasi={this.state.allLokasi}
                />
                <EditLokasi
                  edit={this.state.editLokasi}
                  closeModalHandler={this.closeModalHandler}
                  currentLokasi={this.state.currentLokasi}
                  name={this.state.currentLokasi.name}
                  modalStatus={this.modalStatus}
                  allLokasi={this.state.allLokasi}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ListLokasi.propTypes = {
  getLokasi: PropTypes.func.isRequired,
  ambil: PropTypes.object.isRequired,
  ambilAccess: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  ambil: state.lokasiReducers,
  ambilAccess: state.menuReducers
});

export default connect(
  mapStateToProps,
  { getLokasi }
)(ListLokasi);

const actionsStyles = theme => ({
  root: {
    flexShrink: 0,
    color: theme.palette.text.secondary,
    marginLeft: theme.spacing.unit * 2.5
  }
});

class TablePaginationActions extends React.Component {
  handleFirstPageButtonClick = event => {
    this.props.onChangePage(event, 0);
  };

  handleBackButtonClick = event => {
    this.props.onChangePage(event, this.props.page - 1);
  };

  handleNextButtonClick = event => {
    this.props.onChangePage(event, this.props.page + 1);
  };

  handleLastPageButtonClick = event => {
    this.props.onChangePage(
      event,
      Math.max(0, Math.ceil(this.props.count / this.props.rowsPerPage) - 1)
    );
  };

  render() {
    const { classes, count, page, rowsPerPage, theme } = this.props;

    return (
      <div className={classes.root}>
        <IconButton
          onClick={this.handleFirstPageButtonClick}
          disabled={page === 0}
          aria-label="First Page"
        >
          {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
        </IconButton>
        <IconButton
          onClick={this.handleBackButtonClick}
          disabled={page === 0}
          aria-label="Previous Page"
        >
          {theme.direction === "rtl" ? (
            <KeyboardArrowRight />
          ) : (
            <KeyboardArrowLeft />
          )}
        </IconButton>
        <IconButton
          onClick={this.handleNextButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="Next Page"
        >
          {theme.direction === "rtl" ? (
            <KeyboardArrowLeft />
          ) : (
            <KeyboardArrowRight />
          )}
        </IconButton>
        <IconButton
          onClick={this.handleLastPageButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="Last Page"
        >
          {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
        </IconButton>
      </div>
    );
  }
}

TablePaginationActions.propTypes = {
  classes: PropTypes.object.isRequired,
  count: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
  theme: PropTypes.object.isRequired
};

const TablePaginationActionsWrapped = withStyles(actionsStyles, {
  withTheme: true
})(TablePaginationActions);
