import React from "react";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  Alert
} from "reactstrap";
import PropTypes from "prop-types";
import apiconfig from "../../../config/api.config.json";

import Konfirm from "../../utility/confirm";
import { connect } from "react-redux";
import { lokasiCreate } from "../../../actions/lokasiActions";

class CreateLokasi extends React.Component {
  constructor(props) {
    super(props);
    let userdata = JSON.parse(localStorage.getItem(apiconfig.LS.USERDATA));
    this.state = {
      formdata: {
        name: "",
        ruang: "",
        lantai: "",
        description: "",
        created_by: userdata.username
      },
      konfirmasi: false,
      status: "",
      alertData: {
        status: false,
        message: ""
      }
    };
    this.changeHandler = this.changeHandler.bind(this);
  }

  changeHandler(e) {
    let tmp = this.state.formdata;
    tmp[e.target.name] = e.target.value;
    this.setState({
      alertData: {
        status: false,
        message: ""
      },
      formdata: tmp
    });
  }

  validateFilter(lokasiname) {
    let allLokasi = this.props.allLokasi.map(ele => ele.name);
    let a = allLokasi.filter(e => e === lokasiname);
    if (a.length === 0) {
      return false;
    } else {
      return true;
    }
  }

  submitValidasi = () => {
    if (
      this.state.formdata.name === "" ||
      this.state.formdata.ruang === "" ||
      this.state.formdata.lantai === "" ||
      this.state.formdata.description === ""
    ) {
      this.setState({
        alertData: {
          status: true,
          message: "Semua field harus di isi!"
        }
      });
    } else if (this.validateFilter(this.state.formdata.name) === true) {
      this.setState({
        alertData: {
          status: true,
          message: "Ouh, nama lokasi kamu telah digunakan !"
        }
      });
    } else {
      this.setState({ konfirmasi: true });
    }
  };

  submitHandler = () => {
    this.props.lokasiCreate(this.state.formdata);
    this.props.closeModalHandler();
    this.KonfirmasiTidak();
    // this.setState({
    //   formdata: {}
    // });
  };

  componentWillReceiveProps(newStatus) {
    this.setState({
      status: newStatus.ambil.statusADD
    });
  }

  KonfirmasiTidak = () => {
    this.setState({ konfirmasi: false });
  };

  render() {
    this.props.ambil.statusADD === 200
      ? this.props.modalStatus(1, "Di Tambahkan", this.state.formdata.name)
      : console.log(this.state.status);
    return (
      <Modal isOpen={this.props.create} className={this.props.className}>
        <ModalHeader>Tambah Lokasi Baru</ModalHeader>
        <ModalBody>
          <Konfirm
            showKonfirmasi={this.state.konfirmasi}
            ubahKonfirmasi={this.KonfirmasiTidak}
            submitHandler={this.submitHandler}
          />
          <Form>
            <FormGroup>
              <Label for="lokasiname">Nama Lokasi</Label>
              <Input
                type="text"
                name="name"
                id="name"
                placeholder="masukan nama lokasi"
                value={this.state.formdata.name}
                onChange={this.changeHandler}
              />
            </FormGroup>
            <FormGroup>
              <Label for="lokasiname">Kode Ruang</Label>
              <Input
                type="text"
                name="ruang"
                id="ruang"
                placeholder="masukan kode ruang"
                value={this.state.formdata.ruang}
                onChange={this.changeHandler}
              />
            </FormGroup>
            <FormGroup>
              <Label for="lokasilantai">Nama Lantai</Label>
              <Input
                type="text"
                name="lantai"
                id="lantai"
                placeholder="masukan nama lantai"
                value={this.state.formdata.lantai}
                onChange={this.changeHandler}
              />
            </FormGroup>
            <FormGroup>
              <Label for="description">Deskripsi Lokasi</Label>
              <Input
                type="text"
                name="description"
                id="description"
                placeholder="masukan deskripsi lokasi"
                value={this.state.formdata.description}
                onChange={this.changeHandler}
              />
            </FormGroup>
          </Form>
        </ModalBody>
        <ModalFooter>
          {this.state.alertData.status === true ? (
            <Alert color="danger">{this.state.alertData.message} </Alert>
          ) : (
            ""
          )}
          <Button
            variant="contained"
            color="primary"
            onClick={this.submitValidasi}
          >
            Tambahkan
          </Button>
          <Button variant="contained" onClick={this.props.closeModalHandler}>
            Batal
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

CreateLokasi.propTypes = {
  lokasiCreate: PropTypes.func.isRequired,
  ambil: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  ambil: state.lokasiReducers
});

export default connect(
  mapStateToProps,
  { lokasiCreate }
)(CreateLokasi);
