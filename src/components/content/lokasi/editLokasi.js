import React from "react";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  Alert
} from "reactstrap";
import PropTypes from "prop-types";
import apiconfig from "../../../config/api.config.json";

import Konfirm from "../../utility/confirm";
import { connect } from "react-redux";
import { lokasiUpdate } from "../../../actions/lokasiActions";

class EditLokasi extends React.Component {
  constructor(props) {
    super(props);
    let userdata = JSON.parse(localStorage.getItem(apiconfig.LS.USERDATA));

    this.state = {
      formdata: {
        name: "",
        ruang: "",
        lantai: "",
        description: ""
      },
      konfirmasi: false,
      currentLokasiname: "",
      updated_by: userdata.username,
      status: "",
      alertData: {
        status: false,
        message: ""
      },
      labelWidth: 0
    };
    this.changeHandler = this.changeHandler.bind(this);
  }

  changeHandler(e) {
    let tmp = this.state.formdata;
    tmp[e.target.name] = e.target.value;
    this.setState({
      alertData: {
        status: false,
        message: ""
      },
      formdata: tmp
    });
  }

  validateFilter(lokasiname) {
    let lama = this.props.name;
    let allLokasi = this.props.allLokasi.map(ele => ele.name);
    let a = allLokasi.filter(e => e === lokasiname);
    if (a.length === 0 || lokasiname === lama) {
      return false;
    } else {
      return true;
    }
  }

  submitValidasi = () => {
    if (
      this.state.formdata.name === "" ||
      this.state.formdata.ruang === "" ||
      this.state.formdata.lantai === "" ||
      this.state.formdata.description === ""
    ) {
      this.setState({
        alertData: {
          status: true,
          message: "Semua field harus di isi!"
        }
      });
    } else if (this.validateFilter(this.state.formdata.name) === true) {
      this.setState({
        alertData: {
          status: true,
          message: "Ouh, nama lokasi kamu telah digunakan !"
        }
      });
    } else {
      this.setState({ konfirmasi: true });
    }
  };

  submitHandler = () => {
    let form = this.state.formdata;
    form["updated_by"] = this.state.updated_by;
    this.props.lokasiUpdate(this.state.formdata);
    this.props.closeModalHandler();
    this.KonfirmasiTidak();
  };

  KonfirmasiTidak = () => {
    this.setState({ konfirmasi: false });
  };

  componentWillReceiveProps(newProps) {
    this.setState({
      formdata: newProps.currentLokasi,
      status: newProps.ambil.statusPUT
    });
  }

  render() {
    this.state.status === 200
      ? this.props.modalStatus(1, "Di Ubah!", this.state.formdata.code)
      : console.log(this.state.status);
    return (
      <Modal isOpen={this.props.edit} className={this.props.className}>
        <ModalHeader>Ubah Data Lokasi</ModalHeader>
        <ModalBody>
          <Konfirm
            showKonfirmasi={this.state.konfirmasi}
            ubahKonfirmasi={this.KonfirmasiTidak}
            submitHandler={this.submitHandler}
          />
          <Form>
            <FormGroup>
              <Label for="lokasiname">Nama Lokasi</Label>
              <Input
                type="text"
                name="name"
                id="name"
                placeholder="masukan lokasiname"
                value={this.state.formdata.name}
                onChange={this.changeHandler}
              />
            </FormGroup>
            <FormGroup>
              <Label for="lokasiruang">Kode Ruang</Label>
              <Input
                type="text"
                name="ruang"
                id="ruang"
                placeholder="masukan kode ruang"
                value={this.state.formdata.ruang}
                onChange={this.changeHandler}
              />
            </FormGroup>
            <FormGroup>
              <Label for="lokasilantai">Lantai</Label>
              <Input
                type="text"
                name="lantai"
                id="lantai"
                placeholder="masukan lantai"
                value={this.state.formdata.lantai}
                onChange={this.changeHandler}
              />
            </FormGroup>
            <FormGroup>
              <Label for="description">Deskripsi Lokasi</Label>
              <Input
                type="text"
                name="description"
                id="description"
                placeholder="Deskripsi Lokasi"
                value={this.state.formdata.description}
                onChange={this.changeHandler}
              />
            </FormGroup>
          </Form>
        </ModalBody>
        <ModalFooter>
          {this.state.alertData.status === true ? (
            <Alert color="danger">{this.state.alertData.message} </Alert>
          ) : (
            ""
          )}
          <Button
            variant="contained"
            color="primary"
            onClick={this.submitValidasi}
          >
            Ubah
          </Button>
          <Button variant="contained" onClick={this.props.closeModalHandler}>
            Batal
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

EditLokasi.propTypes = {
  lokasiUpdate: PropTypes.func.isRequired,
  ambil: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  ambil: state.lokasiReducers
});

export default connect(
  mapStateToProps,
  { lokasiUpdate }
)(EditLokasi);
