import React from "react";
import { Modal, ModalBody, ModalFooter, ModalHeader, Button } from "reactstrap";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";

const styles = theme => ({
  root: {
    width: "100%",
    flexGrow: 1
  },
  table: {
    minWidth: 700
  }
});

class ViewLokasi extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <Modal isOpen={this.props.view} className={this.props.className}>
        <ModalHeader> Lihat Lokasi</ModalHeader>
        <ModalBody>
          <div>
            <h3>{this.props.lokasi.name} </h3>
            <h5>{this.props.lokasi.code} </h5>
          </div>
          <div className={classes.root}>
            <Grid container spacing={24}>
              <Grid item xs={6}>
                Lokasi ID
                <br />
                Nama Lokasi
                <br />
                Kode Ruang
                <br />
                Lantai
                <br />
                Created By
                <br />
                Created Date
                <br />
                Updated By
                <br />
                Updated Date
                <br />
                Description
              </Grid>
              <Grid item xs={6}>
                {this.props.lokasi.code}
                <br />
                {this.props.lokasi.name}
                <br />
                {this.props.lokasi.ruang}
                <br />
                {this.props.lokasi.lantai}
                <br />
                {this.props.lokasi.created_by}
                <br />
                {this.props.lokasi.created_date}
                <br />
                {this.props.lokasi.updated_by}
                <br />
                {this.props.lokasi.updated_date}
                <br />
                {this.props.lokasi.description}
              </Grid>
            </Grid>
          </div>
        </ModalBody>
        <ModalFooter>
          <Button color="danger" onClick={this.props.closeModalHandler}>
            Tutup
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

ViewLokasi.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ViewLokasi);
