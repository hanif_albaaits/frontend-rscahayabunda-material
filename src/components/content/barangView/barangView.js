import React, { Fragment } from "react";
import Barcode from "react-barcode";

import { Alert } from "reactstrap";
import { getBarang1 } from "../../../actions/barangActions";
import { getAmbilBrg } from "../../../actions/barangAmbilActions";
import { getPinjamBrg } from "../../../actions/barangPinjamActions";
import { getServisBrg } from "../../../actions/barangServisActions";
import { getPegawai } from "../../../actions/pegawaiActions";
import { getLokasi } from "../../../actions/lokasiActions";
import { connect } from "react-redux";

import CreateBarangA from "./createBarangA";
import CreateBarangP from "./createBarangP";
import CreateBarangS from "./createBarangS";
import CreateBarangT from "./createBarangT";
import printTable from "../../utility/printTable";
// import exportCsv from "../../utility/exportCsv";
import bacaAkses from "../../utility/bacaAkses";
import Spinner from "../../utility/spinner";
import PropTypes from "prop-types";
import moment from "moment";

import IconButton from "@material-ui/core/IconButton";
import FirstPageIcon from "@material-ui/icons/FirstPage";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import LastPageIcon from "@material-ui/icons/LastPage";
import TablePagination from "@material-ui/core/TablePagination";
import { withStyles } from "@material-ui/core/styles";

class BarangView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      formCariAmbil: {
        kd_ambil: /(?:)/,
        kd_pengambil: /(?:)/,
        nama: /(?:)/
      },
      AKSES: {},
      createBarangA: false,
      createBarangP: false,
      createBarangS: false,
      createBarangT: false,
      barang: {},
      barangAmbil: [],
      hasilAmbil: [],
      barangPinjam: [],
      hasilPinjam: [],
      barangServis: [],
      hasilServis: [],
      alertData: {
        status: 0,
        message: "",
        code: ""
      },
      page: 0,
      rowsPerPage: 5
    };
  }
  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  changeHandler = event => {
    let data = event.target.value;
    let char = data.replace(/\\/g, "");
    let tmp = this.state.formCari;
    if (event.target.name) {
      tmp[event.target.name] = new RegExp(char.toLowerCase());
    } else {
      tmp[event.target.name] = char;
    }
    this.cariData();
  };

  cariData = () => {
    const { kd_ambil, kd_pengambil, nama } = this.state.formCariAmbil;
    let temp = [];
    this.state.barangAmbil.map(ele => {
      if (
        kd_ambil.test(ele.kd_ambil.toLowerCase()) &&
        kd_pengambil.test(ele.kd_pengambil.toLowerCase()) &&
        nama.test(ele.nama_pegawai.toLowerCase())
      ) {
        temp.push(ele);
      }
      return temp;
    });
    this.setState({
      hasilAmbil: temp
    });
  };

  closeModalHandler = () => {
    this.setState({
      createBarangA: false,
      createBarangP: false,
      createBarangS: false,
      createBarangT: false
    });
  };

  showCreateA = () => {
    this.setState({ createBarangA: true });
  };
  showCreateP = () => {
    this.setState({ createBarangP: true });
  };
  showCreateS = () => {
    this.setState({ createBarangS: true });
  };
  showCreateT = () => {
    this.setState({ createBarangT: true });
  };

  getUrlParam = (parameter, defaultvalue) => {
    var urlparameter = defaultvalue;
    if (window.location.href.indexOf(parameter) > -1) {
      urlparameter = this.getUrlVars()[parameter];
    }
    return urlparameter;
  };

  getUrlVars = () => {
    var vars = {};
    window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(
      m,
      key,
      value
    ) {
      vars[key] = value;
    });
    return vars;
  };

  componentDidMount() {
    const param = this.getUrlParam("code", "Empty");
    this.props.getBarang1(param);
    this.props.getAmbilBrg(param);
    this.props.getPinjamBrg(param);
    this.props.getServisBrg(param);
    this.props.getPegawai();
    this.props.getLokasi();
  }

  componentWillReceiveProps(newProps) {
    let a = bacaAkses(newProps.ambilAccess.side, "/barang");
    let barang = newProps.ambil.barang1;
    let barangAmbil = newProps.ambilBarang.barang;
    let barangPinjam = newProps.pinjamBarang.barang;
    let barangServis = newProps.servisBarang.barang;
    if (a !== null) {
      this.setState({
        AKSES: a
      });
    }
    if (
      barang !== null &&
      barangAmbil !== null &&
      barangPinjam !== null &&
      barangServis !== null
    ) {
      this.setState({
        barang: barang,
        barangAmbil: barangAmbil,
        hasilAmbil: barangAmbil,
        barangPinjam: barangPinjam,
        hasilPinjam: barangPinjam,
        barangServis: barangServis,
        hasilServis: barangServis,
        loading: false
      });
    }
  }

  modalStatus = (status, message, code) => {
    this.setState({
      alertData: {
        status: status,
        message: message,
        code: code
      },
      createBarangA: false,
      createBarangP: false,
      createBarangS: false,
      createBarangT: false
    });
    setTimeout(() => {
      window.location.href = "/barang";
    }, 3000);
  };

  changeDateFormat = tanggal => {
    return moment(tanggal).format("DD/MM/YYYY");
  };

  printCuk = () => {
    alert("hai");
    window.print();
  };

  render() {
    const barang = this.state.barang;
    const AKSES = this.state.AKSES;
    const loading = this.state.loading;
    let databarang;

    if (loading === true) {
      databarang = <Spinner />;
    } else {
      if (barang !== null) {
        databarang = (
          <div>
            <div className="form-row">
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="kd_barang"
                  className="col-4 col-form-label text-right"
                >
                  Kode Barang
                </label>
                <div className="col-8">
                  <input
                    type="text"
                    className="form-control"
                    id="kd_barang"
                    value={this.state.barang.kd_barang}
                    disabled
                  />
                </div>
              </div>
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="requestby"
                  className="col-4 col-form-label text-right"
                >
                  Nama Barang
                </label>
                <div className="col-8">
                  <input
                    type="text"
                    className="form-control"
                    id="nama"
                    value={this.state.barang.nama}
                    disabled
                  />
                </div>
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="eventcode"
                  className="col-4 col-form-label text-right"
                >
                  Kode Barang RS
                </label>
                <div className="col-8">
                  <input
                    type="text"
                    className="form-control"
                    id="kd_barang_rs"
                    value={this.state.barang.kd_barang_rs}
                    disabled
                  />
                </div>
              </div>
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="merk"
                  className="col-4 col-form-label text-right"
                >
                  Merk Barang
                </label>
                <div className="col-8">
                  <input
                    type="text"
                    className="form-control"
                    id="merk"
                    value={this.state.barang.merk}
                    disabled
                  />
                </div>
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="lokasi"
                  className="col-4 col-form-label text-right"
                >
                  Lokasi Barang
                </label>
                <div className="col-8">
                  <input
                    type="text"
                    className="form-control"
                    id="lokasi"
                    value={this.state.barang.lokasi}
                    disabled
                  />
                </div>
              </div>
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="ukuran"
                  className="col-4 col-form-label text-right"
                >
                  Ukuran Barang
                </label>
                <div className="col-8">
                  <input
                    type="text"
                    className="form-control"
                    id="ukuran"
                    value={this.state.barang.ukuran}
                    disabled
                  />
                </div>
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="lantai"
                  className="col-4 col-form-label text-right"
                >
                  Lantai
                </label>
                <div className="col-8">
                  <input
                    type="text"
                    className="form-control"
                    id="lantai"
                    value={this.state.barang.lantai}
                    disabled
                  />
                </div>
              </div>
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="bahan"
                  className="col-4 col-form-label text-right"
                >
                  Bahan
                </label>
                <div className="col-8">
                  <input
                    className="form-control"
                    aria-label="With textarea"
                    value={this.state.barang.bahan}
                    disabled
                  />
                </div>
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="ruang"
                  className="col-4 col-form-label text-right"
                >
                  Ruang
                </label>
                <div className="col-8">
                  <input
                    type="text"
                    className="form-control"
                    id="ruang"
                    value={this.state.barang.ruang}
                    disabled
                  />
                </div>
              </div>
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="tahun"
                  className="col-4 col-form-label text-right"
                >
                  Tahun Perolehan
                </label>
                <div className="col-8">
                  <input
                    className="form-control"
                    aria-label="With textarea"
                    value={this.state.barang.tahun}
                    disabled
                  />
                </div>
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="tanggal_masuk"
                  className="col-4 col-form-label text-right"
                >
                  Tanggal Masuk
                </label>
                <div className="col-8">
                  <input
                    type="text"
                    className="form-control"
                    id="tanggal_masuk"
                    value={this.state.barang.tanggal_masuk}
                    disabled
                  />
                </div>
              </div>
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="no_seri"
                  className="col-4 col-form-label text-right"
                >
                  No Seri
                </label>
                <div className="col-8">
                  <input
                    className="form-control"
                    aria-label="With textarea"
                    value={this.state.barang.no_seri}
                    disabled
                  />
                </div>
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="total"
                  className="col-4 col-form-label text-right"
                >
                  Jumlah Barang
                </label>
                <div className="col-8">
                  <input
                    type="text"
                    className="form-control"
                    id="total"
                    value={this.state.barang.total}
                    disabled
                  />
                </div>
              </div>
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="harga_beli"
                  className="col-4 col-form-label text-right"
                >
                  Harga Beli
                </label>
                <div className="col-8">
                  <input
                    className="form-control"
                    aria-label="With textarea"
                    value={this.state.barang.harga_beli}
                    disabled
                  />
                </div>
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="jumlah"
                  className="col-4 col-form-label text-right"
                >
                  Jumlah di Gudang
                </label>
                <div className="col-8">
                  <input
                    type="text"
                    className="form-control"
                    id="jumlah"
                    value={this.state.barang.jumlah}
                    disabled
                  />
                </div>
              </div>
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="kondisi"
                  className="col-4 col-form-label text-right"
                >
                  Kondisi Barang
                </label>
                <div className="col-8">
                  <input
                    className="form-control"
                    aria-label="With textarea"
                    value={this.state.barang.kondisi}
                    disabled
                  />
                </div>
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="kondisi"
                  className="col-4 col-form-label text-right"
                >
                  Catatan
                </label>
                <div className="col-8">
                  <textarea
                    className="form-control"
                    aria-label="With textarea"
                    value={this.state.barang.catatan}
                    disabled
                  />
                </div>
              </div>
              <div className="form-group col-md-6 row">
                <label
                  htmlFor="kondisi"
                  className="col-4 col-form-label text-right"
                >
                  Barcode
                </label>
                <div className="col-8">
                  <Barcode value={this.state.barang.kd_barang} />
                </div>
              </div>
            </div>
            <br />
            <div className="form-group row">
              <div className="col d-flex justify-content-end">
                {AKSES.atribut === 2 ? (
                  <Fragment>
                    <button
                      type="button"
                      className="btn btn-primary  float-right ml-1"
                      onClick={this.showCreateT}
                    >
                      Tambah Stock
                    </button>
                    <button
                      type="button"
                      className="btn btn-primary  float-right ml-1"
                      onClick={this.showCreateA}
                    >
                      Ambil
                    </button>
                    <button
                      type="button"
                      className="btn btn-primary  float-right ml-1"
                      onClick={this.showCreateP}
                    >
                      Pinjam
                    </button>
                    <button
                      type="button"
                      className="btn btn-primary  float-right ml-1"
                      onClick={this.showCreateS}
                    >
                      Servis
                    </button>
                  </Fragment>
                ) : (
                  ""
                )}
              </div>
            </div>
          </div>
        );
      } else {
        databarang = (
          <tr className="text-center">
            <td colSpan="10">Tidak Ada data yang tersedia !</td>
          </tr>
        );
      }
    }

    return (
      <div>
        <div className="page-header">
          <h1 className="page-title">Lihat Barang</h1>
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <a href="/dashboard">Home</a>
            </li>
            <li className="breadcrumb-item">
              <a href="/barang">Manajemen Barang</a>
            </li>
            <li className="breadcrumb-item active">Lihat Barang</li>
          </ol>
          <div className="page-header-actions">
            <button
              type="button"
              className="btn btn-primary mx-sm-1"
              onClick={() => {
                printTable("card");
              }}
            >
              Print
            </button>
          </div>
        </div>
        <div className="page-content">
          {this.state.alertData.status === 1 ? (
            <Alert color="success">
              <b>Data {this.state.alertData.message}</b> Data Barang dengan
              referensi <strong>{this.state.alertData.code} </strong>
              telah {this.state.alertData.message}
            </Alert>
          ) : (
            ""
          )}
          {this.state.alertData.status === 2 ? (
            <Alert color="danger">{this.state.alertData.message} </Alert>
          ) : (
            ""
          )}
          <div className="panel">
            <header className="panel-heading">
              <h3 className="panel-title">Data Barang</h3>
            </header>
            <div className="panel-body">
              <div className="card mb-3">
                <hr />
                <div id="card" className="card-body">
                  {databarang}
                </div>
              </div>

              {this.state.barangAmbil.length !== 0 ? (
                <div className="card mb-3">
                  <div className="card-header">List Barang Ambil</div>
                  <div className="card-body">
                    <form className="form-inline">
                      <div className="form-group">
                        {/* <input
                          type="text"
                          placeholder="Kode pengambilan"
                          className="form-control mx-sm-1"
                          name="kd_ambil"
                          onChange={this.changeHandler}
                          autoComplete="off"
                        />
                        <input
                          type="text"
                          placeholder="ID Pegawai"
                          className="form-control mx-sm-1"
                          name="kd_pengambil"
                          onChange={this.changeHandler}
                          autoComplete="off"
                        />
                        <input
                          type="text"
                          placeholder="Nama Pegawai"
                          className="form-control mx-sm-1"
                          name="nama"
                          onChange={this.changeHandler}
                          autoComplete="off"
                        /> */}
                      </div>
                    </form>
                    <div className="table-responsive">
                      <table id="table" className="table">
                        <thead>
                          <tr>
                            <th>Kode Pengambilan</th>
                            <th>Nama Pegawai</th>
                            <th>Lokasi</th>
                            <th>Jumlah Ambil</th>
                            <th>Tanggal Ambil</th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.state.hasilAmbil
                            .slice(
                              this.state.page * this.state.rowsPerPage,
                              this.state.page * this.state.rowsPerPage +
                                this.state.rowsPerPage
                            )
                            .map((row, index) => {
                              return (
                                <tr key={index}>
                                  <td>{row.kd_ambil}</td>
                                  <td>{row.nama_pegawai}</td>
                                  <td>{row.nama_lokasi}</td>
                                  <td>{row.jumlah_ambil}</td>
                                  <td>{row.tanggal_ambil}</td>
                                </tr>
                              );
                            })}
                        </tbody>
                        <TablePagination
                          colSpan={5}
                          count={this.state.hasilAmbil.length}
                          rowsPerPage={this.state.rowsPerPage}
                          page={this.state.page}
                          onChangePage={this.handleChangePage}
                          onChangeRowsPerPage={this.handleChangeRowsPerPage}
                          ActionsComponent={TablePaginationActionsWrapped}
                        />
                      </table>
                    </div>
                  </div>
                </div>
              ) : (
                ""
              )}

              {this.state.barangPinjam.length !== 0 ? (
                <div className="card mb-3">
                  <div className="card-header">List Peminjaman Barang</div>
                  <div className="card-body">
                    <div className="table-responsive">
                      <table id="table" className="table">
                        <thead>
                          <tr>
                            <th>Kode Peminjaman</th>
                            <th>Nama Pegawai</th>
                            <th>Lokasi</th>
                            <th>Jumlah Pinjam</th>
                            <th>Tanggal Pinjam</th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.state.hasilPinjam
                            .slice(
                              this.state.page * this.state.rowsPerPage,
                              this.state.page * this.state.rowsPerPage +
                                this.state.rowsPerPage
                            )
                            .map((row, index) => {
                              return (
                                <tr key={index}>
                                  <td>{row.kd_pinjam}</td>
                                  <td>{row.nama_pegawai}</td>
                                  <td>{row.nama_lokasi}</td>
                                  <td>{row.jumlah_pinjam}</td>
                                  <td>{row.tanggal_pinjam}</td>
                                </tr>
                              );
                            })}
                        </tbody>
                        <TablePagination
                          colSpan={5}
                          count={this.state.hasilPinjam.length}
                          rowsPerPage={this.state.rowsPerPage}
                          page={this.state.page}
                          onChangePage={this.handleChangePage}
                          onChangeRowsPerPage={this.handleChangeRowsPerPage}
                          ActionsComponent={TablePaginationActionsWrapped}
                        />
                      </table>
                    </div>
                  </div>
                </div>
              ) : (
                ""
              )}

              {this.state.barangServis.length !== 0 ? (
                <div className="card mb-3">
                  <div className="card-header">List Servis Barang</div>
                  <div className="card-body">
                    <div className="table-responsive">
                      <table id="table" className="table">
                        <thead>
                          <tr>
                            <th>Kode Servis</th>
                            <th>Nama Pegawai</th>
                            <th>Lokasi</th>
                            <th>Jumlah Servis</th>
                            <th>Tanggal Servis</th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.state.hasilServis
                            .slice(
                              this.state.page * this.state.rowsPerPage,
                              this.state.page * this.state.rowsPerPage +
                                this.state.rowsPerPage
                            )
                            .map((row, index) => {
                              return (
                                <tr key={index}>
                                  <td>{row.kd_servis}</td>
                                  <td>{row.nama_pegawai}</td>
                                  <td>{row.lokasi}</td>
                                  <td>{row.jumlah_servis}</td>
                                  <td>{row.tanggal_servis}</td>
                                </tr>
                              );
                            })}
                        </tbody>
                        <TablePagination
                          colSpan={5}
                          count={this.state.hasilServis.length}
                          rowsPerPage={this.state.rowsPerPage}
                          page={this.state.page}
                          onChangePage={this.handleChangePage}
                          onChangeRowsPerPage={this.handleChangeRowsPerPage}
                          ActionsComponent={TablePaginationActionsWrapped}
                        />
                      </table>
                    </div>
                  </div>
                </div>
              ) : (
                ""
              )}
              <CreateBarangA
                create={this.state.createBarangA}
                closeModalHandler={this.closeModalHandler}
                modalStatus={this.modalStatus}
                barang={this.state.barang}
              />
              <CreateBarangP
                create={this.state.createBarangP}
                closeModalHandler={this.closeModalHandler}
                modalStatus={this.modalStatus}
                barang={this.state.barang}
              />
              <CreateBarangS
                create={this.state.createBarangS}
                closeModalHandler={this.closeModalHandler}
                modalStatus={this.modalStatus}
                barang={this.state.barang}
              />
              <CreateBarangT
                create={this.state.createBarangT}
                closeModalHandler={this.closeModalHandler}
                modalStatus={this.modalStatus}
                barang={this.state.barang}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

BarangView.propTypes = {
  getBarang1: PropTypes.func.isRequired,
  getAmbilBrg: PropTypes.func.isRequired,
  getPinjamBrg: PropTypes.func.isRequired,
  getServisBrg: PropTypes.func.isRequired,
  getLokasi: PropTypes.func.isRequired,
  getPegawai: PropTypes.func.isRequired,
  ambil: PropTypes.object.isRequired,
  ambilAccess: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  ambil: state.barangReducers,
  ambilAccess: state.menuReducers,
  ambilBarang: state.ambilBReducers,
  pinjamBarang: state.pinjamBReducers,
  servisBarang: state.servisBReducers
});

export default connect(
  mapStateToProps,
  { getBarang1, getLokasi, getPegawai, getAmbilBrg, getPinjamBrg, getServisBrg }
)(BarangView);

const actionsStyles = theme => ({
  root: {
    flexShrink: 0,
    color: theme.palette.text.secondary,
    marginLeft: theme.spacing.unit * 2.5
  }
});

class TablePaginationActions extends React.Component {
  handleFirstPageButtonClick = event => {
    this.props.onChangePage(event, 0);
  };

  handleBackButtonClick = event => {
    this.props.onChangePage(event, this.props.page - 1);
  };

  handleNextButtonClick = event => {
    this.props.onChangePage(event, this.props.page + 1);
  };

  handleLastPageButtonClick = event => {
    this.props.onChangePage(
      event,
      Math.max(0, Math.ceil(this.props.count / this.props.rowsPerPage) - 1)
    );
  };

  render() {
    const { classes, count, page, rowsPerPage, theme } = this.props;

    return (
      <div className={classes.root}>
        <IconButton
          onClick={this.handleFirstPageButtonClick}
          disabled={page === 0}
          aria-label="First Page"
        >
          {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
        </IconButton>
        <IconButton
          onClick={this.handleBackButtonClick}
          disabled={page === 0}
          aria-label="Previous Page"
        >
          {theme.direction === "rtl" ? (
            <KeyboardArrowRight />
          ) : (
            <KeyboardArrowLeft />
          )}
        </IconButton>
        <IconButton
          onClick={this.handleNextButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="Next Page"
        >
          {theme.direction === "rtl" ? (
            <KeyboardArrowLeft />
          ) : (
            <KeyboardArrowRight />
          )}
        </IconButton>
        <IconButton
          onClick={this.handleLastPageButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="Last Page"
        >
          {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
        </IconButton>
      </div>
    );
  }
}

TablePaginationActions.propTypes = {
  classes: PropTypes.object.isRequired,
  count: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
  theme: PropTypes.object.isRequired
};

const TablePaginationActionsWrapped = withStyles(actionsStyles, {
  withTheme: true
})(TablePaginationActions);
