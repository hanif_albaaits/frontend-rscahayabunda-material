import React from "react";
import { Modal, ModalBody, ModalFooter, ModalHeader, Button } from "reactstrap";

class ViewBarangS extends React.Component {
  render() {
    return (
      <Modal isOpen={this.props.view} className="modal-lg">
        <ModalHeader>Data peminjaman {this.props.servis.kd_servis}</ModalHeader>
        <ModalBody>
          <form>
            <div className="card mb-3">
              <div className="card-header">Barang yang di servis </div>
              <div className="card-body">
                <div className="form-row">
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="nama_barang"
                      className="col-4 col-form-label text-right"
                    >
                      Nama Barang
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.servis.nama_barang}
                        disabled
                      />
                    </div>
                  </div>
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="merk"
                      className="col-4 col-form-label text-right"
                    >
                      Merk
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.servis.merk_barang}
                        disabled
                      />
                    </div>
                  </div>
                </div>
                <div className="form-row">
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="ukuran"
                      className="col-4 col-form-label text-right"
                    >
                      Ukuran
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.servis.ukuran_barang}
                        disabled
                      />
                    </div>
                  </div>
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="bahan"
                      className="col-4 col-form-label text-right"
                    >
                      Bahan
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.servis.bahan_barang}
                        disabled
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="card mb-3">
              <div className="card-header">Data Pegawai yang mengambil</div>
              <div className="card-body">
                <div className="form-row">
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="nama"
                      className="col-4 col-form-label text-right"
                    >
                      Nama Pegawai
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.servis.nama_pegawai}
                        disabled
                      />
                    </div>
                  </div>
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="email"
                      className="col-4 col-form-label text-right"
                    >
                      Email
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.servis.email_pegawai}
                        disabled
                      />
                    </div>
                  </div>
                </div>
                <div className="form-row">
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="no_hp"
                      className="col-4 col-form-label text-right"
                    >
                      No HP
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.servis.nohp_pegawai}
                        disabled
                      />
                    </div>
                  </div>
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="alamat"
                      className="col-4 col-form-label text-right"
                    >
                      Alamat
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.servis.alamat_pegawai}
                        disabled
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="card mb-3">
              <div className="card-header">Data servis barang</div>
              <div className="card-body">
                <div className="form-row">
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="kondisi"
                      className="col-4 col-form-label text-right"
                    >
                      Lokasi Servis Barang
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.servis.lokasi}
                        disabled
                      />
                    </div>
                  </div>
                </div>
                <div className="form-row">
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="kondisi"
                      className="col-4 col-form-label text-right"
                    >
                      kondisi Sebelum
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.servis.kondisi_sebelum}
                        disabled
                      />
                    </div>
                  </div>
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="Catatan"
                      className="col-4 col-form-label text-right"
                    >
                      Catatan Sebelum
                    </label>
                    <div className="col-8">
                      <textarea
                        type="text"
                        className="form-control"
                        value={this.props.servis.catatan_sebelum}
                        disabled
                      />
                    </div>
                  </div>
                </div>
                <div className="form-row">
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="jumlah_servis"
                      className="col-4 col-form-label text-right"
                    >
                      Jumlah yang di Servis :
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.servis.jumlah_servis}
                        disabled
                      />
                    </div>
                  </div>
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="tanggal_servis"
                      className="col-4 col-form-label text-right"
                    >
                      Tanggal Servis
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.servis.tanggal_servis}
                        disabled
                      />
                    </div>
                  </div>
                </div>
                <div className="form-row">
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="kondisi"
                      className="col-4 col-form-label text-right"
                    >
                      Kondisi Sesudah
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.servis.kondisi_sesudah}
                        disabled
                      />
                    </div>
                  </div>
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="Catatan"
                      className="col-4 col-form-label text-right"
                    >
                      Catatan Sesudah
                    </label>
                    <div className="col-8">
                      <textarea
                        type="text"
                        className="form-control"
                        value={this.props.servis.catatan_sesudah}
                        disabled
                      />
                    </div>
                  </div>
                </div>
                <div className="form-row">
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="jumlah_kembali"
                      className="col-4 col-form-label text-right"
                    >
                      Jumlah Kembali
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.servis.jumlah_kembali}
                        disabled
                      />
                    </div>
                  </div>
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="tanggal_kembali"
                      className="col-4 col-form-label text-right"
                    >
                      Tanggal Kembali
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.servis.tanggal_kembali}
                        disabled
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div>
              Dibuat Oleh : {this.props.servis.created_by + ",   "}
              Tanggal Dibuat :{this.props.servis.created_date}
            </div>
            <div>
              Diubah Oleh : {this.props.servis.updated_by + ",   "}
              Tanggal Diubah :{this.props.servis.updated_date}
            </div>
          </form>
        </ModalBody>
        <ModalFooter>
          <Button color="danger" onClick={this.props.closeModalHandler}>
            Tutup
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

export default ViewBarangS;
