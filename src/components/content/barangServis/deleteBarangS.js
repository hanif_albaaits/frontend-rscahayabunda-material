import React from "react";
import { Modal, ModalBody, ModalFooter, ModalHeader, Button } from "reactstrap";
import { servisDelete } from "../../../actions/barangServisActions";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import apiconfig from "../../../config/api.config.json";

class DeleteBarangS extends React.Component {
  constructor(props) {
    super(props);
    let userdata = JSON.parse(localStorage.getItem(apiconfig.LS.USERDATA));
    this.state = {
      status: "",
      updated_by: userdata.username
    };
    this.deleteHandler = this.deleteHandler.bind(this);
  }

  deleteHandler() {
    let data = this.props.servis;
    data["updated_by"] = this.state.updated_by;
    this.props.servisDelete(data);
    this.props.closeModalHandler();
  }

  componentWillReceiveProps(newStatus) {
    this.setState({
      status: newStatus.ambil1.statusDEL
    });
  }

  render() {
    this.state.status === 200
      ? this.props.modalStatus(1, "Di Hapus!", this.props.servis.kd_servis)
      : console.log(this.state.status);

    return (
      <Modal isOpen={this.props.delete} className={this.props.className}>
        <ModalHeader> Hapus data pengambilan barang </ModalHeader>
        <ModalBody>
          <p>
            Apa anda ingin menghapus data{" "}
            <strong>{this.props.servis.kd_servis}</strong> ?
          </p>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={this.deleteHandler}>
            Ya
          </Button>
          <Button color="danger" onClick={this.props.closeModalHandler}>
            Tidak
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

DeleteBarangS.propTypes = {
  servisDelete: PropTypes.func.isRequired,
  ambil1: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  ambil1: state.servisBReducers
});

export default connect(
  mapStateToProps,
  { servisDelete }
)(DeleteBarangS);
