import React from "react";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  Alert
} from "reactstrap";

import PropTypes from "prop-types";
import apiconfig from "../../../config/api.config.json";
import moment from "moment";
import Konfirm from "../../utility/confirm";
import { connect } from "react-redux";
import { servisUpdate } from "../../../actions/barangServisActions";

class EditBarangS extends React.Component {
  constructor(props) {
    super(props);
    let userdata = JSON.parse(localStorage.getItem(apiconfig.LS.USERDATA));
    this.state = {
      formdata: {
        kd_penyervis: "",
        kd_barang: "",
        lokasi: "",
        kondisi_sebelum: "",
        catatan_sebelum: "",
        kondisi_sesudah: "",
        catatan_sesudah: "",
        jumlah_servis: "",
        tangal_servis: "",
        jumlah_kembali: "",
        tanggal_kembali: ""
      },
      konfirmasi: false,
      updated_by: userdata.username,
      status: "",
      flagStatus: null,
      alertData: {
        status: false,
        message: ""
      }
    };
    this.changeHandler = this.changeHandler.bind(this);
  }

  changeHandler(e) {
    let tmp = this.state.formdata;
    tmp[e.target.name] = e.target.value;
    this.setState({
      alertData: {
        status: false,
        message: ""
      },
      formdata: tmp
    });
  }

  validasiKembali = (jumlah_ambil, jumlah_kembali) => {
    let ambil = parseInt(jumlah_ambil);
    let kembali = parseInt(jumlah_kembali);
    if (kembali === ambil) {
      return true;
    } else {
      return false;
    }
  };

  submitValidasi = () => {
    const {
      kd_penyervis,
      kd_barang,
      lokasi,
      kondisi_sebelum,
      catatan_sebelum,
      kondisi_sesudah,
      catatan_sesudah,
      jumlah_servis,
      tangal_servis,
      jumlah_kembali,
      tanggal_kembali
    } = this.state.formdata;
    if (
      kd_penyervis === "" ||
      kd_barang === "" ||
      lokasi === "" ||
      kondisi_sebelum === "" ||
      catatan_sebelum === "" ||
      kondisi_sesudah === "" ||
      catatan_sesudah === "" ||
      jumlah_servis === "" ||
      tangal_servis === "" ||
      jumlah_kembali === null ||
      tanggal_kembali === null
    ) {
      this.setState({
        alertData: {
          status: true,
          message: "Semua field harus di isi!"
        }
      });
    } else if (this.validasiKembali(jumlah_servis, jumlah_kembali) === false) {
      this.setState({
        alertData: {
          status: true,
          message: "Jumlah barang yang di kembalikan harus sama!"
        }
      });
    } else {
      this.setState({ konfirmasi: true });
    }
  };

  submitHandler = () => {
    let form = this.state.formdata;
    form["updated_by"] = this.state.updated_by;
    form["status"] = 2;
    this.props.servisUpdate(this.state.formdata);
    this.props.closeModalHandler();
    this.KonfirmasiTidak();
  };

  cekStatus = data => {
    if (data === 1) {
      this.setState({ flagStatus: false });
    } else {
      this.setState({ flagStatus: true });
    }
  };

  componentWillReceiveProps(newProps) {
    this.cekStatus(newProps.currentBarangS.status);
    this.setState({
      formdata: newProps.currentBarangS,
      status: newProps.ambil.statusPUT
    });
  }

  KonfirmasiTidak = () => {
    this.setState({ konfirmasi: false });
  };

  render() {
    const t_min = moment()
      .subtract(1, "weeks")
      .format("YYYY-MM-DD");
    const t_max = moment()
      .add(1, "weeks")
      .format("YYYY-MM-DD");
    this.state.status === 200
      ? this.props.modalStatus(1, "Di Ubah!", this.state.formdata.kd_servis)
      : console.log(this.state.status);
    return (
      <Modal isOpen={this.props.edit} className="modal-dialog modal-lg">
        <ModalHeader>Pengembalian Servis Barang</ModalHeader>
        <ModalBody>
          <Konfirm
            showKonfirmasi={this.state.konfirmasi}
            ubahKonfirmasi={this.KonfirmasiTidak}
            submitHandler={this.submitHandler}
          />
          <Form>
            <div className="card mb-3">
              <div className="card-header">Data Servis Barang</div>
              <div className="card-body">
                <div className="form-row">
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="kd_servis"
                      className="col-4 col-form-label text-right"
                    >
                      Kode
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.state.formdata.kd_servis}
                        disabled
                      />
                    </div>
                  </div>
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="kd_penyervis"
                      className="col-4 col-form-label text-right"
                    >
                      Kode Pegawai
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.state.formdata.kd_penyervis}
                        disabled
                      />
                    </div>
                  </div>
                </div>
                <div className="form-row">
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="lokasi"
                      className="col-4 col-form-label text-right"
                    >
                      Lokasi Servis
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.state.formdata.lokasi}
                        disabled
                      />
                    </div>
                  </div>
                </div>
                <div className="form-row">
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="kondisi_sebelum"
                      className="col-4 col-form-label text-right"
                    >
                      Kondisi sebelum :
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.state.formdata.kondisi_sebelum}
                        disabled
                      />
                    </div>
                  </div>
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="catatan_sebelum"
                      className="col-4 col-form-label text-right"
                    >
                      Catatan sebelum :
                    </label>
                    <div className="col-8">
                      <textarea
                        type="text"
                        className="form-control"
                        value={this.state.formdata.catatan_sebelum}
                        disabled
                      />
                    </div>
                  </div>
                </div>
                <div className="form-row">
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="jumlah_servis"
                      className="col-4 col-form-label text-right"
                    >
                      Jumlah Servis :
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.state.formdata.jumlah_servis}
                        disabled
                      />
                    </div>
                  </div>
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="tangal_servis"
                      className="col-4 col-form-label text-right"
                    >
                      Tanggal Servis :
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.state.formdata.tanggal_servis}
                        disabled
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <FormGroup>
              <Label for="kondisi">Kondisi barang sesudah :</Label>
              <Input
                type="text"
                name="kondisi_sesudah"
                id="kondisi_sesudah"
                placeholder="Masukan kondisi terakhir"
                value={this.state.formdata.kondisi_sesudah}
                onChange={this.changeHandler}
                disabled={this.state.flagStatus}
              />
            </FormGroup>
            <FormGroup>
              <Label for="catatan">Catatan barang sesudah :</Label>
              <Input
                type="text"
                name="catatan_sesudah"
                id="catatan_sesudah"
                placeholder="catatan"
                value={this.state.formdata.catatan_sesudah}
                onChange={this.changeHandler}
                disabled={this.state.flagStatus}
              />
            </FormGroup>
            <FormGroup>
              <Label for="jumlah_kembali">
                Jumlah barang yang di kembalikan :
              </Label>
              <Input
                type="Number"
                name="jumlah_kembali"
                id="jumlah_kembali"
                placeholder="jumlah barang yang kembali"
                value={this.state.formdata.jumlah_kembali}
                onChange={this.changeHandler}
                disabled={this.state.flagStatus}
              />
            </FormGroup>
            <FormGroup>
              <Label for="tanggal_kembali">Tanggal Kembali barang :</Label>
              <Input
                type="date"
                name="tanggal_kembali"
                id="tanggal_kembali"
                value={this.state.formdata.tanggal_kembali}
                onChange={this.changeHandler}
                disabled={this.state.flagStatus}
                min={t_min}
                max={t_max}
              />
            </FormGroup>
          </Form>
        </ModalBody>
        <ModalFooter>
          {this.state.alertData.status === true ? (
            <Alert color="danger">{this.state.alertData.message} </Alert>
          ) : (
            ""
          )}
          <Button
            variant="contained"
            color="primary"
            onClick={this.submitValidasi}
            disabled={this.state.flagStatus}
          >
            Ubah
          </Button>
          <Button variant="contained" onClick={this.props.closeModalHandler}>
            Batal
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

EditBarangS.propTypes = {
  servisUpdate: PropTypes.func.isRequired,
  ambil: PropTypes.object.isRequired,
  ambilPegawai: PropTypes.object.isRequired,
  ambilBarang: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  ambil: state.servisBReducers,
  ambilBarang: state.barangReducers,
  ambilPegawai: state.pegawaiReducers
});

export default connect(
  mapStateToProps,
  { servisUpdate }
)(EditBarangS);
