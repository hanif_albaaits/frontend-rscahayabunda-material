import React, { Fragment } from "react";
import { Alert } from "reactstrap";
import { getServis } from "../../../actions/barangServisActions";
import { getPegawai } from "../../../actions/pegawaiActions";
import { getBarang } from "../../../actions/barangActions";

import { connect } from "react-redux";
import EditBarangS from "./editBarangS";
import CreateBarangS from "./createBarangS";
import DeleteBarangS from "./deleteBarangS";
import ViewBarangS from "./viewBarangS";
import printTable from "../../utility/printTable";
import exportCsv from "../../utility/exportCsv";
import bacaAkses from "../../utility/bacaAkses";
import Spinner from "../../utility/spinnerTable";

import PropTypes from "prop-types";
import moment from "moment";

import IconButton from "@material-ui/core/IconButton";
import FirstPageIcon from "@material-ui/icons/FirstPage";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import LastPageIcon from "@material-ui/icons/LastPage";
import TablePagination from "@material-ui/core/TablePagination";
import { withStyles } from "@material-ui/core/styles";

class ListBarangS extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      formCari: {
        kd_servis: /(?:)/,
        kd_penyervis: /(?:)/,
        kd_barang: /(?:)/,
        tanggal_servis: /(?:)/
      },
      AKSES: {},
      createBarangS: false,
      deleteBarangS: false,
      editBarangS: false,
      viewBarangS: false,
      allBarangS: [],
      currentBarangS: {},
      alertData: {
        status: 0,
        message: "",
        code: ""
      },
      hasil: [],
      page: 0,
      rowsPerPage: 5
    };
    this.showCreate = this.showCreate.bind(this);
  }
  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  deleteModalHandler = servisid => {
    let tmp = {};
    this.state.allBarangS.map(ele => {
      if (servisid === ele._id) {
        tmp = ele;
      }
      return tmp;
    });
    this.setState({
      currentBarangS: tmp,
      deleteBarangS: true
    });
  };

  viewModalHandler = servisid => {
    let tmp = {};
    this.state.allBarangS.map(ele => {
      if (servisid === ele._id) {
        tmp = ele;
      }
      return tmp;
    });
    this.setState({
      currentBarangS: tmp,
      viewBarangS: true
    });
  };

  editModalHandler = servisid => {
    let tmp = {};
    this.state.allBarangS.map(ele => {
      if (servisid === ele._id) {
        tmp = {
          _id: ele._id,
          kd_servis: ele.kd_servis,
          kd_penyervis: ele.kd_penyervis,
          kd_barang: ele.kd_barang,
          lokasi: ele.lokasi,
          kondisi_sebelum: ele.kondisi_sebelum,
          catatan_sebelum: ele.catatan_sebelum,
          kondisi_sesudah: ele.kondisi_sesudah,
          catatan_sesudah: ele.catatan_sesudah,
          jumlah_servis: ele.jumlah_servis,
          jumlah_kembali: ele.jumlah_kembali,
          tanggal_servis: ele.tanggal_servis,
          tanggal_kembali: ele.tanggal_kembali,
          status: ele.status,
          update_date: ele.update_date,
          update_by: ele.update_by
        };
        this.setState({
          currentBarangS: tmp,
          editBarangS: true
        });
      }
      return tmp;
    });
  };

  changeHandler = event => {
    let data = event.target.value;
    let char = data.replace(/\\/g, "");
    let tmp = this.state.formCari;
    if (event.target.name) {
      tmp[event.target.name] = new RegExp(char.toLowerCase());
    } else {
      tmp[event.target.name] = char;
    }
    this.cariData();
  };

  cariData = () => {
    const {
      kd_servis,
      kd_penyervis,
      kd_barang,
      tanggal_servis
    } = this.state.formCari;
    let temp = [];
    this.state.allBarangS.map(ele => {
      if (
        kd_servis.test(ele.kd_servis.toLowerCase()) &&
        kd_penyervis.test(ele.kd_penyervis.toLowerCase()) &&
        kd_barang.test(ele.kd_barang.toLowerCase()) &&
        tanggal_servis.test(ele.tanggal_servis)
      ) {
        temp.push(ele);
      }
      return temp;
    });
    this.setState({
      hasil: temp
    });
  };

  closeModalHandler = () => {
    this.setState({
      createBarangS: false,
      viewBarangS: false,
      editBarangS: false,
      deleteBarangS: false
    });
  };

  showCreate = () => {
    this.setState({ createBarangS: true });
  };

  componentDidMount() {
    this.props.getServis();
    this.props.getBarang();
    this.props.getPegawai();
  }

  componentWillReceiveProps(newProps) {
    let a = bacaAkses(newProps.ambilAccess.side, window.location.pathname);
    let allBarangS = newProps.ambil.servis;
    if (a !== null) {
      this.setState({
        AKSES: a
      });
    }
    if (allBarangS !== null) {
      this.setState({
        allBarangS: allBarangS,
        hasil: allBarangS,
        loading: false
      });
    }
  }

  modalStatus = (status, message, code) => {
    this.setState({
      alertData: {
        status: status,
        message: message,
        code: code
      },
      viewBarangS: false,
      editBarangS: false,
      deleteBarangS: false,
      createBarangS: false
    });
    setTimeout(() => {
      window.location.href = "/brg-servis";
    }, 3000);
  };

  changeDateFormat = tanggal => {
    if (tanggal === null) {
      return tanggal;
    } else {
      return moment(tanggal).format("DD/MM/YYYY");
    }
  };

  changeStatus = stat => {
    if (stat === 1) {
      return "Sedang Di Servis";
    }
    if (stat === 2) {
      return "Telah Selesai";
    }
  };

  render() {
    const AKSES = this.state.AKSES;
    const loading = this.state.loading;
    let tablelist;

    if (loading === true) {
      tablelist = <Spinner />;
    } else {
      if (this.state.allBarangS.length > 0) {
        tablelist = this.state.hasil
          .slice(
            this.state.page * this.state.rowsPerPage,
            this.state.page * this.state.rowsPerPage + this.state.rowsPerPage
          )
          .map((row, index) => {
            return (
              <tr key={index}>
                <td className="align-middle">{row.kd_servis}</td>
                <td className="align-middle">{row.kd_penyervis}</td>
                <td className="align-middle">{row.kd_barang}</td>
                <td className="align-middle">{row.nama_pegawai}</td>
                <td className="align-middle">{row.nama_barang}</td>
                <td className="align-middle">
                  {this.changeDateFormat(row.tanggal_servis)}
                </td>
                <td className="align-middle">
                  {this.changeDateFormat(row.tanggal_kembali)}
                </td>
                <td className="align-middle">
                  {this.changeStatus(row.status)}
                </td>
                <td className="align-middle text-center">
                  {AKSES.atribut === 1 || AKSES.atribut === 2 ? (
                    <button
                      type="button"
                      className="btn btn-icon btn-flat btn-default"
                      title="view"
                      onClick={() => {
                        this.viewModalHandler(row._id);
                      }}
                    >
                      <i className="icon md-search" aria-hidden="false" />
                    </button>
                  ) : (
                    ""
                  )}
                  {AKSES.atribut === 2 ? (
                    <Fragment>
                      <button
                        type="button"
                        className="btn btn-icon btn-flat btn-default"
                        title="Edit"
                        onClick={() => {
                          this.editModalHandler(row._id);
                        }}
                      >
                        <i className="icon md-edit" aria-hidden="false" />
                      </button>
                      <button
                        type="button"
                        className="btn btn-sm btn-icon btn-flat btn-default"
                        title="Delete"
                        onClick={() => {
                          this.deleteModalHandler(row._id);
                        }}
                      >
                        <i className="icon md-delete" aria-hidden="false" />
                      </button>
                    </Fragment>
                  ) : (
                    ""
                  )}
                </td>
              </tr>
            );
          });
      } else {
        tablelist = (
          <tr className="text-center">
            <td colSpan="10">Tidak Ada data yang tersedia !</td>
          </tr>
        );
      }
    }

    return (
      <div>
        <div className="page-header">
          <h1 className="page-title">Data Servis</h1>
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <a href="/dashboard">Home</a>
            </li>
            <li className="breadcrumb-item">
              <a href="/barang">Manajemen Barang</a>
            </li>
            <li className="breadcrumb-item active">Data Servis</li>
          </ol>
          <div className="page-header-actions">
            {AKSES.atribut === 2 ? (
              <div>
                <button
                  type="button"
                  className="btn btn-primary"
                  onClick={this.showCreate}
                >
                  Servis Barang
                </button>
              </div>
            ) : (
              ""
            )}
          </div>
        </div>
        <div className="page-content">
          {this.state.alertData.status === 1 ? (
            <Alert color="success">
              <b>Data {this.state.alertData.message}</b> Data Servis dengan
              referensi <strong>{this.state.alertData.code} </strong>
              telah {this.state.alertData.message}
            </Alert>
          ) : (
            ""
          )}
          {this.state.alertData.status === 2 ? (
            <Alert color="danger">{this.state.alertData.message} </Alert>
          ) : (
            ""
          )}
          <div className="panel">
            <header className="panel-heading">
              <h3 className="panel-title">List Servis</h3>
            </header>
            <div className="panel-body">
              <form className="form-inline">
                <div className="form-group">
                  <input
                    type="text"
                    placeholder="cari kode sevis"
                    className="form-control mx-sm-1"
                    name="kd_servis"
                    onChange={this.changeHandler}
                    autoComplete="off"
                  />
                  <input
                    type="text"
                    placeholder="cari ID pegawai"
                    className="form-control mx-sm-1"
                    name="kd_penyervis"
                    autoComplete="off"
                    onChange={this.changeHandler}
                  />
                  <input
                    type="text"
                    placeholder="cari kode barang"
                    className="form-control mx-sm-1"
                    name="kd_barang"
                    autoComplete="off"
                    onChange={this.changeHandler}
                  />
                  <input
                    type="date"
                    className="form-control mx-sm-1"
                    name="tanggal_servis"
                    onChange={this.changeHandler}
                    autoComplete="off"
                  />
                </div>
              </form>
              <div className="table-responsive">
                <table id="table" className="table table-bordered">
                  <thead>
                    <tr>
                      <th className="align-middle">Kode Servis</th>
                      <th className="align-middle">ID Pegawai</th>
                      <th className="align-middle">Kode Barang</th>
                      <th className="align-middle">Nama Pegawai</th>
                      <th className="align-middle">Nama Barang</th>
                      <th className="align-middle">Tanggal Servis</th>
                      <th className="align-middle">Tanggal Kembali</th>
                      <th className="align-middle">Status</th>
                      <th className="align-middle">Action</th>
                    </tr>
                  </thead>
                  <tbody>{tablelist}</tbody>
                  <TablePagination
                    colSpan={9}
                    count={this.state.hasil.length}
                    rowsPerPage={this.state.rowsPerPage}
                    page={this.state.page}
                    onChangePage={this.handleChangePage}
                    onChangeRowsPerPage={this.handleChangeRowsPerPage}
                    ActionsComponent={TablePaginationActionsWrapped}
                  />
                </table>
                <button
                  type="button"
                  className="btn btn-primary mx-sm-1"
                  onClick={() => {
                    printTable("table");
                  }}
                  hidden={true}
                >
                  Print
                </button>
                <button
                  type="button"
                  className="btn btn-primary mx-sm-1"
                  onClick={() => {
                    exportCsv("t_servis_barang");
                  }}
                >
                  Export to CSV
                </button>
                <CreateBarangS
                  create={this.state.createBarangS}
                  closeModalHandler={this.closeModalHandler}
                  modalStatus={this.modalStatus}
                  allBarangS={this.state.allBarangS}
                />
                <ViewBarangS
                  view={this.state.viewBarangS}
                  closeModalHandler={this.closeModalHandler}
                  servis={this.state.currentBarangS}
                />
                <DeleteBarangS
                  delete={this.state.deleteBarangS}
                  servis={this.state.currentBarangS}
                  closeModalHandler={this.closeModalHandler}
                  modalStatus={this.modalStatus}
                />
                <EditBarangS
                  edit={this.state.editBarangS}
                  closeModalHandler={this.closeModalHandler}
                  currentBarangS={this.state.currentBarangS}
                  modalStatus={this.modalStatus}
                  allBarangS={this.state.allBarangS}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ListBarangS.propTypes = {
  getServis: PropTypes.func.isRequired,
  getPegawai: PropTypes.func.isRequired,
  getBarang: PropTypes.func.isRequired,
  ambil: PropTypes.object.isRequired,
  ambilAccess: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  ambil: state.servisBReducers,
  ambilAccess: state.menuReducers
});

export default connect(
  mapStateToProps,
  { getServis, getPegawai, getBarang }
)(ListBarangS);

const actionsStyles = theme => ({
  root: {
    flexShrink: 0,
    color: theme.palette.text.secondary,
    marginLeft: theme.spacing.unit * 2.5
  }
});

class TablePaginationActions extends React.Component {
  handleFirstPageButtonClick = event => {
    this.props.onChangePage(event, 0);
  };

  handleBackButtonClick = event => {
    this.props.onChangePage(event, this.props.page - 1);
  };

  handleNextButtonClick = event => {
    this.props.onChangePage(event, this.props.page + 1);
  };

  handleLastPageButtonClick = event => {
    this.props.onChangePage(
      event,
      Math.max(0, Math.ceil(this.props.count / this.props.rowsPerPage) - 1)
    );
  };

  render() {
    const { classes, count, page, rowsPerPage, theme } = this.props;

    return (
      <div className={classes.root}>
        <IconButton
          onClick={this.handleFirstPageButtonClick}
          disabled={page === 0}
          aria-label="First Page"
        >
          {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
        </IconButton>
        <IconButton
          onClick={this.handleBackButtonClick}
          disabled={page === 0}
          aria-label="Previous Page"
        >
          {theme.direction === "rtl" ? (
            <KeyboardArrowRight />
          ) : (
            <KeyboardArrowLeft />
          )}
        </IconButton>
        <IconButton
          onClick={this.handleNextButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="Next Page"
        >
          {theme.direction === "rtl" ? (
            <KeyboardArrowLeft />
          ) : (
            <KeyboardArrowRight />
          )}
        </IconButton>
        <IconButton
          onClick={this.handleLastPageButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="Last Page"
        >
          {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
        </IconButton>
      </div>
    );
  }
}

TablePaginationActions.propTypes = {
  classes: PropTypes.object.isRequired,
  count: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
  theme: PropTypes.object.isRequired
};

const TablePaginationActionsWrapped = withStyles(actionsStyles, {
  withTheme: true
})(TablePaginationActions);
