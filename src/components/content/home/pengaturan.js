import React from "react";
import apiconfig from "../../../config/api.config.json";
import PropTypes from "prop-types";
import Repass from "./repass";
import { connect } from "react-redux";

class Pengaturan extends React.Component {
  constructor(props) {
    super(props);
    let userdata = JSON.parse(localStorage.getItem(apiconfig.LS.USERDATA));
    this.state = {
      userdata: {
        username: userdata.username,
        role_id: userdata.role_id,
        kd_pegawai: userdata.kd_pegawai
      },
      alertData: {
        status: 0,
        message: "",
        code: ""
      },
      showRepass: false,
      dropdownOpen: false,
      title: ""
    };
  }

  showHandler = () => {
    this.setState({ showRepass: true });
  };

  closeHandler = () => {
    this.setState({ showRepass: false });
  };

  render() {
    // alert(JSON.stringify(this.state.notif));
    return (
      <div>
        <div className="page-header">
          <h1 className="page-title">Setting</h1>
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <a href="/dashboard">Home</a>
            </li>
            <li className="breadcrumb-item active">Setting</li>
          </ol>
          <div className="page-header-actions">
            <button
              type="button"
              className="btn btn-primary"
              onClick={() => {
                alert("under-maintenance");
              }}
            >
              Setting
            </button>
          </div>
        </div>

        <div className="page-content">
          <div className="panel">
            <header className="panel-heading">
              <h3 className="panel-title">
                Hello Welcome, {this.state.userdata.username}
              </h3>
            </header>
            <div className="panel-body">
              <p>
                your ID role {this.state.userdata.role_id} dengan Kode Pegawai{" "}
                {this.state.userdata.kd_pegawai}
              </p>
              <Repass
                show={this.state.showRepass}
                closeHandler={this.closeHandler}
                username={this.state.userdata.username}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Pengaturan.propTypes = {
  ambil: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  ambil: state.loginReducers
});

export default connect(mapStateToProps)(Pengaturan);
