import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  getBarangSum,
  getBarangKeluarSum
} from "../../../actions/barangActions";
import Chart from "../../utility/chart";
import Spinner from "../../utility/spinner";
import apiconfig from "../../../config/api.config.json";

class Home extends React.Component {
  constructor(props) {
    super(props);
    let userdata = JSON.parse(localStorage.getItem(apiconfig.LS.USERDATA));
    this.state = {
      loading: true,
      userdata: userdata,
      info:
        "Admin info: maaf anda belum mengembalikan barang laptop yang di pinjam tgl 22/12/2019",
      dataExpired: [],
      dataPinjaman: [],
      chartData1: null,
      chartData2: null,
      chart1: {},
      chart2: {},
      totalkeluar: null
    };
  }

  componentWillMount() {
    this.props.getBarangSum();
    this.props.getBarangKeluarSum();
  }

  componentWillReceiveProps(newProps) {
    let a = newProps.ambilBarang.sum;
    let b = newProps.ambilBarang.keluar;
    if (a !== null && b !== null) {
      console.log(JSON.stringify(a));
      console.log(JSON.stringify(b));
      this.getChartData1(a);
      this.getChartData2(b);
      this.setState({
        loading: false
      });
    }
  }

  getChartData1(temp) {
    let data = [];
    data.push(temp.Total);
    data.push(temp.Gudang);
    data.push(temp.Good);
    data.push(temp.Expired);
    this.setState({
      chartData1: {
        labels: [
          "Total Barang",
          "Jumlah di Gudang",
          "Good Condition",
          "Expired"
        ],
        datasets: [
          {
            label: "Unit",
            data: data,
            backgroundColor: [
              "rgba(255,99,132,0.6)",
              "rgba(54,162,235,0.6)",
              "rgba(255,206,86,0.6)",
              "rgba(75,192,192,0.6)"
            ]
          }
        ]
      },
      chart1: temp
    });
  }

  getChartData2(temp) {
    let data = [];
    // data.push(temp.Total);
    data.push(temp.Pinjam);
    data.push(temp.Ambil);
    data.push(temp.Servis);
    this.setState({
      chartData2: {
        labels: ["Peminjaman", "Pengambilan", "On Service"],
        datasets: [
          {
            label: "Keluar",
            data: data,
            backgroundColor: [
              "rgba(200,99,132,0.6)",
              "rgba(30,162,235,0.6)",
              "rgba(130,70,0,0.6)"
            ]
          }
        ]
      },
      chart2: temp
    });
  }

  render() {
    return (
      <div>
        <div className="page-header">
          <h1 className="page-title">Data Barang</h1>
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <a href="/dashboard">Home</a>
            </li>
            <li className="breadcrumb-item active">Overview</li>
          </ol>
        </div>

        <div className="page-content">
          <div className="panel">
            <div className="panel-body">
              {this.state.loading === true ? (
                <Spinner />
              ) : (
                <div>
                  <div className="form-row">
                    <div className="form-group col-md-6 centered">
                      <Chart
                        chartData={this.state.chartData1}
                        legendPosition="bottom"
                        judul="Total Barang"
                      />
                    </div>
                    <div className="form-group col-md-6 centered">
                      <Chart
                        chartData={this.state.chartData2}
                        legendPosition="bottom"
                        judul="Barang Keluar"
                      />
                    </div>
                  </div>
                  <div className="form-row">
                    <div className="form-group col-md-6 centered">
                      <div className="table-responsive">
                        <table className="table table-bordered table-sm">
                          <thead>
                            <tr>
                              <th>Total Barang</th>
                              <th>Barang di Gudang</th>
                              <th>Good Condition</th>
                              <th>Expired</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>{this.state.chart1.Total}</td>
                              <td>{this.state.chart1.Gudang}</td>
                              <td>{this.state.chart1.Good}</td>
                              <td>{this.state.chart1.Expired}</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div className="form-group col-md-6 centered">
                      <div className="table-responsive">
                        <table className="table table-bordered table-sm">
                          <thead>
                            <tr>
                              <th>Total Barang Keluar</th>
                              <th>Barang yang di Pinjam</th>
                              <th>Pengambilan</th>
                              <th>On Service</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>{this.state.chart2.Total}</td>
                              <td>{this.state.chart2.Ambil}</td>
                              <td>{this.state.chart2.Pinjam}</td>
                              <td>{this.state.chart2.Servis}</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Home.propTypes = {
  getBarangKeluarSum: PropTypes.func.isRequired,
  getBarangSum: PropTypes.func.isRequired,
  ambilBarang: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  ambilBarang: state.barangReducers
});

export default connect(
  mapStateToProps,
  { getBarangKeluarSum, getBarangSum }
)(Home);
