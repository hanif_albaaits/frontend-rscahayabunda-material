import React, { Fragment } from "react";
import { Alert } from "reactstrap";
import { getDivisi } from "../../../actions/divisiActions";
import { connect } from "react-redux";
import EditDivisi from "./editDivisi";
import CreateDivisi from "./createDivisi";
import DeleteDivisi from "./deleteDivisi";
import ViewDivisi from "./viewDivisi";
import printTable from "../../utility/printTable";
import exportCsv from "../../utility/exportCsv";
import bacaAkses from "../../utility/bacaAkses";
import Spinner from "../../utility/spinnerTable";

import PropTypes from "prop-types";
import moment from "moment";

import IconButton from "@material-ui/core/IconButton";
import FirstPageIcon from "@material-ui/icons/FirstPage";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import LastPageIcon from "@material-ui/icons/LastPage";
import TablePagination from "@material-ui/core/TablePagination";
import { withStyles } from "@material-ui/core/styles";

class ListDivisi extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      formCari: {
        code: /(?:)/,
        name: /(?:)/,
        created_date: /(?:)/
      },
      AKSES: {},
      createDivisi: false,
      deleteDivisi: false,
      editDivisi: false,
      viewDivisi: false,
      allDivisi: [],
      currentDivisi: {},
      alertData: {
        status: 0,
        message: "",
        code: ""
      },
      hasil: [],
      page: 0,
      rowsPerPage: 5
    };
    this.showCreate = this.showCreate.bind(this);
  }
  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  deleteModalHandler = divisiid => {
    let tmp = {};
    this.state.allDivisi.map(ele => {
      if (divisiid === ele._id) {
        tmp = ele;
      }
      return tmp;
    });
    this.setState({
      currentDivisi: tmp,
      deleteDivisi: true
    });
  };

  viewModalHandler = divisiid => {
    let tmp = {};
    this.state.allDivisi.map(ele => {
      if (divisiid === ele._id) {
        tmp = ele;
      }
      return tmp;
    });
    this.setState({
      currentDivisi: tmp,
      viewDivisi: true
    });
  };

  editModalHandler = divisiid => {
    let tmp = {};
    this.state.allDivisi.map(ele => {
      if (divisiid === ele._id) {
        tmp = {
          _id: ele._id,
          code: ele.code,
          name: ele.name,
          description: ele.description,
          update_date: ele.update_date,
          update_by: ele.update_by
        };
        this.setState({
          currentDivisi: tmp,
          editDivisi: true
        });
      }
      return tmp;
    });
  };

  changeHandler = event => {
    let data = event.target.value;
    let char = data.replace(/\\/g, "");
    let tmp = this.state.formCari;
    if (event.target.name) {
      tmp[event.target.name] = new RegExp(char.toLowerCase());
    } else {
      tmp[event.target.name] = char;
    }
    this.cariData();
  };

  cariData = () => {
    const { code, name, created_date } = this.state.formCari;
    let temp = [];
    this.state.allDivisi.map(ele => {
      if (
        code.test(ele.code.toLowerCase()) &&
        name.test(ele.name.toLowerCase()) &&
        created_date.test(ele.created_date)
      ) {
        temp.push(ele);
      }
      return temp;
    });
    this.setState({
      hasil: temp
    });
  };

  closeModalHandler = () => {
    this.setState({
      createDivisi: false,
      viewDivisi: false,
      editDivisi: false,
      deleteDivisi: false
    });
  };

  showCreate = () => {
    this.setState({ createDivisi: true });
  };

  componentDidMount() {
    this.props.getDivisi();
  }

  componentWillReceiveProps(newProps) {
    let a = bacaAkses(newProps.ambilAccess.side, window.location.pathname);
    let allDivisi = newProps.ambil.divisi;
    if (a !== null) {
      this.setState({
        AKSES: a
      });
    }
    if (allDivisi !== null) {
      this.setState({
        allDivisi: allDivisi,
        hasil: allDivisi,
        loading: false
      });
    }
  }

  modalStatus = (status, message, code) => {
    this.setState({
      alertData: {
        status: status,
        message: message,
        code: code
      },
      viewDivisi: false,
      editDivisi: false,
      deleteDivisi: false,
      createDivisi: false
    });
    setTimeout(() => {
      window.location.href = "/divisi";
    }, 3000);
  };

  changeDateFormat = tanggal => {
    return moment(tanggal, "YYYY-MM-DD hh:mm:ss a").format("DD/MM/YYYY");
  };

  render() {
    const AKSES = this.state.AKSES;
    const loading = this.state.loading;
    let tablelist;

    if (loading === true) {
      tablelist = <Spinner />;
    } else {
      if (this.state.allDivisi.length > 0) {
        tablelist = this.state.hasil
          .slice(
            this.state.page * this.state.rowsPerPage,
            this.state.page * this.state.rowsPerPage + this.state.rowsPerPage
          )
          .map((row, index) => {
            return (
              <tr key={index}>
                <td className="align-middle">{row.code}</td>
                <td className="align-middle">{row.name}</td>
                <td className="align-middle">
                  {this.changeDateFormat(row.created_date)}
                </td>
                <td className="align-middle">{row.created_by}</td>
                <td className="align-middle text-center">
                  {AKSES.atribut === 1 || AKSES.atribut === 2 ? (
                    <button
                      type="button"
                      className="btn btn-icon btn-flat btn-default"
                      title="view"
                      onClick={() => {
                        this.viewModalHandler(row._id);
                      }}
                    >
                      <i className="icon md-search" aria-hidden="false" />
                    </button>
                  ) : (
                    ""
                  )}
                  {AKSES.atribut === 2 ? (
                    <Fragment>
                      <button
                        type="button"
                        className="btn btn-icon btn-flat btn-default"
                        title="Edit"
                        onClick={() => {
                          this.editModalHandler(row._id);
                        }}
                      >
                        <i className="icon md-edit" aria-hidden="false" />
                      </button>
                      <button
                        type="button"
                        className="btn btn-sm btn-icon btn-flat btn-default"
                        title="Delete"
                        onClick={() => {
                          this.deleteModalHandler(row._id);
                        }}
                      >
                        <i className="icon md-delete" aria-hidden="false" />
                      </button>
                    </Fragment>
                  ) : (
                    ""
                  )}
                </td>
              </tr>
            );
          });
      } else {
        tablelist = (
          <tr className="text-center">
            <td colSpan="10">Tidak Ada data yang tersedia !</td>
          </tr>
        );
      }
    }

    return (
      <div>
        <div className="page-header">
          <h1 className="page-title">Master Divisi</h1>
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <a href="/dashboard">Home</a>
            </li>
            <li className="breadcrumb-item">
              <a href="/dashboard">Master</a>
            </li>
            <li className="breadcrumb-item active">Master Divisi</li>
          </ol>
          <div className="page-header-actions">
            {AKSES.atribut === 2 ? (
              <div>
                <button
                  type="button"
                  className="btn btn-primary"
                  onClick={this.showCreate}
                >
                  Tambah Divisi
                </button>
              </div>
            ) : (
              ""
            )}
          </div>
        </div>

        <div className="page-content">
          {this.state.alertData.status === 1 ? (
            <Alert color="success">
              <b>Data {this.state.alertData.message}</b> Data Divisi dengan
              referensi code <strong>{this.state.alertData.code} </strong>
              telah {this.state.alertData.message}
            </Alert>
          ) : (
            ""
          )}
          {this.state.alertData.status === 2 ? (
            <Alert color="danger">{this.state.alertData.message} </Alert>
          ) : (
            ""
          )}
          <div className="panel">
            <header className="panel-heading">
              <h3 className="panel-title">List Divisi</h3>
            </header>
            <div className="panel-body">
              <form className="form-inline">
                <div className="form-group">
                  <input
                    type="text"
                    placeholder="cari ID"
                    className="form-control mx-sm-1"
                    name="code"
                    onChange={this.changeHandler}
                    autoComplete="off"
                  />
                  <input
                    type="text"
                    placeholder="cari divisi"
                    className="form-control mx-sm-1"
                    name="name"
                    onChange={this.changeHandler}
                    autoComplete="off"
                  />
                  <input
                    type="date"
                    className="form-control mx-sm-1"
                    name="created_date"
                    onChange={this.changeHandler}
                  />
                </div>
              </form>
              <div className="example table-responsive">
                <table id="table" className="table table-bordered">
                  <thead>
                    <tr>
                      <th>ID Divisi</th>
                      <th>Nama Divisi</th>
                      <th>Created Date</th>
                      <th>Created By</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>{tablelist}</tbody>
                  <TablePagination
                    colSpan={5}
                    count={this.state.hasil.length}
                    rowsPerPage={this.state.rowsPerPage}
                    page={this.state.page}
                    onChangePage={this.handleChangePage}
                    onChangeRowsPerPage={this.handleChangeRowsPerPage}
                    ActionsComponent={TablePaginationActionsWrapped}
                  />
                </table>
                <button
                  type="button"
                  className="btn btn-primary mx-sm-1"
                  onClick={() => {
                    printTable("table");
                  }}
                  hidden={true}
                >
                  Print
                </button>
                <button
                  type="button"
                  className="btn btn-primary mx-sm-1"
                  onClick={() => {
                    exportCsv("m_divisi");
                  }}
                >
                  Export to CSV
                </button>
                <ViewDivisi
                  view={this.state.viewDivisi}
                  closeModalHandler={this.closeModalHandler}
                  divisi={this.state.currentDivisi}
                />
                <DeleteDivisi
                  delete={this.state.deleteDivisi}
                  divisi={this.state.currentDivisi}
                  closeModalHandler={this.closeModalHandler}
                  modalStatus={this.modalStatus}
                />
                <CreateDivisi
                  create={this.state.createDivisi}
                  closeModalHandler={this.closeModalHandler}
                  modalStatus={this.modalStatus}
                  allDivisi={this.state.allDivisi}
                />
                <EditDivisi
                  edit={this.state.editDivisi}
                  closeModalHandler={this.closeModalHandler}
                  currentDivisi={this.state.currentDivisi}
                  name={this.state.currentDivisi.name}
                  modalStatus={this.modalStatus}
                  allDivisi={this.state.allDivisi}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ListDivisi.propTypes = {
  getDivisi: PropTypes.func.isRequired,
  ambil: PropTypes.object.isRequired,
  ambilAccess: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  ambil: state.divisiReducers,
  ambilAccess: state.menuReducers
});

export default connect(
  mapStateToProps,
  { getDivisi }
)(ListDivisi);

const actionsStyles = theme => ({
  root: {
    flexShrink: 0,
    color: theme.palette.text.secondary,
    marginLeft: theme.spacing.unit * 2.5
  }
});

class TablePaginationActions extends React.Component {
  handleFirstPageButtonClick = event => {
    this.props.onChangePage(event, 0);
  };

  handleBackButtonClick = event => {
    this.props.onChangePage(event, this.props.page - 1);
  };

  handleNextButtonClick = event => {
    this.props.onChangePage(event, this.props.page + 1);
  };

  handleLastPageButtonClick = event => {
    this.props.onChangePage(
      event,
      Math.max(0, Math.ceil(this.props.count / this.props.rowsPerPage) - 1)
    );
  };

  render() {
    const { classes, count, page, rowsPerPage, theme } = this.props;

    return (
      <div className={classes.root}>
        <IconButton
          onClick={this.handleFirstPageButtonClick}
          disabled={page === 0}
          aria-label="First Page"
        >
          {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
        </IconButton>
        <IconButton
          onClick={this.handleBackButtonClick}
          disabled={page === 0}
          aria-label="Previous Page"
        >
          {theme.direction === "rtl" ? (
            <KeyboardArrowRight />
          ) : (
            <KeyboardArrowLeft />
          )}
        </IconButton>
        <IconButton
          onClick={this.handleNextButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="Next Page"
        >
          {theme.direction === "rtl" ? (
            <KeyboardArrowLeft />
          ) : (
            <KeyboardArrowRight />
          )}
        </IconButton>
        <IconButton
          onClick={this.handleLastPageButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="Last Page"
        >
          {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
        </IconButton>
      </div>
    );
  }
}

TablePaginationActions.propTypes = {
  classes: PropTypes.object.isRequired,
  count: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
  theme: PropTypes.object.isRequired
};

const TablePaginationActionsWrapped = withStyles(actionsStyles, {
  withTheme: true
})(TablePaginationActions);
