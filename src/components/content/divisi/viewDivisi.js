import React from "react";
import { Modal, ModalBody, ModalFooter, ModalHeader, Button } from "reactstrap";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";

const styles = theme => ({
  root: {
    width: "100%",
    flexGrow: 1
  },
  table: {
    minWidth: 700
  }
});

class ViewDivisi extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <Modal isOpen={this.props.view} className={this.props.className}>
        <ModalHeader> Lihat Divisi</ModalHeader>
        <ModalBody>
          <div>
            <h3>{this.props.divisi.name} </h3>
            <h5>{this.props.divisi.code} </h5>
          </div>
          <div className={classes.root}>
            <Grid container spacing={24}>
              <Grid item xs={6}>
                Divisi ID
                <br />
                Nama Divisi
                <br />
                Created By
                <br />
                Created Date
                <br />
                Updated By
                <br />
                Updated Date
                <br />
                Description
              </Grid>
              <Grid item xs={6}>
                {this.props.divisi.code}
                <br />
                {this.props.divisi.name}
                <br />
                {this.props.divisi.created_by}
                <br />
                {this.props.divisi.created_date}
                <br />
                {this.props.divisi.updated_by}
                <br />
                {this.props.divisi.updated_date}
                <br />
                {this.props.divisi.description}
              </Grid>
            </Grid>
          </div>
        </ModalBody>
        <ModalFooter>
          <Button color="danger" onClick={this.props.closeModalHandler}>
            Tutup
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

ViewDivisi.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ViewDivisi);
