import React from "react";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  Alert
} from "reactstrap";
import PropTypes from "prop-types";
import apiconfig from "../../../config/api.config.json";

import Konfirm from "../../utility/confirm";
import { connect } from "react-redux";
import { divisiUpdate } from "../../../actions/divisiActions";

class EditDivisi extends React.Component {
  constructor(props) {
    super(props);
    let userdata = JSON.parse(localStorage.getItem(apiconfig.LS.USERDATA));

    this.state = {
      formdata: {
        name: "",
        description: ""
      },
      konfirmasi: false,
      currentDivisiname: "",
      updated_by: userdata.username,
      status: "",
      alertData: {
        status: false,
        message: ""
      },
      labelWidth: 0
    };
    this.changeHandler = this.changeHandler.bind(this);
  }

  changeHandler(e) {
    let tmp = this.state.formdata;
    tmp[e.target.name] = e.target.value;
    this.setState({
      alertData: {
        status: false,
        message: ""
      },
      formdata: tmp
    });
  }

  validateFilter(divisiname) {
    let lama = this.props.name;
    let allDivisi = this.props.allDivisi.map(ele => ele.name);
    let a = allDivisi.filter(e => e === divisiname);
    if (a.length === 0 || divisiname === lama) {
      return false;
    } else {
      return true;
    }
  }

  submitValidasi = () => {
    if (
      this.state.formdata.name === "" ||
      this.state.formdata.description === ""
    ) {
      this.setState({
        alertData: {
          status: true,
          message: "Semua field harus di isi!"
        }
      });
    } else if (this.validateFilter(this.state.formdata.name) === true) {
      this.setState({
        alertData: {
          status: true,
          message: "Ouh, nama divisi kamu telah digunakan !"
        }
      });
    } else {
      this.setState({ konfirmasi: true });
    }
  };

  submitHandler = () => {
    let form = this.state.formdata;
    form["updated_by"] = this.state.updated_by;
    this.props.divisiUpdate(this.state.formdata);
    this.props.closeModalHandler();
    this.KonfirmasiTidak();
  };

  KonfirmasiTidak = () => {
    this.setState({ konfirmasi: false });
  };

  componentWillReceiveProps(newProps) {
    this.setState({
      formdata: newProps.currentDivisi,
      status: newProps.ambil.statusPUT
    });
  }

  render() {
    this.state.status === 200
      ? this.props.modalStatus(1, "Di Ubah!", this.state.formdata.code)
      : console.log(this.state.status);
    return (
      <Modal isOpen={this.props.edit} className={this.props.className}>
        <ModalHeader>Ubah Data Divisi</ModalHeader>
        <ModalBody>
          <Konfirm
            showKonfirmasi={this.state.konfirmasi}
            ubahKonfirmasi={this.KonfirmasiTidak}
            submitHandler={this.submitHandler}
          />
          <Form>
            <FormGroup>
              <Label for="divisiname">Nama Divisi</Label>
              <Input
                type="text"
                name="name"
                id="name"
                placeholder="masukan divisiname"
                value={this.state.formdata.name}
                onChange={this.changeHandler}
              />
            </FormGroup>
            <FormGroup>
              <Label for="description">Deskripsi Divisi</Label>
              <Input
                type="text"
                name="description"
                id="description"
                placeholder="Deskripsi Divisi"
                value={this.state.formdata.description}
                onChange={this.changeHandler}
              />
            </FormGroup>
          </Form>
        </ModalBody>
        <ModalFooter>
          {this.state.alertData.status === true ? (
            <Alert color="danger">{this.state.alertData.message} </Alert>
          ) : (
            ""
          )}
          <Button
            variant="contained"
            color="primary"
            onClick={this.submitValidasi}
          >
            Ubah
          </Button>
          <Button variant="contained" onClick={this.props.closeModalHandler}>
            Batal
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

EditDivisi.propTypes = {
  divisiUpdate: PropTypes.func.isRequired,
  ambil: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  ambil: state.divisiReducers
});

export default connect(
  mapStateToProps,
  { divisiUpdate }
)(EditDivisi);
