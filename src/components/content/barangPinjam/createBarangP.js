import React from "react";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  Alert
} from "reactstrap";
import Select from "react-select";
import PropTypes from "prop-types";
import apiconfig from "../../../config/api.config.json";
import moment from "moment";
import Konfirm from "../../utility/confirm";
import { connect } from "react-redux";
import { pinjamCreate } from "../../../actions/barangPinjamActions";

class CreateBarangP extends React.Component {
  constructor(props) {
    super(props);
    let userdata = JSON.parse(localStorage.getItem(apiconfig.LS.USERDATA));
    this.state = {
      formdata: {
        kd_peminjam: "",
        kd_barang: "",
        kd_lokasi: "",
        kondisi_sebelum: "",
        catatan_sebelum: "",
        jumlah_pinjam: "",
        tanggal_pinjam: "",
        lama_peminjaman: "",
        created_by: userdata.username
      },
      konfirmasi: false,
      status: "",
      alertData: {
        status: false,
        message: ""
      }
    };
    this.changeHandler = this.changeHandler.bind(this);
  }

  changeHandler(e) {
    let tmp = this.state.formdata;
    tmp[e.target.name] = e.target.value;
    this.setState({
      alertData: {
        status: false,
        message: ""
      },
      formdata: tmp
    });
  }

  select1 = selectedOption => {
    let tmp = this.state.formdata;
    tmp[selectedOption.name] = selectedOption.value;
    this.setState({
      formdata: tmp
    });
  };

  validateAmbil(jumlah, kd_barang) {
    let allBarang = this.props.ambilBarang.barang;
    let total = 0;
    allBarang.map(row => {
      if (row.kd_barang === kd_barang) {
        return (total += row.jumlah);
      }
      return total;
    });
    let x = parseInt(jumlah);
    if (x > total) {
      return true;
    } else {
      return false;
    }
  }

  submitValidasi = () => {
    const {
      kd_peminjam,
      kd_barang,
      kd_lokasi,
      kondisi_sebelum,
      catatan_sebelum,
      jumlah_pinjam,
      tanggal_pinjam,
      lama_peminjaman
    } = this.state.formdata;
    if (
      kd_peminjam === "" ||
      kd_barang === "" ||
      kd_lokasi === "" ||
      kondisi_sebelum === "" ||
      catatan_sebelum === "" ||
      jumlah_pinjam === "" ||
      tanggal_pinjam === "" ||
      lama_peminjaman === ""
    ) {
      this.setState({
        alertData: {
          status: true,
          message: "Semua field harus di isi!"
        }
      });
    } else if (this.validateAmbil(jumlah_pinjam, kd_barang) === true) {
      this.setState({
        alertData: {
          status: true,
          message: "barang yang di ambil melebihi !"
        }
      });
    } else {
      this.setState({ konfirmasi: true });
    }
  };

  submitHandler = () => {
    let form = this.state.formdata;
    form["status"] = 1;
    this.props.pinjamCreate(form);
    this.props.closeModalHandler();
    this.KonfirmasiTidak();
  };

  componentWillReceiveProps(newStatus) {
    this.setState({
      status: newStatus.ambil.statusADD
    });
  }

  KonfirmasiTidak = () => {
    this.setState({ konfirmasi: false });
  };

  render() {
    const sLokasi = this.props.ambilLokasi.lokasi.map((row, x) => {
      return {
        value: row.code,
        label: row.name,
        name: "kd_lokasi"
      };
    });
    const sBarang = this.props.ambilBarang.barang.map((row, x) => {
      return {
        value: row.kd_barang,
        label: row.nama + " " + row.merk,
        name: "kd_barang"
      };
    });
    const sPegawai = this.props.ambilPegawai.pegawai.map((row, x) => {
      return {
        value: row.kd_pegawai,
        label: row.name,
        name: "kd_peminjam"
      };
    });
    const today = moment().format("YYYY-MM-DD");
    const t_max_lama = moment()
      .add(1, "months")
      .format("YYYY-MM-DD");
    const t_max = moment()
      .add(1, "weeks")
      .format("YYYY-MM-DD");

    this.state.status === 200
      ? this.props.modalStatus(1, "Di Tambahkan", this.state.formdata.kd_barang)
      : console.log(this.state.status);
    return (
      <Modal isOpen={this.props.create} className="modal-dialog modal-lg">
        <ModalHeader>Pinjam Barang Baru</ModalHeader>
        <ModalBody>
          <Konfirm
            showKonfirmasi={this.state.konfirmasi}
            ubahKonfirmasi={this.KonfirmasiTidak}
            submitHandler={this.submitHandler}
          />
          <Form>
            <FormGroup>
              <Label for="sBarang">Pilih Barang yang dipinjam :</Label>
              <Select
                className="basic-single"
                classNamePrefix="select"
                label="sBarang"
                name="kd_barang"
                options={sBarang}
                value={this.state.kd_barang}
                onChange={this.select1}
              />
            </FormGroup>
            <FormGroup>
              <Label for="sPegawai">Pilih Pegawai yang meminjam :</Label>
              <Select
                className="basic-single"
                classNamePrefix="select"
                label="sPegawai"
                name="kd_peminjam"
                options={sPegawai}
                value={this.state.kd_peminjam}
                onChange={this.select1}
              />
            </FormGroup>
            <FormGroup>
              <Label for="sLokasi">Pilih Lokasi barang :</Label>
              <Select
                className="basic-single"
                classNamePrefix="select"
                label="sLokasi"
                name="kd_lokasi"
                options={sLokasi}
                value={this.state.kd_lokasi}
                onChange={this.select1}
              />
            </FormGroup>
            <FormGroup>
              <Label for="kondisi_sebelum">
                Kondisi barang sebelum dipinjam :
              </Label>
              <Input
                type="text"
                name="kondisi_sebelum"
                id="kondisi_sebelum"
                placeholder="Masukan kondisi terakhir"
                value={this.state.formdata.kondisi}
                onChange={this.changeHandler}
              />
            </FormGroup>
            <FormGroup>
              <Label for="catatan_sebelum">
                Catatan barang sebelum dipinjam :
              </Label>
              <Input
                type="text"
                name="catatan_sebelum"
                id="catatan_sebelum"
                placeholder="catatan_sebelum"
                value={this.state.formdata.catatan_sebelum}
                onChange={this.changeHandler}
              />
            </FormGroup>
            <FormGroup>
              <Label for="jumlah_pinjam">Jumlah barang yang di pinjam :</Label>
              <Input
                type="number"
                name="jumlah_pinjam"
                id="jumlah_pinjam"
                placeholder="jumlah barang yang dipinjam"
                value={this.state.formdata.jumlah_pinjam}
                onChange={this.changeHandler}
              />
            </FormGroup>
            <FormGroup>
              <Label for="tanggal_pinjam">Tanggal Pinjam barang :</Label>
              <Input
                type="date"
                name="tanggal_pinjam"
                id="tanggal_pinjam"
                value={this.state.formdata.tanggal_pinjam}
                onChange={this.changeHandler}
                min={today}
                max={t_max}
              />
            </FormGroup>
            <FormGroup>
              <Label for="lama_peminjaman">Tanggal Lama Peminjaman :</Label>
              <Input
                type="date"
                name="lama_peminjaman"
                id="lama_peminjaman"
                value={this.state.formdata.lama_peminjaman}
                onChange={this.changeHandler}
                min={today}
                max={t_max_lama}
              />
            </FormGroup>
          </Form>
        </ModalBody>
        <ModalFooter>
          {this.state.alertData.status === true ? (
            <Alert color="danger">{this.state.alertData.message} </Alert>
          ) : (
            ""
          )}
          <Button
            variant="contained"
            color="primary"
            onClick={this.submitValidasi}
          >
            Tambahkan
          </Button>
          <Button variant="contained" onClick={this.props.closeModalHandler}>
            Batal
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

CreateBarangP.propTypes = {
  pinjamCreate: PropTypes.func.isRequired,
  ambil: PropTypes.object.isRequired,
  ambilLokasi: PropTypes.object.isRequired,
  ambilPegawai: PropTypes.object.isRequired,
  ambilBarang: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  ambil: state.pinjamBReducers,
  ambilLokasi: state.lokasiReducers,
  ambilBarang: state.barangReducers,
  ambilPegawai: state.pegawaiReducers
});

export default connect(
  mapStateToProps,
  { pinjamCreate }
)(CreateBarangP);
