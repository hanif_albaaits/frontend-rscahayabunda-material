import React, { Fragment } from "react";
import { Alert } from "reactstrap";
import { getPinjam } from "../../../actions/barangPinjamActions";
import { getLokasi } from "../../../actions/lokasiActions";
import { getPegawai } from "../../../actions/pegawaiActions";
import { getBarang } from "../../../actions/barangActions";

import { connect } from "react-redux";
import EditBarangP from "./editBarangP";
import CreateBarangP from "./createBarangP";
import DeleteBarangP from "./deleteBarangP";
import ViewBarangP from "./viewBarangP";
import printTable from "../../utility/printTable";
import exportCsv from "../../utility/exportCsv";
import bacaAkses from "../../utility/bacaAkses";
import Spinner from "../../utility/spinnerTable";

import PropTypes from "prop-types";
import moment from "moment";

import IconButton from "@material-ui/core/IconButton";
import FirstPageIcon from "@material-ui/icons/FirstPage";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import LastPageIcon from "@material-ui/icons/LastPage";
import TablePagination from "@material-ui/core/TablePagination";
import { withStyles } from "@material-ui/core/styles";

class ListBarangP extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      formCari: {
        kd_pinjam: /(?:)/,
        kd_peminjam: /(?:)/,
        kd_barang: /(?:)/,
        tanggal_pinjam: /(?:)/
      },
      AKSES: {},
      createBarangP: false,
      deleteBarangP: false,
      editBarangP: false,
      viewBarangP: false,
      allBarangP: [],
      currentBarangP: {},
      alertData: {
        status: 0,
        message: "",
        code: ""
      },
      hasil: [],
      page: 0,
      rowsPerPage: 5
    };
    this.showCreate = this.showCreate.bind(this);
  }
  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  deleteModalHandler = pinjamid => {
    let tmp = {};
    this.state.allBarangP.map(ele => {
      if (pinjamid === ele._id) {
        tmp = ele;
      }
      return tmp;
    });
    this.setState({
      currentBarangP: tmp,
      deleteBarangP: true
    });
  };

  viewModalHandler = pinjamid => {
    let tmp = {};
    this.state.allBarangP.map(ele => {
      if (pinjamid === ele._id) {
        tmp = ele;
      }
      return tmp;
    });
    this.setState({
      currentBarangP: tmp,
      viewBarangP: true
    });
  };

  editModalHandler = pinjamid => {
    let tmp = {};
    this.state.allBarangP.map(ele => {
      if (pinjamid === ele._id) {
        tmp = {
          _id: ele._id,
          kd_pinjam: ele.kd_pinjam,
          kd_peminjam: ele.kd_peminjam,
          kd_barang: ele.kd_barang,
          kd_lokasi: ele.kd_lokasi,
          kondisi_sebelum: ele.kondisi_sebelum,
          catatan_sebelum: ele.catatan_sebelum,
          kondisi_sesudah: ele.kondisi_sesudah,
          catatan_sesudah: ele.catatan_sesudah,
          jumlah_pinjam: ele.jumlah_pinjam,
          jumlah_kembali: ele.jumlah_kembali,
          tanggal_pinjam: ele.tanggal_pinjam,
          tanggal_kembali: ele.tanggal_kembali,
          status: ele.status,
          update_date: ele.update_date,
          update_by: ele.update_by
        };
        this.setState({
          currentBarangP: tmp,
          editBarangP: true
        });
      }
      return tmp;
    });
  };

  changeHandler = event => {
    let data = event.target.value;
    let char = data.replace(/\\/g, "");
    let tmp = this.state.formCari;
    if (event.target.name) {
      tmp[event.target.name] = new RegExp(char.toLowerCase());
    } else {
      tmp[event.target.name] = char;
    }
    this.cariData();
  };

  cariData = () => {
    const {
      kd_pinjam,
      kd_peminjam,
      kd_barang,
      tanggal_pinjam
    } = this.state.formCari;
    let temp = [];
    this.state.allBarangP.map(ele => {
      if (
        kd_pinjam.test(ele.kd_pinjam.toLowerCase()) &&
        kd_peminjam.test(ele.kd_peminjam.toLowerCase()) &&
        kd_barang.test(ele.kd_barang.toLowerCase()) &&
        tanggal_pinjam.test(ele.tanggal_pinjam)
      ) {
        temp.push(ele);
      }
      return temp;
    });
    this.setState({
      hasil: temp
    });
  };

  closeModalHandler = () => {
    this.setState({
      createBarangP: false,
      viewBarangP: false,
      editBarangP: false,
      deleteBarangP: false
    });
  };

  showCreate = () => {
    this.setState({ createBarangP: true });
  };

  componentDidMount() {
    this.props.getPinjam();
    this.props.getBarang();
    this.props.getPegawai();
    this.props.getLokasi();
  }

  componentWillReceiveProps(newProps) {
    let a = bacaAkses(newProps.ambilAccess.side, window.location.pathname);
    let allBarangP = newProps.ambil.pinjam;
    if (a !== null) {
      this.setState({
        AKSES: a
      });
    }
    if (allBarangP !== null) {
      this.setState({
        allBarangP: allBarangP,
        hasil: allBarangP,
        loading: false
      });
    }
  }

  modalStatus = (status, message, code) => {
    this.setState({
      alertData: {
        status: status,
        message: message,
        code: code
      },
      viewBarangP: false,
      editBarangP: false,
      deleteBarangP: false,
      createBarangP: false
    });
    setTimeout(() => {
      window.location.href = "/brg-pinjam";
    }, 3000);
  };

  changeDateFormat = tanggal => {
    if (tanggal === null) {
      return tanggal;
    } else {
      return moment(tanggal).format("DD/MM/YYYY");
    }
  };

  changeStatus = stat => {
    if (stat === 1) {
      return "Sedang Di Pinjam";
    }
    if (stat === 2) {
      return "Sudah Di Kembalikan";
    }
  };

  render() {
    const AKSES = this.state.AKSES;
    const loading = this.state.loading;
    let tablelist;

    if (loading === true) {
      tablelist = <Spinner />;
    } else {
      if (this.state.allBarangP.length > 0) {
        tablelist = this.state.hasil
          .slice(
            this.state.page * this.state.rowsPerPage,
            this.state.page * this.state.rowsPerPage + this.state.rowsPerPage
          )
          .map((row, index) => {
            return (
              <tr key={index}>
                <td className="align-middle">{row.kd_pinjam}</td>
                <td className="align-middle">{row.kd_peminjam}</td>
                <td className="align-middle">{row.kd_barang}</td>
                <td className="align-middle">{row.nama_pegawai}</td>
                <td className="align-middle">{row.nama_barang}</td>
                <td className="align-middle">
                  {this.changeDateFormat(row.tanggal_pinjam)}
                </td>
                <td className="align-middle">
                  {this.changeDateFormat(row.tanggal_kembali)}
                </td>
                <td className="align-middle">
                  {this.changeStatus(row.status)}
                </td>
                <td className="align-middle text-center">
                  {AKSES.atribut === 1 || AKSES.atribut === 2 ? (
                    <button
                      type="button"
                      className="btn btn-icon btn-flat btn-default"
                      title="view"
                      onClick={() => {
                        this.viewModalHandler(row._id);
                      }}
                    >
                      <i className="icon md-search" aria-hidden="false" />
                    </button>
                  ) : (
                    ""
                  )}
                  {AKSES.atribut === 2 ? (
                    <Fragment>
                      <button
                        type="button"
                        className="btn btn-icon btn-flat btn-default"
                        title="Edit"
                        onClick={() => {
                          this.editModalHandler(row._id);
                        }}
                      >
                        <i className="icon md-edit" aria-hidden="false" />
                      </button>
                      <button
                        type="button"
                        className="btn btn-sm btn-icon btn-flat btn-default"
                        title="Delete"
                        onClick={() => {
                          this.deleteModalHandler(row._id);
                        }}
                      >
                        <i className="icon md-delete" aria-hidden="false" />
                      </button>
                    </Fragment>
                  ) : (
                    ""
                  )}
                </td>
              </tr>
            );
          });
      } else {
        tablelist = (
          <tr className="text-center">
            <td colSpan="10">Tidak Ada data yang tersedia !</td>
          </tr>
        );
      }
    }

    return (
      <div>
        <div className="page-header">
          <h1 className="page-title">Data Peminjaman</h1>
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <a href="/dashboard">Home</a>
            </li>
            <li className="breadcrumb-item">
              <a href="/barang">Manajemen Barang</a>
            </li>
            <li className="breadcrumb-item active">Data Peminjaman</li>
          </ol>
          <div className="page-header-actions">
            {AKSES.atribut === 2 ? (
              <div>
                <button
                  type="button"
                  className="btn btn-primary"
                  onClick={this.showCreate}
                >
                  Pinjam Barang
                </button>
              </div>
            ) : (
              ""
            )}
          </div>
        </div>
        <div className="page-content">
          {this.state.alertData.status === 1 ? (
            <Alert color="success">
              <b>Data {this.state.alertData.message}</b> Data Peminjaman dengan
              referensi <strong>{this.state.alertData.code} </strong>
              telah {this.state.alertData.message}
            </Alert>
          ) : (
            ""
          )}
          {this.state.alertData.status === 2 ? (
            <Alert color="danger">{this.state.alertData.message} </Alert>
          ) : (
            ""
          )}
          <div className="panel">
            <header className="panel-heading">
              <h3 className="panel-title">List Peminjaman</h3>
            </header>
            <div className="panel-body">
              <form className="form-inline">
                <div className="form-group">
                  <input
                    type="text"
                    placeholder="cari kode peminjaman"
                    className="form-control mx-sm-1"
                    name="kd_pinjam"
                    onChange={this.changeHandler}
                    autoComplete="off"
                  />
                  <input
                    type="text"
                    placeholder="cari ID pegawai"
                    className="form-control mx-sm-1"
                    name="kd_peminjam"
                    onChange={this.changeHandler}
                    autoComplete="off"
                  />
                  <input
                    type="text"
                    placeholder="cari kode barang"
                    className="form-control mx-sm-1"
                    name="kd_barang"
                    onChange={this.changeHandler}
                    autoComplete="off"
                  />
                  <input
                    type="date"
                    className="form-control mx-sm-1"
                    name="tanggal_pinjam"
                    onChange={this.changeHandler}
                  />
                </div>
              </form>
              <div className="example table-responsive">
                <table id="table" className="table table-bordered">
                  <thead>
                    <tr>
                      <th className="align-middle">Kode Pinjam</th>
                      <th className="align-middle">ID Pegawai</th>
                      <th className="align-middle">Kode Barang</th>
                      <th className="align-middle">Nama Pegawai</th>
                      <th className="align-middle">Nama Barang</th>
                      <th className="align-middle">Tanggal Pinjam</th>
                      <th className="align-middle">Tanggal Kembali</th>
                      <th className="align-middle">Status</th>
                      <th className="align-middle">Action</th>
                    </tr>
                  </thead>
                  <tbody>{tablelist}</tbody>
                  <TablePagination
                    colSpan={9}
                    count={this.state.hasil.length}
                    rowsPerPage={this.state.rowsPerPage}
                    page={this.state.page}
                    onChangePage={this.handleChangePage}
                    onChangeRowsPerPage={this.handleChangeRowsPerPage}
                    ActionsComponent={TablePaginationActionsWrapped}
                  />
                </table>
                <button
                  type="button"
                  className="btn btn-primary mx-sm-1"
                  hidden={true}
                  onClick={() => {
                    printTable("table");
                  }}
                >
                  Print
                </button>
                <button
                  type="button"
                  className="btn btn-primary mx-sm-1"
                  onClick={() => {
                    exportCsv("t_pinjam_barang");
                  }}
                >
                  Export to CSV
                </button>
                <CreateBarangP
                  create={this.state.createBarangP}
                  closeModalHandler={this.closeModalHandler}
                  modalStatus={this.modalStatus}
                  allBarangP={this.state.allBarangP}
                />
                <ViewBarangP
                  view={this.state.viewBarangP}
                  closeModalHandler={this.closeModalHandler}
                  pinjam={this.state.currentBarangP}
                />
                <DeleteBarangP
                  delete={this.state.deleteBarangP}
                  pinjam={this.state.currentBarangP}
                  closeModalHandler={this.closeModalHandler}
                  modalStatus={this.modalStatus}
                />
                <EditBarangP
                  edit={this.state.editBarangP}
                  closeModalHandler={this.closeModalHandler}
                  currentBarangP={this.state.currentBarangP}
                  modalStatus={this.modalStatus}
                  allBarangP={this.state.allBarangP}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ListBarangP.propTypes = {
  getPinjam: PropTypes.func.isRequired,
  getPegawai: PropTypes.func.isRequired,
  getBarang: PropTypes.func.isRequired,
  getLokasi: PropTypes.func.isRequired,
  ambil: PropTypes.object.isRequired,
  ambilAccess: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  ambil: state.pinjamBReducers,
  ambilAccess: state.menuReducers
});

export default connect(
  mapStateToProps,
  { getPinjam, getPegawai, getBarang, getLokasi }
)(ListBarangP);

const actionsStyles = theme => ({
  root: {
    flexShrink: 0,
    color: theme.palette.text.secondary,
    marginLeft: theme.spacing.unit * 2.5
  }
});

class TablePaginationActions extends React.Component {
  handleFirstPageButtonClick = event => {
    this.props.onChangePage(event, 0);
  };

  handleBackButtonClick = event => {
    this.props.onChangePage(event, this.props.page - 1);
  };

  handleNextButtonClick = event => {
    this.props.onChangePage(event, this.props.page + 1);
  };

  handleLastPageButtonClick = event => {
    this.props.onChangePage(
      event,
      Math.max(0, Math.ceil(this.props.count / this.props.rowsPerPage) - 1)
    );
  };

  render() {
    const { classes, count, page, rowsPerPage, theme } = this.props;

    return (
      <div className={classes.root}>
        <IconButton
          onClick={this.handleFirstPageButtonClick}
          disabled={page === 0}
          aria-label="First Page"
        >
          {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
        </IconButton>
        <IconButton
          onClick={this.handleBackButtonClick}
          disabled={page === 0}
          aria-label="Previous Page"
        >
          {theme.direction === "rtl" ? (
            <KeyboardArrowRight />
          ) : (
            <KeyboardArrowLeft />
          )}
        </IconButton>
        <IconButton
          onClick={this.handleNextButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="Next Page"
        >
          {theme.direction === "rtl" ? (
            <KeyboardArrowLeft />
          ) : (
            <KeyboardArrowRight />
          )}
        </IconButton>
        <IconButton
          onClick={this.handleLastPageButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="Last Page"
        >
          {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
        </IconButton>
      </div>
    );
  }
}

TablePaginationActions.propTypes = {
  classes: PropTypes.object.isRequired,
  count: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
  theme: PropTypes.object.isRequired
};

const TablePaginationActionsWrapped = withStyles(actionsStyles, {
  withTheme: true
})(TablePaginationActions);
