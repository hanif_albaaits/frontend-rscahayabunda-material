import React from "react";
import { Modal, ModalBody, ModalFooter, ModalHeader, Button } from "reactstrap";

class ViewBarangP extends React.Component {
  render() {
    return (
      <Modal isOpen={this.props.view} className="modal-lg">
        <ModalHeader>Data peminjaman {this.props.pinjam.kd_pinjam}</ModalHeader>
        <ModalBody>
          <form>
            <div className="card mb-3">
              <div className="card-header">Barang yang di pinjam </div>
              <div className="card-body">
                <div className="form-row">
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="nama_barang"
                      className="col-4 col-form-label text-right"
                    >
                      Nama Barang
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.pinjam.nama_barang}
                        disabled
                      />
                    </div>
                  </div>
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="merk"
                      className="col-4 col-form-label text-right"
                    >
                      Merk
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.pinjam.merk_barang}
                        disabled
                      />
                    </div>
                  </div>
                </div>
                <div className="form-row">
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="ukuran"
                      className="col-4 col-form-label text-right"
                    >
                      Ukuran
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.pinjam.ukuran_barang}
                        disabled
                      />
                    </div>
                  </div>
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="bahan"
                      className="col-4 col-form-label text-right"
                    >
                      Bahan
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.pinjam.bahan_barang}
                        disabled
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="card mb-3">
              <div className="card-header">Data Pegawai yang mengambil</div>
              <div className="card-body">
                <div className="form-row">
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="nama"
                      className="col-4 col-form-label text-right"
                    >
                      Nama Pegawai
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.pinjam.nama_pegawai}
                        disabled
                      />
                    </div>
                  </div>
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="email"
                      className="col-4 col-form-label text-right"
                    >
                      Email
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.pinjam.email_pegawai}
                        disabled
                      />
                    </div>
                  </div>
                </div>
                <div className="form-row">
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="no_hp"
                      className="col-4 col-form-label text-right"
                    >
                      No HP
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.pinjam.nohp_pegawai}
                        disabled
                      />
                    </div>
                  </div>
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="alamat"
                      className="col-4 col-form-label text-right"
                    >
                      Alamat
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.pinjam.alamat_pegawai}
                        disabled
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="card mb-3">
              <div className="card-header">Lokasi barang</div>
              <div className="card-body">
                <div className="form-row">
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="nama"
                      className="col-4 col-form-label text-right"
                    >
                      Lokasi
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.pinjam.nama_lokasi}
                        disabled
                      />
                    </div>
                  </div>
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="lantai"
                      className="col-4 col-form-label text-right"
                    >
                      Lantai
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.pinjam.lantai_lokasi}
                        disabled
                      />
                    </div>
                  </div>
                </div>
                <div className="form-row">
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="ruang"
                      className="col-4 col-form-label text-right"
                    >
                      Ruang
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.pinjam.ruang_lokasi}
                        disabled
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="card mb-3">
              <div className="card-header">Data peminjaman barang</div>
              <div className="card-body">
                <div className="form-row">
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="kondisi"
                      className="col-4 col-form-label text-right"
                    >
                      kondisi Sebelum
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.pinjam.kondisi_sebelum}
                        disabled
                      />
                    </div>
                  </div>
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="Catatan"
                      className="col-4 col-form-label text-right"
                    >
                      Catatan Sebelum
                    </label>
                    <div className="col-8">
                      <textarea
                        type="text"
                        className="form-control"
                        value={this.props.pinjam.catatan_sebelum}
                        disabled
                      />
                    </div>
                  </div>
                </div>
                <div className="form-row">
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="jumlah_pinjam"
                      className="col-4 col-form-label text-right"
                    >
                      Jumlah Ambil
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.pinjam.jumlah_pinjam}
                        disabled
                      />
                    </div>
                  </div>
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="tanggal_pinjam"
                      className="col-4 col-form-label text-right"
                    >
                      Tanggal Ambil
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.pinjam.tanggal_pinjam}
                        disabled
                      />
                    </div>
                  </div>
                </div>
                <div className="form-row">
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="kondisi"
                      className="col-4 col-form-label text-right"
                    >
                      Kondisi Sesudah
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.pinjam.kondisi_sesudah}
                        disabled
                      />
                    </div>
                  </div>
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="Catatan"
                      className="col-4 col-form-label text-right"
                    >
                      Catatan Sesudah
                    </label>
                    <div className="col-8">
                      <textarea
                        type="text"
                        className="form-control"
                        value={this.props.pinjam.catatan_sesudah}
                        disabled
                      />
                    </div>
                  </div>
                </div>
                <div className="form-row">
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="jumlah_kembali"
                      className="col-4 col-form-label text-right"
                    >
                      Jumlah Kembali
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.pinjam.jumlah_kembali}
                        disabled
                      />
                    </div>
                  </div>
                  <div className="form-group col-md-6 row">
                    <label
                      htmlFor="tanggal_kembali"
                      className="col-4 col-form-label text-right"
                    >
                      Tanggal Kembali
                    </label>
                    <div className="col-8">
                      <input
                        type="text"
                        className="form-control"
                        value={this.props.pinjam.tanggal_kembali}
                        disabled
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div>
              Dibuat Oleh : {this.props.pinjam.created_by + ",   "}
              Tanggal Dibuat :{this.props.pinjam.created_date}
            </div>
            <div>
              Diubah Oleh : {this.props.pinjam.updated_by + ",   "}
              Tanggal Diubah :{this.props.pinjam.updated_date}
            </div>
          </form>
        </ModalBody>
        <ModalFooter>
          <Button color="danger" onClick={this.props.closeModalHandler}>
            Tutup
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}
export default ViewBarangP;
