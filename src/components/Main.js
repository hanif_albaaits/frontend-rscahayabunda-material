import React from "react";
import { Switch, Route } from "react-router-dom";
import { Redirect } from "react-router";
import Login from "./layout/Login";
// import Test from "./layout/test";
import NotFound from "./layout/notfound";
import Dashboard from "./Dashboard";
import apiConfig from "../config/api.config.json";

const Main = () => {
  return (
    <main>
      <Switch>
        {/* <Route exact path="/" component={Login} /> */}
        <Route
          exact
          path="/"
          render={() =>
            localStorage.getItem(apiConfig.LS.TOKEN) == null ? (
              <Redirect to="/login" />
            ) : (
              <Dashboard />
            )
          }
        />
        <Route
          exact
          path="/dashboard"
          render={() =>
            localStorage.getItem(apiConfig.LS.TOKEN) == null ? (
              <Redirect to="/login" />
            ) : (
              <Dashboard />
            )
          }
        />
        <Route
          exact
          path="/login"
          render={() =>
            localStorage.getItem(apiConfig.LS.TOKEN) == null ? (
              <Route exact path="/login" component={Login} />
            ) : (
              <Dashboard />
            )
          }
        />
        <Route exact path="/not-found" component={NotFound} />
        <Dashboard />
        {/* <Redirect to="/not-found" /> */}
      </Switch>
    </main>
  );
};

export default Main;
